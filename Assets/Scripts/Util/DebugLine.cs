﻿using System;
using UnityEngine;

public class DebugLine
{

    public Vector3 point1;

    public Vector3 point2;

    public Color color;

    public DebugLine(Vector3 point1, Vector3 point2) : this(point1, point2, Color.yellow) { }

    public DebugLine(Vector3 point1, Vector3 point2, Color color)
    {
        this.point1 = point1;
        this.point2 = point2;
        this.color = color;
    }
}