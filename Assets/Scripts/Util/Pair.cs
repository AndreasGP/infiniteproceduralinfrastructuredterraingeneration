﻿using System;
using System.Xml.Serialization;
using UnityEngine;

[System.Serializable]
public struct Pair<T>
{

    [XmlAttribute("x")] public T x;

    [XmlAttribute("z")] public T z;

    public Pair(T x, T z)
    {
        this.x = x;
        this.z = z;
    }

    public bool Equals(Pair<T> other)
    {
        return x.Equals(other.x) && z.Equals(other.z) || z.Equals(other.x) && x.Equals(other.z);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is Point2 && Equals((Point2) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (x.GetHashCode()) ^ z.GetHashCode();
        }
    }

    public override string ToString()
    {
        return "[" + x + ", " + z + "]";
    }

}