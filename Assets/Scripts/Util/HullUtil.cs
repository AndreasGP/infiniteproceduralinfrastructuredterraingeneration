﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;

public static class HullUtil {

    public static List<Vector3> ConvexHull(List<Vector3> points)
    {
        if (points.Count < 3)
        {
            throw new ArgumentException("At least 3 points reqired", "points");
        }

        List<Vector3> hull = new List<Vector3>();

        // get leftmost point
        Vector3 vPointOnHull = points[0];
        foreach (var point in points)
        {
            if (point.x < vPointOnHull.x)
                vPointOnHull = point;
        }


        Vector3 vEndpoint;
        do
        {
            hull.Add(vPointOnHull);
            vEndpoint = points[0];

            for (int i = 1; i < points.Count; i++)
            {
                if ((vPointOnHull == vEndpoint)
                    || (Orientation(vPointOnHull, vEndpoint, points[i]) == -1))
                {
                    vEndpoint = points[i];
                }
            }

            vPointOnHull = vEndpoint;

        }
        while (vEndpoint != hull[0]);

        return hull;
    }

    private static int Orientation(Vector3 p1, Vector3 p2, Vector3 p)
    {
        // Determinant
        var Orin = (p2.x - p1.x) * (p.z - p1.z) - (p.x - p1.x) * (p2.z - p1.z);

        if (Orin > 0)
            return -1; //          (* Orientation is to the left-hand side  *)
        if (Orin < 0)
            return 1; // (* Orientation is to the right-hand side *)

        return 0; //  (* Orientation is neutral aka collinear  *)
    }

    public static List<IntPoint> ConvexHull(List<IntPoint> points)
    {
        if (points.Count < 3)
        {
            throw new ArgumentException("At least 3 points reqired", "points");
        }

        List<IntPoint> hull = new List<IntPoint>();

        // get leftmost point
        var vPointOnHull = points[0];
        foreach (var point in points)
        {
            if (point.X < vPointOnHull.X)
                vPointOnHull = point;
        }


        IntPoint vEndpoint;
        do
        {
            hull.Add(vPointOnHull);
            vEndpoint = points[0];

            for (int i = 1; i < points.Count; i++)
            {
                if ((vPointOnHull == vEndpoint)
                    || (Orientation(vPointOnHull, vEndpoint, points[i]) == -1))
                {
                    vEndpoint = points[i];
                }
            }

            vPointOnHull = vEndpoint;

        }
        while (vEndpoint != hull[0]);

        return hull;
    }

    private static int Orientation(IntPoint p1, IntPoint p2, IntPoint p)
    {
        // Determinant
        var Orin = (p2.X - p1.X) * (p.Y - p1.Y) - (p.X - p1.X) * (p2.Y - p1.Y);

        if (Orin > 0)
            return -1; //          (* Orientation is to the left-hand side  *)
        if (Orin < 0)
            return 1; // (* Orientation is to the right-hand side *)

        return 0; //  (* Orientation is neutral aka collinear  *)
    }
}
