﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A custom class to generate BillboardAssets from materials for the generation algorithm.
/// </summary>
public class BillboardManager : MonoBehaviour
{
    public static BillboardManager Ins;

    private Dictionary<Material, BillboardAsset> billboardAssets;

	void Start ()
	{
	    Ins = this;
        billboardAssets = new Dictionary<Material, BillboardAsset>();
	}

    public static BillboardAsset GetOrGenerateBillboardAsset(Material material, float width, float height, float bottom)
    {
        lock (Ins.billboardAssets)
        {
            if (!Ins.billboardAssets.ContainsKey(material))
            {
                var billboard = new BillboardAsset();
                billboard.width = width;
                billboard.height = height;
                billboard.bottom = bottom;
                billboard.material = material;

                var texCoords = new Vector4[1];
                texCoords[0] = new Vector4(0, 0, 1, 1);
                billboard.SetImageTexCoords(texCoords);

                var verts = new Vector2[4];
                verts[0] = new Vector2(0, 0);
                verts[1] = new Vector2(1, 0);
                verts[2] = new Vector2(1, 1);
                verts[3] = new Vector2(0, 1);
                billboard.SetVertices(verts);

                var indices = new ushort[6];
                indices[0] = 0;
                indices[1] = 2;
                indices[2] = 1;
                indices[3] = 2;
                indices[4] = 0;
                indices[5] = 3;
                billboard.SetIndices(indices);

                Ins.billboardAssets.Add(material, billboard);
            }

            return Ins.billboardAssets[material];
        }
    }
}
