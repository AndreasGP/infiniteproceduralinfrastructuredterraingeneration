﻿using System;
using System.Xml.Serialization;
using UnityEngine;

[System.Serializable]
public struct Point2
{

    [XmlAttribute("x")] public int x;

    [XmlAttribute("z")] public int z;

    public Point2(int x, int z)
    {
        this.x = x;
        this.z = z;
    }

    public Point2 Scl(float scalar)
    {
        return new Point2((int)(x * scalar), (int)(z * scalar));
    }

    public Point2 Scl(int scalar) {
        return new Point2(x * scalar, z * scalar);
    }


    public int GetLengthSquared() {
        return x * x + z * z;
    }

    public float GetLength() {
        return Mathf.Sqrt(GetLengthSquared());
    }

    public Vector2 ToVec2()
    {
        return new Vector2(x, z);
    }
    public Vector3 ToVec3()
    {
        return new Vector3(x, 0, z);
    }

    public int DistOnLongestAxis(Point2 other)
    {
        var _x = Math.Abs(x - other.x);
        var _z = Math.Abs(z - other.z);
        return  _x < _z ? _z : _x;
    }

    public Point2 MidpointBetween(Point2 other)
    {
        return new Point2((x + other.x)/2, (z + other.z)/2);
    }

    public static Point2 operator +(Point2 a, Point2 b)
    {
        a.x += b.x;
        a.z += b.z;
        return a;
    }

    public static Point2 operator -(Point2 a, Point2 b)
    {
        a.x -= b.x;
        a.z -= b.z;
        return a;
    }

    public static Point2 operator *(Point2 a, Point2 b)
    {
        a.x *= b.x;
        a.z *= b.z;
        return a;
    }

    public static bool operator ==(Point2 x, Point2 y)
    {
        return x.x == y.x && x.z == y.z;
    }

    public static bool operator !=(Point2 x, Point2 y) {
        return x.x != y.x || x.z != y.z;
    }

    public bool Equals(Point2 other)
    {
        return x == other.x && z == other.z;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is Point2 && Equals((Point2) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (x*397) ^ z;
        }
    }

    public override string ToString()
    {
        return "[" + x + ", " + z + "]";
    }

    public float Dist(Point2 pos)
    {
        return Mathf.Sqrt(Mathf.Pow(pos.x - x, 2) + Mathf.Pow(pos.z - z, 2));
    }
}