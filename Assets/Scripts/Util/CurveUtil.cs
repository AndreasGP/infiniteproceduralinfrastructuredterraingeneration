﻿using System.Collections;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public static class CurveUtil
{
    ///Calculates curve between point2 and point3
    public static List<Vector3> Segment(Vector3 point1, Vector3 point2, Vector3 point3, Vector3 point4, int segments)
    {

        var offset2 = Offset(point2, point1, point3);
        var offset3 = Offset(point3, point4, point2);

        var p0 = point2;
        var p1 = offset2;
        var p2 = offset3;
        var p3 = point3;

        var points = new List<Vector3>();
        for (int i = 0; i < segments; i++)
        {
            var t = i / (float)(segments - 1);
            points.Add(Bezier(t, p0, p1, p2, p3));
        }
        return points;
    }

    public static List<Vector3> SegmentWithJoinedStartAndEnd(Vector3 point1, Vector3 point2, Vector3 point3,
        Vector3 point4, int segments)
    {
        return SegmentWithJoinedStartAndEnd(point1, point2, point3, point4, segments, false);
    }

    ///Calculates a curve between point2 and point3, but the first 2 and last 2 segments are joined into one
        ///
        public static List<Vector3> SegmentWithJoinedStartAndEnd(Vector3 point1, Vector3 point2, Vector3 point3, Vector3 point4, int segments, bool skip)
    {

        var offset2 = Offset(point2, point1, point3);
        var offset3 = Offset(point3, point4, point2);

        var p0 = point2;
        var p1 = offset2;
        var p2 = offset3;
        var p3 = point3;

        var points = new List<Vector3>();
        
        //First point
        points.Add(Bezier(0, p0, p1, p2, p3));

        //Start from third point
        for (int i = skip ? 1 : 2; i < (skip ? segments - 1 : segments - 2); i++)
        {
            var t = i / (float)(segments - 1);
            points.Add(Bezier(t, p0, p1, p2, p3));
        }

        //Last point
        points.Add(Bezier(1, p0, p1, p2, p3));

        return points;
    }

    public static float GetSplineLength(List<Vector3> spline)
    {
        var length = 0f;
        for(int i = 0; i < spline.Count - 1; i++)
        {
            length += (spline[i + 1] - spline[i]).magnitude;
        }

        return length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="spline"></param>
    /// <param name="splineLength"></param>
    /// <param name="queryDist">In range [0, 1] - returns an approximate point on the curve that is given percent from its start</param>
    /// <returns></returns>
    public static Vector3 GetPointOnSplineByLength(List<Vector3> spline, float splineLength, float queryDist, out int segmentStartIndex)
    {
        var targetLength = splineLength * queryDist;
        var totalLength = 0f;
        for (int i = 0; i < spline.Count - 1; i++)
        {
            var segmentLength = (spline[i + 1] - spline[i]).magnitude;

            if (totalLength + segmentLength >= targetLength - 0.001f)
            {
                //Target is on this segment
                var distFromSegmentStart = targetLength - totalLength;
                segmentStartIndex = i;
                return Vector3.Lerp(spline[i], spline[i + 1], distFromSegmentStart / segmentLength);
            }
            else
            {
                totalLength += segmentLength;
            }
        }

        Debug.Log("Query dist: " + queryDist + "Target: " + targetLength + " spline length: " + splineLength);
        Debug.Log("Total length: " + totalLength);

        Debug.Log("Should not happen");
        segmentStartIndex = -1;
        return Vector3.zero;
    }

    public static Vector3 Offset(Vector3 point, Vector3 further, Vector3 closer)
    {
        var dist1 = (point - further).magnitude;
        var dist2 = (point - closer).magnitude;
        var dist = Mathf.Min(dist1, dist2) * 0.4f;

        var dir = (closer - further).normalized;

        var pos = point + dir * dist;

        return pos;
    }

    public static Vector3 Bezier(float t, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
    {
        var q1 = (1 - t) * (1 - t) * (1 - t);
        var q2 = 3 * (1 - t) * (1 - t) * t;
        var q3 = 3 * (1 - t) * t * t;
        var q4 = t * t * t;

        float x = p1.x * q1 + p2.x * q2 + p3.x * q3 + p4.x * q4;
        float z = p1.z * q1 + p2.z * q2 + p3.z * q3 + p4.z * q4;

        return new Vector3(x, 0, z);
    }


}
