﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ActionQueue : MonoBehaviour
{
    public static ActionQueue Ins;

    public static int MAX_HEAVY_ACTIONS_PER_FRAME = 1;

    private Queue<Action> queuedActions;

    private Queue<Action> queuedHeavyActions;

    void Awake()
    {
        if (Ins != null)
        {
            Debug.LogError("Multiple ActionQueues in scene.");
        }
        Ins = this;
        queuedActions = new Queue<Action>();
        queuedHeavyActions = new Queue<Action>();
    }

    void Update()
    {
        lock (queuedActions) {
            while (queuedActions.Count > 0) {
                Action action = queuedActions.Dequeue();
                action.Invoke();
            }
        }

        var sw = new System.Diagnostics.Stopwatch();
        sw.Start();

        var actionName = "";

        lock (queuedHeavyActions)
        {
            int count = 0;
            while (queuedHeavyActions.Count > 0)
            {
                Action action = queuedHeavyActions.Dequeue();
                actionName = action.Target.ToString();
                action.Invoke();
                count++;


                if (count == MAX_HEAVY_ACTIONS_PER_FRAME)
                    break;
            }
        }

        sw.Stop();
        if (sw.ElapsedMilliseconds > 15)
        {
            Debug.Log("Took " + sw.ElapsedMilliseconds + " ms to execute heavy action " + actionName);
        }
    }

    /// <summary>
    /// Enqueues the given action to be executed at the next frame by the main thread.
    /// </summary>
    /// <param name="action">The action to be enqueued</param>
    public void Enqueue(Action action) {
        lock (queuedActions)
        {
            queuedActions.Enqueue(action);
        }
    }
    /// <summary>
    /// Enqueues the given action to be executed at the next frame by the main thread. 
    /// Heavy actions are limited to a specific number per frame to ease the load.
    /// </summary>
    /// <param name="action">The action to be enqueued</param>
    public void EnqueueHeavy(Action action)
    {
        lock (queuedHeavyActions)
        {
            queuedHeavyActions.Enqueue(action);
        }
    }
}
