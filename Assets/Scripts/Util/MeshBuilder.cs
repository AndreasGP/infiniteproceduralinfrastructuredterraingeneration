﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MeshBuilder {

    private List<Vector3> vertices = new List<Vector3>();
    private List<Vector2> uv = new List<Vector2>();
    private List<Vector3> normals = new List<Vector3>();
    private List<Color> colors = new List<Color>();
    private Dictionary<int, List<int>> indices = new Dictionary<int, List<int>>();

    public void AddVertex(Vector3 vertex) {
        vertices.Add(vertex);
    }

    public void AddVertices(Vector3[] vertices) {
        foreach (Vector3 v in vertices) {
            this.vertices.Add(v);
        }
    }

    public void AddNormals(Vector3[] normals) {
        this.normals.AddRange(normals);
    }

    public void AddColor(Color color) {
        colors.Add(color);
    }

    public void AddFaceColor(Color color) {
        colors.Add(color);
        colors.Add(color);
        colors.Add(color);
        colors.Add(color);

    }

    public void AddColors(Color[] colors)
    {
        this.colors.AddRange(colors);
    }

    public void AddColors(Color color, int count) {
        for (int i = 0; i < count; i++) {
            colors.Add(color);
        }
    }

    public void AddTexCoord(Vector2 uv)
    {
        this.uv.Add(uv);
    }

    public void AddTexCoords(Vector2[] uv) {
        this.uv.AddRange(uv);
    }

    public int getNextIndex()
    {
        return vertices.Count;
    }

    public void AddQuadIndices(int materialIndex) {
        int offset = vertices.Count;
        List<int> indices = GetIndices(materialIndex);
        indices.Add(offset + 0);
        indices.Add(offset + 3);
        indices.Add(offset + 1);

        indices.Add(offset + 0);
        indices.Add(offset + 2);
        indices.Add(offset + 3);
    }

    public void AddTriangleIndices(int materialIndex) {
        int offset = vertices.Count;
        List<int> indices = GetIndices(materialIndex);
        indices.Add(offset + 0);
        indices.Add(offset + 1);
        indices.Add(offset + 2);
    }

    public void AddTriangleIndices(int materialIndex, int index1, int index2, int index3)
    {
        List<int> indices = GetIndices(materialIndex);
        indices.Add(index1);
        indices.Add(index2);
        indices.Add(index3);
    }

    public List<int> GetIndices(int index) {
        if(!indices.ContainsKey(index))
            indices[index] = new List<int>();
        return indices[index];
    }

    public void AddIndices(int materialIndex, int[] indices) {
        int offset = vertices.Count;
        var meshIndices = GetIndices(materialIndex);
        foreach (int index in indices) {
            meshIndices.Add(index + offset);
        }
    }


    public List<Color> GetColors() {
        return colors;
    }

    public Mesh ToBaseMeshWithoutSubMeshes()
    {
        if (vertices.Count == 0)
        {
            return null;
        }

        var mesh = new Mesh();
        mesh.name = "Mesh";

        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.colors = colors.ToArray();
        mesh.normals = normals.ToArray();
        mesh.uv = uv.ToArray();

        return mesh;
    }

    public Dictionary<int, List<int>> GetIndices()
    {
        return indices;
    }


    public Mesh ToMesh() {
        if (vertices.Count == 0) {
            return null;
        }

        var mesh = new Mesh();
        mesh.name = "Mesh";

        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.colors = colors.ToArray();
        mesh.normals = normals.ToArray();
        mesh.uv = uv.ToArray();

        mesh.subMeshCount = indices.Count;

        int actualSubmeshId = 0;

        if (mesh.subMeshCount > 0)
        {
            foreach(var entry in indices) {
                if (entry.Value.Count > 0)
                    mesh.SetTriangles(entry.Value.ToArray(), actualSubmeshId++);
            }
        }
        return mesh;
    }

}