﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;
using UnityEngine.Assertions.Comparers;

public static class ChunkUtil
{


    public static bool IntersectsWithOutlines(IntPoint pos, List<List<IntPoint>> outlines)
    {
        foreach (var outline in outlines)
        {
            if (InsideSimplePolygon(pos, outline))
                return true;
        }

        return false;
    }


    public static bool IsConvex(List<Vector3> points)
    {
        int n = points.Count;
        if (n < 4)
            return true;

        var sign = false;

        for (int i = 0; i < n; i++)
        {
            double dx1 = points[(i + 2) % n].x - points[(i + 1) % n].x;
            double dy1 = points[(i + 2) % n].z - points[(i + 1) % n].z;
            double dx2 = points[i].x - points[(i + 1) % n].x;
            double dy2 = points[i].z - points[(i + 1) % n].z;
            double zcrossproduct = dx1 * dy2 - dy1 * dx2;

            if (i == 0)
                sign = zcrossproduct > 0;
            else if (sign != (zcrossproduct > 0))
                return false;
        }

        return true;
    }

    public static bool IsConvex(List<IntPoint> points)
    {
        int n = points.Count;
        if (n < 4)
            return true;

        var sign = false;

        for (int i = 0; i < n; i++)
        {
            double dx1 = points[(i + 2) % n].X - points[(i + 1) % n].X;
            double dy1 = points[(i + 2) % n].Y - points[(i + 1) % n].Y;
            double dx2 = points[i].X - points[(i + 1) % n].X;
            double dy2 = points[i].Y - points[(i + 1) % n].Y;
            double zcrossproduct = dx1 * dy2 - dy1 * dx2;

            if (i == 0)
                sign = zcrossproduct > 0;
            else if (sign != (zcrossproduct > 0))
                return false;
        }

        return true;
    }

    public static Rect GetPointsRect(List<Vector3> points)
    {
        var minX = points[0].x;
        var maxX = points[0].x;
        var minZ = points[0].z;
        var maxZ = points[0].z;

        for (int i = 1; i < points.Count; i++)
        {
            var p = points[i];
            if (p.x < minX) minX = p.x;
            if (p.x > maxX) maxX = p.x;
            if (p.z < minZ) minZ = p.z;
            if (p.z > maxZ) maxZ = p.z;
        }

        return new Rect(minX, minZ, maxX - minX, maxZ - minZ);
    }

    public static List<IntPoint> GetRectAsIntPoints(Rect rect)
    {
        var chunkBounds = new List<IntPoint>();
        chunkBounds.Add(new IntPoint((int) (rect.xMin * ClipperUtil.COORD_MULTIPLIER),
            (int) (rect.yMin * ClipperUtil.COORD_MULTIPLIER)));
        chunkBounds.Add(new IntPoint((int) (rect.xMin * ClipperUtil.COORD_MULTIPLIER),
            (int) (rect.yMax * ClipperUtil.COORD_MULTIPLIER)));
        chunkBounds.Add(new IntPoint((int) (rect.xMax * ClipperUtil.COORD_MULTIPLIER),
            (int) (rect.yMax * ClipperUtil.COORD_MULTIPLIER)));
        chunkBounds.Add(new IntPoint((int) (rect.xMax * ClipperUtil.COORD_MULTIPLIER),
            (int) (rect.yMin * ClipperUtil.COORD_MULTIPLIER)));
        return chunkBounds;
    }

    public static Rect GetMacroChunkRect(int macroChunkX, int macroChunkZ)
    {
        var pos = CoordUtil.MacroChunkToGlobalPosition(macroChunkX, macroChunkZ);
        return new Rect(pos.x, pos.z, Const.MACRO_CHUNK_WIDTH_IN_METERS, Const.MACRO_CHUNK_WIDTH_IN_METERS);
    }

    public static Rect GetMacroChunkRect(int macroChunkX, int macroChunkZ, float offset)
    {
        var pos = CoordUtil.MacroChunkToGlobalPosition(macroChunkX, macroChunkZ);
        return new Rect(pos.x - offset, pos.z - offset, Const.MACRO_CHUNK_WIDTH_IN_METERS + offset * 2,
            Const.MACRO_CHUNK_WIDTH_IN_METERS + offset * 2);
    }


    public static Rect GetMesoChunkRect(Point2 mesoChunk)
    {
        return GetMesoChunkRect(mesoChunk.x, mesoChunk.z, 0);
    }

    public static Rect GetMesoChunkRect(int mesoChunkX, int mesoChunkZ, float offset)
    {
        var pos = CoordUtil.MesoChunkToGlobalPosition(mesoChunkX, mesoChunkZ);
        return new Rect(pos.x - offset, pos.z - offset, Const.MESO_CHUNK_WIDTH_IN_METERS + offset * 2,
            Const.MESO_CHUNK_WIDTH_IN_METERS + offset * 2);
    }

    public static Rect GetMicroChunkRect(int microChunkX, int microChunkZ, float offset)
    {
        var pos = CoordUtil.MicroChunkToGlobalPosition(microChunkX, microChunkZ);
        return new Rect(pos.x - offset, pos.z - offset, Const.MICRO_CHUNK_WIDTH_IN_METERS + offset * 2,
            Const.MICRO_CHUNK_WIDTH_IN_METERS + offset * 2);
    }

    public static System.Random GetMacroChunkRandom(int macroChunkX, int macroChunkZ)
    {
        //https://stackoverflow.com/questions/5928725/hashing-2d-3d-and-nd-vectors
        var hash = (macroChunkX * 73856093) ^ (macroChunkZ * 83492791);
        return new System.Random(hash);
    }

    public static System.Random GetMesoChunkRandom(Point2 mesoChunkPos)
    {
        return GetMesoChunkRandom(mesoChunkPos.x, mesoChunkPos.z);
    }

    public static System.Random GetMesoChunkRandom(int mesoChunkX, int mesoChunkZ)
    {
        //https://stackoverflow.com/questions/5928725/hashing-2d-3d-and-nd-vectors
        var hash = (mesoChunkX * 73856093) ^ (mesoChunkZ * 83492791);
        return new System.Random(hash);
    }

    public static System.Random GetMicroChunkRandom(int microChunkX, int microChunkZ)
    {
        //https://stackoverflow.com/questions/5928725/hashing-2d-3d-and-nd-vectors
        var hash = (microChunkX * 73856093) ^ (microChunkZ * 83492791);
        return new System.Random(hash);
    }

    public static Vector3? GetMacroChunkRoadGlobalPos(Point2 macroChunkPos)
    {
        return GetMacroChunkRoadGlobalPos(macroChunkPos.x, macroChunkPos.z);
    }

    public static Vector3? GetMacroChunkRoadGlobalPos(int macroChunkPosX, int macroChunkPosZ)
    {
        var random = GetMacroChunkRandom(macroChunkPosX, macroChunkPosZ);
        if (random.NextDouble() < Const.MACRO_CHUNK_ROAD_PROBABILITY)
        {
            //Offset a bit to avoid road nodes being on the chunk borders
            var offset = Const.HIGHWAY_WIDTH_IN_METERS * 2f;
            var w = Const.MACRO_CHUNK_WIDTH_IN_METERS - 2 * offset;
            var localPos = new Vector3(offset + (float) random.NextDouble() * w, 0,
                offset + (float) random.NextDouble() * w);

            return localPos + CoordUtil.MacroChunkToGlobalPosition(macroChunkPosX, macroChunkPosZ);
        }

        return null;
    }

    public static Point2 GetMacroChunkRuralNodeTile(Point2 macroChunkPos)
    {
        return GetMacroChunkRuralNodeTile(macroChunkPos.x, macroChunkPos.z);
    }

    public static Point2 GetMacroChunkRuralNodeTile(int macroChunkPosX, int macroChunkPosZ)
    {
        var random = GetMacroChunkRandom(macroChunkPosX + 1, macroChunkPosZ + 1);
        var localTile = new Point2(
            (int) (random.NextDouble() * Const.MACRO_CHUNK_WIDTH_IN_TILES),
            (int) (random.NextDouble() * Const.MACRO_CHUNK_WIDTH_IN_TILES));

        return localTile + new Point2(macroChunkPosX, macroChunkPosZ).Scl(Const.MACRO_CHUNK_WIDTH_IN_TILES);
    }

    public static Point2 GetMesoChunkRuralNodeTile(Point2 mesoChunkPos)
    {
        return GetMesoChunkRuralNodeTile(mesoChunkPos.x, mesoChunkPos.z);
    }

    public static Point2 GetMesoChunkRuralNodeTile(int mesoChunkPosX, int mesoChunkPosZ)
    {
        var random = GetMesoChunkRandom(mesoChunkPosX + 1, mesoChunkPosZ + 1);
        var localTile = new Point2(
            (int) (random.NextDouble() * Const.MESO_CHUNK_WIDTH_IN_TILES),
            (int) (random.NextDouble() * Const.MESO_CHUNK_WIDTH_IN_TILES));

        return localTile + new Point2(mesoChunkPosX, mesoChunkPosZ).Scl(Const.MESO_CHUNK_WIDTH_IN_TILES);
    }


    public static bool RectContainsPolygon(Rect rect, List<IntPoint> polygon)
    {
        foreach (var point in polygon)
        {
            var pos = point.ToVec3();
            if (pos.x < rect.xMin || pos.z < rect.yMin || pos.x >= rect.xMax || pos.z >= rect.yMax)
                return false;
        }

        return true;
    }

    //https://stackoverflow.com/questions/451426/how-do-i-calculate-the-area-of-a-2d-polygon
    public static float PolygonArea(List<Vector3> vectors)
    {
        var sum = 0.0;
        for (int i = 0; i < vectors.Count; i++)
        {
            var v1 = vectors[i];
            var v2 = vectors[(i + 1) % vectors.Count];

            sum += v1.x * v2.z - v2.x * v1.z;
        }

        return (float) (0.5 * Math.Abs(sum));
    }

    //https://stackoverflow.com/questions/451426/how-do-i-calculate-the-area-of-a-2d-polygon
    public static float PolygonArea(List<IntPoint> points)
    {
        var sum = 0.0;
        var div = ClipperUtil.COORD_MULTIPLIER;

        for (int i = 0; i < points.Count; i++)
        {
            var v1 = points[i];
            var v2 = points[(i + 1) % points.Count];

            sum += (v1.X / div) * (v2.Y / div) - (v2.X / div) * (v1.Y / div);
        }

        return (float) (0.5 * Math.Abs(sum));
    }


    //Returns true if the line p1-p2 intersects any line formed within list of 'points'
    public static bool Intersects(Vector3 p1, Vector3 p2, List<Vector3> points)
    {
        var len1 = (p1 - p2).sqrMagnitude;

        foreach (var p3 in points)
        {
            if (p3 == p2)
                continue;
            foreach (var p4 in points)
            {
                if (p4 == p2)
                    continue;
                if (ChunkUtil.Intersects(p1, p2, p3, p4))
                {
                    var len2 = (p3 - p4).sqrMagnitude;
                    if (len2 < len1)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    //Returns if all triangles formed between line p1-p2 and all the points in 'points' have all inner
    //angles smaller than angleThreshold
    public static bool Acute(Vector3 p1, Vector3 p2, List<Vector3> points, int angleThreshold)
    {
        var len1 = (p1 - p2).sqrMagnitude;

        foreach (var p3 in points)
        {
            if (p3 == p2)
                continue;

            if (ChunkUtil.Angle(p1, p2, p3) < angleThreshold)
            {
                var len2 = (p3 - p1).sqrMagnitude;
                if (len2 < len1)
                {
                    return false;
                }
            }

            if (ChunkUtil.Angle(p2, p1, p3) < angleThreshold)
            {
                var len2 = (p3 - p2).sqrMagnitude;
                if (len2 < len1)
                {
                    return false;
                }
            }
        }

        return true;
    }

    //Old stuff

    public static System.Random GetChunkRandom(Point2 chunkPos)
    {
        return GetChunkRandom(chunkPos.x, chunkPos.z);
    }

    public static System.Random GetChunkRandom(int chunkPosX, int ChunkPosZ)
    {
        //https://stackoverflow.com/questions/5928725/hashing-2d-3d-and-nd-vectors
        var hash = (chunkPosX * 73856093) ^ (ChunkPosZ * 83492791);
        return new System.Random(hash);
    }

    public static int Hash(Point2 p)
    {
        return Hash(p.x, p.z);
    }

    public static int Hash(int x, int z)
    {
        //https://stackoverflow.com/questions/5928725/hashing-2d-3d-and-nd-vectors
        return Mathf.Abs((x * 83492791) ^ (z * 73856093));
    }

    //    public static Point2? GetChunkRoadGlobalTile(Point2 chunkPos)
    //    {
    //        return GetChunkRoadGlobalTile(chunkPos.x, chunkPos.z);
    //    }
    //
    //    public static Point2? GetChunkRoadGlobalTile(int chunkPosX, int chunkPosZ)
    //    {
    //        var random = GetChunkRandom(chunkPosX, chunkPosZ);
    //        if (random.NextDouble() < Const.MESO_CHUNK_RURAL_ROAD_PROBABILITY) {
    //            var localTile = new Point2(
    //                (int)(random.NextDouble() * Const.CHUNK_WIDTH_IN_TILES),
    //                (int)(random.NextDouble() * Const.CHUNK_WIDTH_IN_TILES));
    //
    //            return localTile + new Point2(chunkPosX, chunkPosZ).Scl(Const.CHUNK_WIDTH_IN_TILES);
    //        }
    //        return null;
    //    }

    private static bool CounterClockwise(Vector3 a, Vector3 b, Vector3 c)
    {
        return (c.z - a.z) * (b.x - a.x) > (b.z - a.z) * (c.x - a.x);
    }

    public static bool Intersects(Vector3 start1, Vector3 end1, Vector3 start2, Vector3 end2)
    {
        //https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
        return CounterClockwise(start1, start2, end2) != CounterClockwise(end1, start2, end2)
               && CounterClockwise(start1, end1, start2) != CounterClockwise(start1, end1, end2);
    }

    //Angle between p1-p2 and p1-p3
    public static float Angle(Point2 p1, Point2 p2, Point2 p3)
    {
        var a = new Vector2(p2.x - p1.x, p2.z - p1.z);
        var b = new Vector2(p3.x - p1.x, p3.z - p1.z);
        var val = Mathf.Acos((a.x * b.x + a.y * b.y) / (a.magnitude * b.magnitude)) * 180 / Mathf.PI;
//        Debug.Log("Returning acos of " + (a.x * b.x + a.y * b.y) / (a.magnitude * b.magnitude));
//        Debug.Log("A cos is  " + Math.Acos((a.x * b.x + a.y * b.y) / (a.magnitude * b.magnitude)));
        return val;
    }

    //Angle between p1-p2 and p1-p3 in degrees
    public static float Angle(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        var a = p2 - p1;
        var b = p3 - p1;
        return Mathf.Acos((a.x * b.x + a.z * b.z) / (a.magnitude * b.magnitude)) * 180 / Mathf.PI;
    }

    //Angle between p1-p2
    public static float Angle(Point2 p1, Point2 p2)
    {
        return Mathf.Atan2(p2.z - p1.z, p2.x - p1.x) * 180 / Mathf.PI;
    }

    public static double AngleDifferenceDeg(double angle1, double angle2)
    {
        var a = angle1 - angle2;
        if (a > 180)
        {
            a -= 360;
        }
        else if (a < -180)
        {
            a += 360;
        }

        return a;
    }

    public static double AngleDifferenceRad(double angle1, double angle2)
    {
        var a = angle1 - angle2;
        if (a > Mathf.PI)
        {
            a -= Mathf.PI * 2;
        }
        else if (a < -Mathf.PI)
        {
            a += Mathf.PI * 2;
        }

        return a;
    }


    public static Point2 FindMostParallelPoints(Vector3 c, List<Vector3> points)
    {

        var bestAngle = 360.1;
        var bestI = -1;
        var bestJ = -1;

        for (int i = 0; i < points.Count; i++)
        {
            for (int j = i + 1; j < points.Count; j++)
            {
                var angle = Math.Abs(Angle(c, points[i], points[j]));
                if (Math.Abs(angle - 180) < bestAngle)
                {
                    bestAngle = Math.Abs(angle - 180);
                    bestI = i;
                    bestJ = j;
                }
            }
        }

        return new Point2(bestI, bestJ);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="c"></param>
    /// <param name="points"></param>
    /// <param name="minAngle">The angular difference between 2 most parallel lines, 0 meaning they're completely parallel</param>
    /// <returns></returns>
    public static Point2 FindMostParallelPoints(Vector3 c, List<Vector3> points, out float minAngle)
    {

        var bestAngle = 360.1;
        var bestI = -1;
        var bestJ = -1;

        for (int i = 0; i < points.Count; i++)
        {
            for (int j = i + 1; j < points.Count; j++)
            {
                var angle = Math.Abs(Angle(c, points[i], points[j]));
                if (Math.Abs(angle - 180) < bestAngle)
                {
                    bestAngle = Math.Abs(angle - 180);
                    bestI = i;
                    bestJ = j;
                }
            }
        }

        minAngle = (float) bestAngle;
        return new Point2(bestI, bestJ);
    }

    //Find a point p from 'points' such that line c-p is most parallel to the line c-edge
    public static MesoRoadNode FindMostParallelPoint(Vector3 c, Vector3 edge, List<MesoRoadNode> points)
    {
        var bestAngle = 360.1;
        var best = -1;
        for (int i = 0; i < points.Count; i++)
        {
            if (points[i].GlobalPos == edge)
                continue;

            var angle = Math.Abs(Angle(c, edge, points[i].GlobalPos));
            if (Math.Abs(angle - 180) < bestAngle)
            {
                bestAngle = Math.Abs(angle - 180);
                best = i;
            }
        }

        return points[best];
    }


    //TODO: Actually use the fact that the points are ordered
    public static Point2 FindMostParallelPoints(Vector3 c, List<MesoRoadNode> orderedPoints)
    {
        var bestAngle = 360.1;
        var bestI = -1;
        var bestJ = -1;
        for (int i = 0; i < orderedPoints.Count; i++)
        {
            for (int j = i + 1; j < orderedPoints.Count; j++)
            {
                var angle = Math.Abs(Angle(c, orderedPoints[i].GlobalPos, orderedPoints[j].GlobalPos));
                if (Math.Abs(angle - 180) < bestAngle)
                {
                    bestAngle = Math.Abs(angle - 180);
                    bestI = i;
                    bestJ = j;
                }
            }
        }

        return new Point2(bestI, bestJ);
    }

    //TODO: Actually use the fact that the points are ordered
    public static Point2 FindMostParallelPoints(Vector3 c, List<MacroRoadNode> orderedPoints)
    {
        var bestAngle = 360.1;
        var bestI = -1;
        var bestJ = -1;
        for (int i = 0; i < orderedPoints.Count; i++)
        {
            for (int j = i + 1; j < orderedPoints.Count; j++)
            {
                var angle = Math.Abs(Angle(c, orderedPoints[i].GlobalPos, orderedPoints[j].GlobalPos));
                if (Math.Abs(angle - 180) < bestAngle)
                {
                    bestAngle = Math.Abs(angle - 180);
                    bestI = i;
                    bestJ = j;
                }
            }
        }

        return new Point2(bestI, bestJ);
    }

    public static IntPoint CentroidOfPolygon(List<IntPoint> points)
    {
        var centroid = new IntPoint();

        foreach (var point in points)
        {
            centroid += point;
        }

        return centroid / points.Count;
    }

    public static Vector3 CentroidOfPolygon(List<Vector3> vertices)
    {
        var centroid = new Vector3();

        foreach (var vertex in vertices)
        {
            centroid += vertex;
        }

        return centroid / vertices.Count;
    }

    //https://stackoverflow.com/questions/1119627/how-to-test-if-a-point-is-inside-of-a-convex-polygon-in-2d-integer-coordinates
    public static bool InsidePolygon(Vector3 point, List<Vector3> polygon, bool isConvex, float minWidthSqr)
    {
        if (isConvex)
        {
            return InsideConvexPolygon(point, polygon, minWidthSqr);
        }
        else
        {
            return InsideSimplePolygon(point, polygon);
        }
    }
    
    public static bool InsideSimplePolygon(Vector3 p, List<Vector3> polygon)
    {
        // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
        bool inside = false;
        for (int i = 0, j = polygon.Count - 1; i < polygon.Count; j = i++)
        {
            if ((polygon[i].z > p.z) != (polygon[j].z > p.z) &&
                p.x < (polygon[j].x - polygon[i].x) * (p.z - polygon[i].z) / (polygon[j].z - polygon[i].z) +
                polygon[i].x)
            {
                inside = !inside;
            }
        }

        return inside;
    }

    public static bool InsideSimplePolygon(IntPoint p, List<IntPoint> polygon)
    {
        // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
        bool inside = false;
        for (int i = 0, j = polygon.Count - 1; i < polygon.Count; j = i++)
        {
            if ((polygon[i].Y > p.Y) != (polygon[j].Y > p.Y) &&
                p.X < (polygon[j].X - polygon[i].X) * (p.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) +
                polygon[i].X)
            {
                inside = !inside;
            }
        }

        return inside;
    }


    //https://stackoverflow.com/questions/1119627/how-to-test-if-a-point-is-inside-of-a-convex-polygon-in-2d-integer-coordinates
    public static bool InsideConvexPolygon(Vector3 point, List<Vector3> polygon, float minWidthSqr)
    {
        bool? previous = null;

        for (int i = 0; i < polygon.Count; i++)
        {
            var a = polygon[i];
            var b = polygon[(i + 1) % polygon.Count];

            if ((a - b).sqrMagnitude < minWidthSqr)
                continue;

            var affineSeg = b - a;
            var affinePoint = point - a;

            var current = GetSide(affineSeg, affinePoint);
            if (!current.HasValue)
            {
                return false; //outside or over an edge
            }
            else if (!previous.HasValue)
            {
                previous = current; //first segment
            }
            else if (previous != current)
            {
                return false;
            }
        }

        return true;
    }

    //https://stackoverflow.com/questions/1119627/how-to-test-if-a-point-is-inside-of-a-convex-polygon-in-2d-integer-coordinates
    public static bool InsideConvexPolygon(Vector3 point, List<Vector3> polygon)
    {
        bool? previous = null;

        for (int i = 0; i < polygon.Count; i++)
        {
            var a = polygon[i];
            var b = polygon[(i + 1) % polygon.Count];

            var affineSeg = b - a;
            var affinePoint = point - a;

            var current = GetSide(affineSeg, affinePoint);
            if (!current.HasValue)
            {
                return false; //outside or over an edge
            }
            else if (!previous.HasValue)
            {
                previous = current; //first segment
            }
            else if (previous != current)
            {
                return false;
            }
        }

        return true;
    }

    public static bool? GetSide(Vector3 a, Vector3 b)
    {
        var cross = a.x * b.z - a.z * b.x;
        if (cross < 0)
        {
            return false;
        }
        else if (cross > 0)
        {
            return true;
        }
        else
        {
            return null;
        }
    }

    //https://stackoverflow.com/questions/4543506/algorithm-for-intersection-of-2-lines comment
    public static Vector3? IntersectionPointWithLines(Vector3 start1, Vector3 end1, Vector3 start2, Vector3 end2)
    {
        var a1 = end1.z - start1.z;
        var b1 = start1.x - end1.x;
        var c1 = a1 * start1.x + b1 * start1.z;
        var a2 = end2.z - start2.z;
        var b2 = start2.x - end2.x;
        var c2 = a2 * start2.x + b2 * start2.z;

        var determinant = a1 * b2 - a2 * b1;

        if (Mathf.Abs(determinant) < 0.00002f) //lines are parallel
            return null;

        //Cramer's Rule
        float x = (b2 * c1 - b1 * c2) / determinant;
        float z = (a1 * c2 - a2 * c1) / determinant;
        return new Vector3(x, 0, z);
    }

    public static Vector3? IntersectionPointWithLineSegments(Vector3 start1, Vector3 end1, Vector3 start2, Vector3 end2)
    {
        var intersection = IntersectionPointWithLines(start1, end1, start2, end2);

        if (intersection.HasValue && IsBetweenTwoVectors(intersection.Value, start1, end1) &&
            IsBetweenTwoVectors(intersection.Value, start2, end2))
        {
            return intersection.Value;
        }

        return null;
    }

    //https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
    public static float PointDistanceFromLineSegment(Point2 p, Point2 start, Point2 end)
    {

        var A = p.x - start.x;
        var B = p.z - start.z;
        var C = end.x - start.x;
        var D = end.z - start.z;

        var dot = A * C + B * D;
        var len_sq = C * C + D * D;
        var param = -1f;
        if (len_sq != 0) //in case of 0 length line
            param = dot / (float) len_sq;

        float xx, yy;

        if (param < 0)
        {
            xx = start.x;
            yy = start.z;
        }
        else if (param > 1)
        {
            xx = end.x;
            yy = end.z;
        }
        else
        {
            xx = start.x + param * C;
            yy = start.z + param * D;
        }

        var dx = p.x - xx;
        var dy = p.z - yy;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    //https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
    public static float PointDistanceFromLineSegment(Vector3 p, Vector3 start, Vector3 end)
    {

        var A = p.x - start.x;
        var B = p.z - start.z;
        var C = end.x - start.x;
        var D = end.z - start.z;

        var dot = A * C + B * D;
        var len_sq = C * C + D * D;
        var param = -1f;
        if (len_sq != 0) //in case of 0 length line
            param = dot / (float) len_sq;

        float xx, yy;

        if (param < 0)
        {
            xx = start.x;
            yy = start.z;
        }
        else if (param > 1)
        {
            xx = end.x;
            yy = end.z;
        }
        else
        {
            xx = start.x + param * C;
            yy = start.z + param * D;
        }

        var dx = p.x - xx;
        var dy = p.z - yy;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    public static float PointDistanceFromLine(Vector3 p, Vector3 start, Vector3 end)
    {

        var A = p.x - start.x;
        var B = p.z - start.z;
        var C = end.x - start.x;
        var D = end.z - start.z;

        var dot = A * C + B * D;
        var len_sq = C * C + D * D;
        var param = -1f;
        if (len_sq != 0) //in case of 0 length line
            param = dot / (float) len_sq;

        float xx, yy;

        xx = start.x + param * C;
        yy = start.z + param * D;

        var dx = p.x - xx;
        var dy = p.z - yy;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    //https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
    public static Vector3 ProjectPointTolineSegment(Vector3 p, Vector3 start, Vector3 end)
    {

        var A = p.x - start.x;
        var B = p.z - start.z;
        var C = end.x - start.x;
        var D = end.z - start.z;

        var dot = A * C + B * D;
        var len_sq = C * C + D * D;
        var param = -1f;
        if (len_sq != 0) //in case of 0 length line
            param = dot / (float) len_sq;

        float xx, yy;

        if (param < 0)
        {
            xx = start.x;
            yy = start.z;
        }
        else if (param > 1)
        {
            xx = end.x;
            yy = end.z;
        }
        else
        {
            xx = start.x + param * C;
            yy = start.z + param * D;
        }

        return new Vector3(xx, 0, yy);
    }

    public static bool IsBetweenTwoVectors(Vector3 targetPoint, Vector3 point1, Vector3 point2)
    {
        double minX = Math.Min(point1.x, point2.x);
        double minZ = Math.Min(point1.z, point2.z);
        double maxX = Math.Max(point1.x, point2.x);
        double maxZ = Math.Max(point1.z, point2.z);

        double targetX = targetPoint.x;
        double targetZ = targetPoint.z;

        //TODO: might cause floating point problems, check SO and pastebin link above
        return minX <= targetX + 0.01f
               && targetX <= maxX + 0.01f
               && minZ <= targetZ + 0.01f
               && targetZ <= maxZ + 0.01f;
    }

    public static List<Point2> IntersectingTiles(List<Vector3> polygon, bool isConvex)
    {
        var tiles = new List<Point2>();

        var minX = polygon.Aggregate((curMin, p) => (p.x < curMin.x ? p : curMin));
        var minZ = polygon.Aggregate((curMin, p) => (p.z < curMin.z ? p : curMin));
        var minTile = CoordUtil.GlobalPositionToGlobalTile(minX.x, minZ.z);

        var maxX = polygon.Aggregate((curMax, p) => (p.x > curMax.x ? p : curMax));
        var maxZ = polygon.Aggregate((curMax, p) => (p.z > curMax.z ? p : curMax));

        var maxTile = CoordUtil.GlobalPositionToGlobalTile(maxX.x, maxZ.z) + new Point2(1, 1);

        var len = Const.TILE_WIDTH_IN_METERS;
        for (int x = minTile.x; x <= maxTile.x; x++)
        {
            for (int z = minTile.z; z <= maxTile.z; z++)
            {
                var tileRect = new Rect(x * len, z * len, len, len);

                if (InsidePolygon(Vec2ToVec3(tileRect.center), polygon, isConvex, 1))
                {
                    tiles.Add(new Point2(x, z));
                    continue;
                }

                for (int i = 0; i < polygon.Count; i++)
                {
                    var p1 = polygon[i];
                    var p2 = polygon[(i + 1) % polygon.Count];

                    if (LineSegmentIntersectsRect(p1, p2, tileRect))
                    {
                        tiles.Add(new Point2(x, z));
                        break;
                    }
                }
            }
        }

        return tiles;
    }


    //https://www.gamedev.net/forums/topic/440350-2d-line-box-intersection/
    public static bool LineSegmentIntersectLineSegment(IntPoint v1, IntPoint v2, IntPoint v3, IntPoint v4)
    {
        float denom = ((v4.Y - v3.Y) * ((float) v2.X - v1.X)) - ((v4.X - v3.X) * ((float) v2.Y - v1.Y));
        float numerator = ((v4.X - v3.X) * ((float) v1.Y - v3.Y)) - ((v4.Y - v3.Y) * ((float) v1.X - v3.X));

        float numerator2 = ((v2.X - v1.X) * ((float) v1.Y - v3.Y)) - ((v2.Y - v1.Y) * ((float) v1.X - v3.X));

        if (Mathf.Abs(denom) <= 0.000002f)
        {
            if (Mathf.Abs(numerator) <= 0.000002f && Mathf.Abs(numerator2) <= 0.000002f)
            {
                return false; //COINCIDENT;
            }

            return false; // PARALLEL;
        }

        float ua = numerator / denom;
        float ub = numerator2 / denom;

        return (ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f);
    }


    //https://www.gamedev.net/forums/topic/440350-2d-line-box-intersection/
    public static bool LineSegmentIntersectLineSegment(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
    {
        float denom = ((v4.z - v3.z) * (v2.x - v1.x)) - ((v4.x - v3.x) * (v2.z - v1.z));
        float numerator = ((v4.x - v3.x) * (v1.z - v3.z)) - ((v4.z - v3.z) * (v1.x - v3.x));

        float numerator2 = ((v2.x - v1.x) * (v1.z - v3.z)) - ((v2.z - v1.z) * (v1.x - v3.x));

        if (Mathf.Abs(denom) <= 0.000002f)
        {
            if (Mathf.Abs(numerator) <= 0.000002f && Mathf.Abs(numerator2) <= 0.000002f)
            {
                return false; //COINCIDENT;
            }

            return false; // PARALLEL;
        }

        float ua = numerator / denom;
        float ub = numerator2 / denom;

        return (ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f);
    }

    //https://www.gamedev.net/forums/topic/440350-2d-line-box-intersection/
    public static bool LineSegmentIntersectsRect(Vector3 v1, Vector3 v2, Rect r)
    {
        Vector3 lowerLeft = new Vector3(r.x, 0, r.y + r.height);
        Vector3 upperRight = new Vector3(r.x + r.width, 0, r.y);
        Vector3 upperLeft = new Vector3(r.x, 0, r.y);
        Vector3 lowerRight = new Vector3(r.x + r.width, 0, r.y + r.height);
        // check if it is inside
        if (v1.x > lowerLeft.x && v1.x < upperRight.x && v1.z < lowerLeft.z && v1.z > upperRight.z &&
            v2.x > lowerLeft.x && v2.x < upperRight.x && v2.z < lowerLeft.z && v2.z > upperRight.z)
        {
            return true;
        }

        // check each line for intersection
        if (LineSegmentIntersectLineSegment(v1, v2, upperLeft, lowerLeft)) return true;
        if (LineSegmentIntersectLineSegment(v1, v2, lowerLeft, lowerRight)) return true;
        if (LineSegmentIntersectLineSegment(v1, v2, upperLeft, upperRight)) return true;
        if (LineSegmentIntersectLineSegment(v1, v2, upperRight, lowerRight)) return true;
        return false;
    }

    /// <summary>
    /// Returns the intersection point of given 2 line segments p-p2, q-q2. y coordinate is ignored.
    /// Based on https://www.codeproject.com/Tips/862988/Find-the-Intersection-Point-of-Two-Line-Segments
    /// </summary>
    public static Vector3? LineSegmentsIntersection(Vector3 p, Vector3 p2, Vector3 q, Vector3 q2)
    {
        var r = p2 - p;
        var s = q2 - q;
        var rxs = Cross(r, s);
        var qpxr = Cross(q - p, r);

        var epsilon = 0.00001f;

        // If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
        if (Math.Abs(rxs) <= epsilon && Math.Abs(qpxr) <= epsilon)
        {
            // 1. If either  0 <= (q - p) * r <= r * r or 0 <= (p - q) * s <= * s
            // then the two lines are overlapping,
//            if (considerCollinearOverlapAsIntersect)
//                if ((0 <= (q - p) * r && (q - p) * r <= r * r) || (0 <= (p - q) * s && (p - q) * s <= s * s))
//                    return true;

            // 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
            // then the two lines are collinear but disjoint.
            // No need to implement this expression, as it follows from the expression above.
            return null;
        }

        // 3. If r x s = 0 and (q - p) x r != 0, then the two lines are parallel and non-intersecting.
        if (Math.Abs(rxs) <= epsilon && Math.Abs(qpxr) > epsilon)
        {
            return null;
        }

        // t = (q - p) x s / (r x s)
        var t = Cross(q - p, s) / rxs;

        // u = (q - p) x r / (r x s)

        var u = Cross(q - p, r) / rxs;

        // 4. If r x s != 0 and 0 <= t <= 1 and 0 <= u <= 1
        // the two line segments meet at the point p + t r = q + u s.
        if (Math.Abs(rxs) > epsilon && (0 <= t && t <= 1) && (0 <= u && u <= 1))
        {
            // We can calculate the intersection point using either t or u.
            // An intersection was found.
            return p + t * r;
        }

        // 5. Otherwise, the two line segments are not parallel but do not intersect.
        return null;
    }

    private static float Cross(Vector3 v1, Vector3 v2)
    {
        return v1.x * v2.z - v2.x * v1.z;
    }

    public static List<Vector3> GetRectIntersectionPointsWithSpline(Rect rect, List<Vector3> spline)
    {
        var output = new HashSet<Vector3>();

        for (int i = 0; i < spline.Count - 1; i++)
        {
            var v1 = spline[i];
            var v2 = spline[i + 1];
            var intersections = RectLineIntersectionPoints(v1, v2, rect);
            foreach (var intersection in intersections)
                output.Add(intersection);
        }

        return output.ToList();
    }

    public static List<Vector3> RectLineIntersectionPoints(Vector3 v1, Vector3 v2, Rect r)
    {
        Vector3 lowerLeft = new Vector3(r.x, 0, r.y + r.height);
        Vector3 upperRight = new Vector3(r.x + r.width, 0, r.y);
        Vector3 upperLeft = new Vector3(r.x, 0, r.y);
        Vector3 lowerRight = new Vector3(r.x + r.width, 0, r.y + r.height);

        var output = new List<Vector3>();

        if (v1.x > lowerLeft.x && v1.x < upperRight.x && v1.z < lowerLeft.z && v1.z > upperRight.z &&
            v2.x > lowerLeft.x && v2.x < upperRight.x && v2.z < lowerLeft.z && v2.z > upperRight.z)
        {
            //Empty list if line is inside
            return output;
        }

        // check each line for intersection
        var i1 = LineSegmentsIntersection(v1, v2, upperLeft, lowerLeft);
        if (i1.HasValue)
            output.Add(i1.Value);

        var i2 = LineSegmentsIntersection(v1, v2, lowerLeft, lowerRight);
        if (i2.HasValue)
            output.Add(i2.Value);

        var i3 = LineSegmentsIntersection(v1, v2, upperLeft, upperRight);
        if (i3.HasValue)
            output.Add(i3.Value);

        var i4 = LineSegmentsIntersection(v1, v2, upperRight, lowerRight);
        if (i4.HasValue)
            output.Add(i4.Value);

        return output;
    }

    public static float ClockwiseAngle(Vector3 start, Vector3 end1, Vector3 end2)
    {
        var v1 = (end1 - start).normalized;
        var v2 = (end2 - start).normalized;
        var dot = v1.x * v2.x + v1.z * v2.z;
        var det = v1.x * v2.z - v1.z * v2.x;
        return Mathf.Atan2(det, dot);
    }


    //https://stackoverflow.com/questions/3120357/get-closest-point-to-a-line
    public static Vector3 GetPointProjectionOnLine(Vector3 point, Vector3 a, Vector3 b)
    {
        var AP = point - a;
        var AB = b - a;

        var magnitudeAB = AB.sqrMagnitude;
        var ABAPproduct = Vector3.Dot(AP, AB);
        var distance = ABAPproduct / magnitudeAB;

        return a + AB * distance;
    }

    public static bool PointToTheRightOfLine(Vector3 point, Vector3 a, Vector3 b)
    {
        //https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line
        return (b.x - a.x) * (point.z - a.z) - (b.z - a.z) * (point.x - a.x) > 0;
    }

    public static float FindMinDistanceToPolygon(Vector3 vertex, List<Vector3> polygonVertices)
    {
        var minDist = float.MaxValue;

        for (int i = 0; i < polygonVertices.Count; i++)
        {
            var v1 = polygonVertices[i];
            var v2 = polygonVertices[(i + 1) % polygonVertices.Count];

            var dist = PointDistanceFromLineSegment(vertex, v1, v2);

            if (dist < minDist)
                minDist = dist;
        }

        return minDist;
    }
    
    public static bool RoadSegmentOverlapsChunkRegion(List<Vector3> chunkRegionOutline, RoadSegment segment)
    {
        //TODO: Currently not considering inflated roads :(
        for (int i = 0; i < segment.Spline.Count - 1; i++)
        {
            var p1 = segment.Spline[i];
            var p2 = segment.Spline[i + 1];

            if (InsideConvexPolygon(p1, chunkRegionOutline) || InsideConvexPolygon(p2, chunkRegionOutline))
                return true;

            for (int j = 0; j < chunkRegionOutline.Count; j++)
            {
                var c1 = chunkRegionOutline[j];
                var c2 = chunkRegionOutline[(j + 1) % chunkRegionOutline.Count];
                if (LineSegmentIntersectLineSegment(p1, p2, c1, c2))
                {
                    return true;
                }
            }
        }

        return false;
    }
    
    public static bool RoadSegmentIntersectsChunk(Rect chunkRoadRect, RoadSegment segment)
    {
        for (int i = 0; i < segment.Spline.Count - 1; i++)
        {
            var p1 = segment.Spline[i];
            var p2 = segment.Spline[i + 1];
            if (LineSegmentIntersectsRect(p1, p2, chunkRoadRect))
            {
                return true;
            }
        }

        return false;
    }

    public static int LongestEdge(List<Vector3> points)
    {
        var longestI = 0;
        var longest = (points[1] - points[0]).sqrMagnitude;
        for (int i = 1; i < points.Count; i++)
        {
            var v1 = points[i];
            var v2 = points[(i + 1) % points.Count];

            var len = (v2 - v1).sqrMagnitude;
            if (len > longest)
            {
                longest = len;
                longestI = i;
            }
        }

        return longestI;
    }

    public static int LongestEdge(List<IntPoint> points)
    {
        var longestI = 0;
        var longest = (points[1] - points[0]).SqrLength();
        for (int i = 1; i < points.Count; i++)
        {
            var v1 = points[i];
            var v2 = points[(i + 1) % points.Count];

            var len = (v2 - v1).SqrLength();
            if (len > longest)
            {
                longest = len;
                longestI = i;
            }
        }

        return longestI;
    }

    public static Vector3 Vec2ToVec3(Vector2 vec)
    {
        return new Vector3(vec.x, 0, vec.y);
    }


    public static bool ConvexPolygonsIntersect(List<IntPoint> poly1, List<IntPoint> poly2)
    {
        for (int i = 0; i < poly1.Count; i++)
        {
            var v1 = poly1[i];
            var v2 = poly1[(i + 1) % poly1.Count];

            for (int j = 0; j < poly2.Count; j++)
            {
                var v3 = poly2[j];
                var v4 = poly2[(j + 1) % poly2.Count];

                if (LineSegmentIntersectLineSegment(v1, v2, v3, v4))
                    return true;
            }
        }

        return false;
    }

    public static float PointDistanceFromPolygon(Vector3 point, List<Vector3> poly)
    {
        var minDist = 99999f;
        for (int i = 0; i < poly.Count; i++)
        {
            var v1 = poly[i];
            var v2 = poly[(i + 1) % poly.Count];

            var dist = PointDistanceFromLineSegment(point, v1, v2);
            if (dist < minDist)
                minDist = dist;
        }

        return minDist;
    }

    /// <summary>
    /// </summary>
    /// <returns>The first intersection between the line segment and the polygon, if any.</returns>
    public static Vector3? LineSegmentIntersectionWithPolygon(Vector3 start, Vector3 end, List<Vector3> poly)
    {
        for (int i = 0; i < poly.Count; i++)
        {
            var v1 = poly[i];
            var v2 = poly[(i + 1) % poly.Count];

            var intersect = LineSegmentsIntersection(v1, v2, start, end);
            if (intersect.HasValue)
                return intersect.Value;
        }

        return null;
    }

    public static void GenerateFenceTerrainObjects(MesoChunk mesoChunk, List<Vector3> outline,
        List<TerrainObjectMetaData> terrainObjects, Vector3 gatePosition, float maxDistFromGatePosition)
    {
        GenerateFenceTerrainObjects(mesoChunk, outline, terrainObjects, gatePosition, maxDistFromGatePosition, false);
    }

    public static void GenerateFenceTerrainObjects(MesoChunk mesoChunk, List<Vector3> outline, List<TerrainObjectMetaData> terrainObjects, Vector3 gatePosition, 
        float maxDistFromGatePosition, bool useTallFence)
    {
        for (int i = 0; i < outline.Count; i++)
        {
            var v1 = outline[i];
            var v2 = outline[(i + 1) % outline.Count];

            var dir = (v2 - v1);
            dir.y = 0;
            dir = dir.normalized;
            var rotRad = Mathf.Atan2(dir.z, dir.x);

            var lenVec = (v2 - v1);
            lenVec.y = 0;
            var len = lenVec.magnitude;

            var segments = Mathf.Round(len / (useTallFence ? 3 : 2));

            var scale = len / (useTallFence ? 3 : 2)  / segments;


            for (int j = 0; j < segments; j++)
            {

                var leftPos = v1 + (v2 - v1) * j / (float)segments;
                var rightPos = v1 + (v2 - v1) * (j + 1) / (float)segments;

                var centerPos = (leftPos + rightPos) / 2;

                if ((centerPos - gatePosition).magnitude < maxDistFromGatePosition)
                {
                    //Skip since a gate should be here
                    continue;
                }

                var height = Mathf.Min(mesoChunk.GetHeightAt(leftPos), mesoChunk.GetHeightAt((rightPos)));

                leftPos.y = height;

                var fence = TerrainObjectMetaData.Generate(useTallFence ? AssetManager.Ins.HighFencePrefab : AssetManager.Ins.LowFencePrefab, 
                    leftPos, 270 - rotRad * Mathf.Rad2Deg, new Vector3(1, 1, scale), 0);
                terrainObjects.Add(fence);
            }
        }
    }

    public static bool IsValidHousePosition(Vector3 housePos, float houseRotationRad, List<Vector3> polygon, float width, float height, out List<IntPoint> housePointOutline)
    {
        var rad = houseRotationRad;
        var radX = width / 2;
        var radY = height / 2;
        var offset1 = new Vector3(-radX * Mathf.Cos(rad) - radY * Mathf.Sin(rad), 0, -radX * Mathf.Sin(rad) + radY * Mathf.Cos(rad));
        var offset2 = new Vector3(radX * Mathf.Cos(rad) - radY * Mathf.Sin(rad), 0, radX * Mathf.Sin(rad) + radY * Mathf.Cos(rad));

        if (!ChunkUtil.InsideSimplePolygon(housePos + offset1, polygon) ||
            !ChunkUtil.InsideSimplePolygon(housePos - offset1, polygon) ||
            !ChunkUtil.InsideSimplePolygon(housePos + offset2, polygon) ||
            !ChunkUtil.InsideSimplePolygon(housePos - offset2, polygon))
        {
            housePointOutline = null;
            return false;
        }
        housePointOutline = new List<IntPoint>();
        housePointOutline.Add(new IntPoint(housePos - offset2));
        housePointOutline.Add(new IntPoint(housePos + offset1));
        housePointOutline.Add(new IntPoint(housePos + offset2));
        housePointOutline.Add(new IntPoint(housePos - offset1));

        return true;
    }

    public static List<Vector3> GetHouseOutline(Vector3 housePos, float houseRotationRad, float width, float height)
    {
        var rad = houseRotationRad;
        var radX = width / 2;
        var radY = height / 2;
        var offset1 = new Vector3(-radX * Mathf.Cos(rad) - radY * Mathf.Sin(rad), 0, -radX * Mathf.Sin(rad) + radY * Mathf.Cos(rad));
        var offset2 = new Vector3(radX * Mathf.Cos(rad) - radY * Mathf.Sin(rad), 0, radX * Mathf.Sin(rad) + radY * Mathf.Cos(rad));

        var houseOutline = new List<Vector3>();
        houseOutline.Add(housePos - offset2);
        houseOutline.Add(housePos + offset1);
        houseOutline.Add(housePos + offset2);
        houseOutline.Add(housePos - offset1);

        return houseOutline;
    }

public static bool IsValidGardenPosition(Vector3 gardenPos, float gradenRotationRad, float width, float height, List<Vector3> polygon, out List<IntPoint> gardenPointOutline)
    {
        var rad = gradenRotationRad;
        var radX = width / 2;
        var radY = height / 2;
        var offset1 = new Vector3(-radX * Mathf.Cos(rad) - radY * Mathf.Sin(rad), 0, -radX * Mathf.Sin(rad) + radY * Mathf.Cos(rad));
        var offset2 = new Vector3(radX * Mathf.Cos(rad) - radY * Mathf.Sin(rad), 0, radX * Mathf.Sin(rad) + radY * Mathf.Cos(rad));

        if (!ChunkUtil.InsideSimplePolygon(gardenPos + offset1, polygon) ||
            !ChunkUtil.InsideSimplePolygon(gardenPos - offset1, polygon) ||
            !ChunkUtil.InsideSimplePolygon(gardenPos + offset2, polygon) ||
            !ChunkUtil.InsideSimplePolygon(gardenPos - offset2, polygon))
        {
            gardenPointOutline = null;
            return false;
        }

        gardenPointOutline = new List<IntPoint>();
        gardenPointOutline.Add(new IntPoint(gardenPos - offset2));
        gardenPointOutline.Add(new IntPoint(gardenPos + offset1));
        gardenPointOutline.Add(new IntPoint(gardenPos + offset2));
        gardenPointOutline.Add(new IntPoint(gardenPos - offset1));

        return true;
    }
}