﻿using UnityEngine;


public class Billboardable : MonoBehaviour
{
    public BillboardRenderer BillboardRenderer;

    public  Material BillboardMaterial;

    public float Width;

    public float Height;

    public float Bottom;
}
