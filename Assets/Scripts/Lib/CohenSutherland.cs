﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Cohen Sutherland line clipping algorithm
/// http://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
/// http://bluetoque.ca/2012/01/cohen-sutherland-line-clipping/
/// </summary>
public class CohenSutherland {
    /// <summary>
    /// Bitfields used to partition the space into 9 regiond
    /// </summary>
    private const byte INSIDE = 0; // 0000
    private const byte LEFT = 1;   // 0001
    private const byte RIGHT = 2;  // 0010
    private const byte BOTTOM = 4; // 0100
    private const byte TOP = 8;    // 1000

    /// <summary>
    /// Compute the bit code for a point (x, y) using the clip rectangle
    /// bounded diagonally by (xmin, ymin), and (xmax, ymax)
    /// ASSUME THAT xmax , xmin , ymax and ymin are global constants.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private static byte ComputeOutCode(Rect rect, double x, double y) {
        // initialised as being inside of clip window
        byte code = INSIDE;

        if (x < rect.xMin)           // to the left of clip window
            code |= LEFT;
        else if (x > rect.xMax)     // to the right of clip window
            code |= RIGHT;
        if (y < rect.yMin)         // below the clip window
            code |= BOTTOM;
        else if (y > rect.yMax)       // above the clip window
            code |= TOP;

        return code;
    }

    /// <summary>
    /// Cohen–Sutherland clipping algorithm clips a line from
    /// P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with
    /// diagonal from (xmin, ymin) to (xmax, ymax).
    /// </summary>
    /// <param name="x0"></param>
    /// <param name="y0""</param>
    /// <param name="x1"></param>
    /// <param name="y1"></param>
    /// <returns>a list of two points in the resulting clipped line, or zero</returns>
    public static List<Vector2> CohenSutherlandLineClip(Rect rect,
                           Vector2 p0, Vector2 p1) {
        var x0 = p0.x;
        var y0 = p0.y;
        var x1 = p1.x;
        var y1 = p1.y;

        // compute outcodes for P0, P1, and whatever point lies outside the clip rectangle
        byte outcode0 = CohenSutherland.ComputeOutCode(rect, x0, y0);
        byte outcode1 = CohenSutherland.ComputeOutCode(rect, x1, y1);
        bool accept = false;

        while (true) {
            // Bitwise OR is 0. Trivially accept and get out of loop
            if ((outcode0 | outcode1) == 0) {
                accept = true;
                break;
            }
            // Bitwise AND is not 0. Trivially reject and get out of loop
            else if ((outcode0 & outcode1) != 0) {
                break;
            } else {
                // failed both tests, so calculate the line segment to clip
                // from an outside point to an intersection with clip edge
                float x, y;

                // At least one endpoint is outside the clip rectangle; pick it.
                byte outcodeOut = (outcode0 != 0) ? outcode0 : outcode1;

                // Now find the intersection point;
                // use formulas y = y0 + slope * (x - x0), x = x0 + (1 / slope) * (y - y0)
                if ((outcodeOut & TOP) != 0) {   // point is above the clip rectangle
                    x = x0 + (x1 - x0) * (rect.yMax - y0) / (y1 - y0);
                    y = rect.yMax;
                } else if ((outcodeOut & BOTTOM) != 0) { // point is below the clip rectangle
                    x = x0 + (x1 - x0) * (rect.yMin - y0) / (y1 - y0);
                    y = rect.yMin;
                } else if ((outcodeOut & RIGHT) != 0) {  // point is to the right of clip rectangle
                    y = y0 + (y1 - y0) * (rect.xMax - x0) / (x1 - x0);
                    x = rect.xMax;
                } else if ((outcodeOut & LEFT) != 0) {   // point is to the left of clip rectangle
                    y = y0 + (y1 - y0) * (rect.xMin - x0) / (x1 - x0);
                    x =rect.xMin;
                } else {
                    x = float.NaN;
                    y = float.NaN;
                }

                // Now we move outside point to intersection point to clip
                // and get ready for next pass.
                if (outcodeOut == outcode0) {
                    x0 = x;
                    y0 = y;
                    outcode0 = CohenSutherland.ComputeOutCode(rect, x0, y0);
                } else {
                    x1 = x;
                    y1 = y;
                    outcode1 = CohenSutherland.ComputeOutCode(rect, x1, y1);
                }
            }
        }

        // return the clipped line
        return (accept) ?
            new List<Vector2>()
        {
            new Vector2(x0,y0),
            new Vector2(x1, y1),
        } : null;

    }
}