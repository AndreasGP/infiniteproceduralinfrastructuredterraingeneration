﻿using System;
using UnityEngine;
// ReSharper disable PossibleLossOfFraction
#pragma warning disable 649

public enum SettingsLevel
{
    Always,
    WhenSelected,
    Never
}

public class Settings : MonoBehaviour
{
    private static Settings Ins;


    [Header("General Settings")]

    [Range(1, 25)]
    [SerializeField]
    public int minFramesPerChunk = 1;
    public static int MinFramesPerChunk { get { return Ins.minFramesPerChunk; } }

    [SerializeField]
    private bool debugThreads = true;
    public static bool DebugThreads { get { return Ins.debugThreads; } }

    [SerializeField]
    [Range(-1, 6)]
    public int mesoGenerationRadius;
    public static int MesoGenerationRadius { get { return Ins.mesoGenerationRadius; } }

    [SerializeField]
    private bool spawnTerrainObjects = true;
    public static bool SpawnTerrainObjects { get { return Ins.spawnTerrainObjects; } }

    [SerializeField]
    [Range(50, 500)]
    public int terrainObjectSpawnRadius;
    public static int TerrainObjectSpawnRadius { get { return Ins.terrainObjectSpawnRadius; } }

    [SerializeField]
    [Range(1, 100)]
    public int maxObjectsInstantiatedPerFrame;
    public static int MaxObjectsInstantiatedPerFrame { get { return Ins.maxObjectsInstantiatedPerFrame; } }


    [Space(3)]
    [Header("Macro Chunk Settings")]

    [SerializeField]
    private SettingsLevel drawMacroChunkBorders;
    public static SettingsLevel DrawMacroChunkBorders { get { return Ins.drawMacroChunkBorders; } }

//    [SerializeField]
    private SettingsLevel drawMacroChunkPosition = SettingsLevel.Never;
    public static SettingsLevel DrawMacroChunkPosition { get { return Ins.drawMacroChunkPosition; } }

//    [SerializeField]
    private SettingsLevel drawMesoChunkCoverage = SettingsLevel.Never;
    public static SettingsLevel DrawMesoChunkCoverage { get { return Ins.drawMesoChunkCoverage; } }

    [SerializeField]
    private SettingsLevel drawHighwayEdges = SettingsLevel.Never;
    public static SettingsLevel DrawHighwayEdges { get { return Ins.drawHighwayEdges; } }

    [SerializeField]
    private SettingsLevel drawHighwaySplines;
    public static SettingsLevel DrawHighwaySplines { get { return Ins.drawHighwaySplines; } }

    [SerializeField]
    private SettingsLevel drawHighwayOutlines;
    public static SettingsLevel DrawHighwayOutlines { get { return Ins.drawHighwayOutlines; } }

    [SerializeField]
    private bool generateMacroChunkNoiseMesh;
    public static bool GenerateMacroChunkNoiseMesh { get { return Ins.generateMacroChunkNoiseMesh; } }


    [Space(3)]
    [Header("Meso Chunk Settings")]

    [SerializeField]
    private SettingsLevel drawMesoChunkBorders;
    public static SettingsLevel DrawMesoChunkBorders { get { return Ins.drawMesoChunkBorders; } }

    [SerializeField]
    private SettingsLevel drawMesoChunkPosition;
    public static SettingsLevel DrawMesoChunkPosition { get { return Ins.drawMesoChunkPosition; } }

    [SerializeField]
    private SettingsLevel drawSideRoadEdges;
    public static SettingsLevel DrawSideRoadEdges { get { return Ins.drawSideRoadEdges; } }

    [SerializeField]
    private SettingsLevel drawSideRoadSplines;
    public static SettingsLevel DrawSideRoadSplines { get { return Ins.drawSideRoadSplines; } }

    [SerializeField]
    private SettingsLevel drawSideRoadOutlines;
    public static SettingsLevel DrawSideRoadOutlines { get { return Ins.drawSideRoadOutlines; } }

    [SerializeField]
    private SettingsLevel drawMesoRegionBorders;
    public static SettingsLevel DrawMesoRegionBorders { get { return Ins.drawMesoRegionBorders; } }

    [SerializeField]
    private SettingsLevel drawMesoRegionSubregions;
    public static SettingsLevel DrawMesoRegionSubregions { get { return Ins.drawMesoRegionSubregions; } }

    [SerializeField]
    private SettingsLevel drawMesoRegionSubregionNames;
    public static SettingsLevel DrawMesoRegionSubregionNames { get { return Ins.drawMesoRegionSubregionNames; } }
    [SerializeField]
    private bool generateTerrainMeshes;
    public static bool GenerateMesoChunkTerrainMeshes { get { return Ins.generateTerrainMeshes; } }

    [Space(3)]
    [Header("City Settings")]

    [SerializeField]
    private SettingsLevel drawCityCenters;
    public static SettingsLevel DrawCityCenters { get { return Ins.drawCityCenters; } }

    [SerializeField]
    private SettingsLevel drawCityBorders;
    public static SettingsLevel DrawCityBorders { get { return Ins.drawCityBorders; } }

    [SerializeField]
    private SettingsLevel drawCityHighwayBorders;
    public static SettingsLevel DrawCityHighwayBorders { get { return Ins.drawCityHighwayBorders; } }

    [SerializeField]
    private SettingsLevel drawCityBlocks;
    public static SettingsLevel DrawCityBlocks { get { return Ins.drawCityBlocks; } }

    [SerializeField]
    private bool generateCityBlockMeshes;
    public static bool GenerateCityBlockMeshes { get { return Ins.generateCityBlockMeshes; } }

    [Space(3)]
    [Header("Village Settings")]

    [SerializeField]
    private SettingsLevel drawVillageBorders;
    public static SettingsLevel DrawVillageBorders { get { return Ins.drawVillageBorders; } }


    void OnValidate()
    {
        Ins = this;
    }

    void Awake()
    {
        Ins = this;
//        Application.runInBackground = true;
    }


}
