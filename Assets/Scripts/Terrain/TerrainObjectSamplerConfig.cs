﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public enum SamplerType
{
    PoissonDisc,
    Grid,
    Simplex,
    //A custom sampler for use by nature reserves,  implements a custom possion disc
    NatureReserve
}

public struct TerrainObjectSamplerConfig
{
    public SamplerType SamplerType { get; private set; }

    public float SamplerStepX { get; private set; }

    public float SamplerStepY { get; private set; }

    public float VarianceX { get; private set; }

    public float VarianceY { get; private set; }

    public bool SpawnGrass { get; private set; }

    public List<TerrainObjectConfig> SecondaryTerrainObjectConfigs { get; private set; }

    public List<TerrainObjectConfig> TerrainObjectConfigs { get; private set; }

    public TerrainObjectSamplerConfig(SamplerType samplerType, float samplerStep, bool spawnGrass, List<TerrainObjectConfig> terrainObjectConfigs) 
        : this(samplerType, samplerStep, samplerStep, spawnGrass, terrainObjectConfigs)
    {
    }

    public TerrainObjectSamplerConfig(SamplerType samplerType, float samplerStepX, float samplerStepY, bool spawnGrass, List<TerrainObjectConfig> terrainObjectConfigs)
        : this(samplerType, samplerStepX, samplerStepY, 0, 0, spawnGrass, terrainObjectConfigs, null)
    {
    }

    public TerrainObjectSamplerConfig(SamplerType samplerType, float samplerStepX, float samplerStepY, float varianceX, float varianceY, 
        bool spawnGrass, List<TerrainObjectConfig> terrainObjectConfigs, List<TerrainObjectConfig> secondaryTerrainObjectConfigs) : this()
    {
        SamplerType = samplerType;
        SamplerStepX = samplerStepX;
        SamplerStepY = samplerStepY;
        VarianceX = varianceX;
        VarianceY = varianceY;
        SpawnGrass = spawnGrass;
        TerrainObjectConfigs = terrainObjectConfigs;
        SecondaryTerrainObjectConfigs = secondaryTerrainObjectConfigs;
    }

    public TerrainObjectConfig GetTerrainObjectConfig(float probability)
    {
        var accum = 0f;

        foreach (var conf in TerrainObjectConfigs)
        {
            accum += conf.Probability;
            if (accum >= probability)
                return conf;
        }

        Debug.LogError("Probabilities out of bounds");
        return TerrainObjectConfigs[0];
    }
}