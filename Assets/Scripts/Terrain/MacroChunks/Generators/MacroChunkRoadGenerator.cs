﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;

public class MacroChunkRoadGenerator
{
    private MacroChunk macroChunk;

    private Point2 macroChunkPos;

    public MacroChunkRoadGenerator(MacroChunk macroChunk)
    {
        this.macroChunk = macroChunk;
        macroChunkPos = macroChunk.Pos;
    }
    
    /// <summary>
    /// Generates the macroroaddata object for a given macrochunk.
    /// </summary>
    /// <returns></returns>
    public MacroRoadData GenerateRoadDataAsync()
    {
        var roadSegments = new List<RoadSegment>();
        var intersectingRoadSegments = new List<RoadSegment>();
        //Central road node neighbour -> road segment
        var centralRoadSegments = new Dictionary<Vector3, RoadSegment>();
        //Chunk position -> chunk macro road node
        var macroRoadNodes = new Dictionary<Point2, MacroRoadNode>();
        var centerRoadOutline = new List<IntPoint>();
        var centerShoulderOutline = new List<IntPoint>();
        var roadCoverage = new bool[Const.MACRO_CHUNK_AREA_IN_MESO_CHUNKS];

        #region  Generate neighbouring road nodes and check if any of their segments intersect this chunk
        for (int x = macroChunkPos.x - 2; x <= macroChunkPos.x + 2; x++)
        {
            for (int z = macroChunkPos.z - 2; z <= macroChunkPos.z + 2; z++)
            {
                var otherPos = new Point2(x, z);
                var otherMacroNode = GenerateMacroRoadNodeAsync(otherPos);
                if (otherMacroNode != null)
                {
                    macroRoadNodes.Add(otherPos, otherMacroNode);
                    var segments = GenerateMacroRoadNodeRoadSegmentsAsync(otherMacroNode);
                    roadSegments.AddRange(segments);
                }
            }
        }
        #endregion

        #region  Separate central road segments and calculate their outline
        var centralRoadNode = macroRoadNodes.ContainsKey(macroChunkPos) ? macroRoadNodes[macroChunkPos] : null;
        if (centralRoadNode != null)
        {
            var center = centralRoadNode.GlobalPos;
            foreach (var roadSegment in roadSegments)
            {
                if (roadSegment.V1 == center)
                {
                    centralRoadSegments.Add(roadSegment.V2, roadSegment);
                    intersectingRoadSegments.Add(roadSegment);
                }
                else if (roadSegment.V2 == center)
                {
                    centralRoadSegments.Add(roadSegment.V1, roadSegment);
                    intersectingRoadSegments.Add(roadSegment);
                }
            }
            var outlines = CalculateCentralRoadNodeOutlineAndCenterOutlinesAsync(macroChunkPos, centralRoadNode, centralRoadSegments);
            centerRoadOutline = outlines.x;
            centerShoulderOutline = outlines.z;
        }
        #endregion

        #region Separate road segments that are not intersecting with this chunk and generate an outline for the rest
        for (int i = roadSegments.Count - 1; i >= 0; i--)
        {
            var roadSegment = roadSegments[i];
            if (roadSegment.RoadOutline != null)
            {
                //Road segment is a central road segment and already generated
                continue;
            }

            if (ChunkUtil.RoadSegmentIntersectsChunk(macroChunk.RoadRect, roadSegment))
            {
                var vertices = GenerateRoadSegmentRoadAndShoulderVerticesAsync(macroChunkPos, roadSegment.Spline, false);
                roadSegment.SetRoadAndShoulderVertices(vertices.x, vertices.z);
                intersectingRoadSegments.Add(roadSegment);
            }
        }

        if (centerRoadOutline.Count < 5)
        {
            centerRoadOutline = null;
            centerShoulderOutline = null;
        }
        #endregion

        return new MacroRoadData(centralRoadSegments, roadSegments, intersectingRoadSegments, centralRoadNode, macroRoadNodes, centerRoadOutline, centerShoulderOutline, roadCoverage);
    }

    /// <summary>
    /// Generates road coverage for each mesochunk on the macrochunk as a boolean and returns a list of each intersecting mesochunk's side road node positions.
    /// </summary>
    /// <param name="roadCoverage"></param>
    /// <param name="intersectingRoadSegments"></param>
    /// <param name="mesoChunkCityData"></param>
    /// <returns></returns>
    public Vector3?[] GenerateCoverageAndMesoChunkRoadGlobalPositionsAsync(bool[] roadCoverage, List<RoadSegment> intersectingRoadSegments, MesoChunkCityData[] mesoChunkCityData)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;
        //A collection for determining the road global positions for this macrochunk
        var mesoChunkIntersectingRoadSegments = new RoadSegment[w*w];

        foreach (var segment in intersectingRoadSegments)
        {
            for (int i = 0; i < segment.Spline.Count - 1; i++)
            {
                var start = segment.Spline[i];
                var end = segment.Spline[i + 1];
                //TODO: If spline segment doesnt intersect with macrochunk, return early

                var intersectingMesoChunks = GetIntersectingMesoChunks(macroChunkPos, start, end, Const.HIGHWAY_WIDTH_IN_METERS);
                foreach (var mesoChunkPos in intersectingMesoChunks)
                {
                    var local = CoordUtil.GlobalMesoChunkToLocalMesoChunk(mesoChunkPos);
                    var j = local.z * w + local.x;
                    roadCoverage[j] = true;

                    if(mesoChunkIntersectingRoadSegments[j] == null)
                    {
                        mesoChunkIntersectingRoadSegments[j] = segment;
                    }
//                    else if(mesoChunkIntersectingRoadSegments[j] != segment)
//                    {
//                        //This mesochunk intersects with multiple highway segments
//                    }
                }
            }
        }

        var minMesoChunk = CoordUtil.MacroChunkToMesoChunk(macroChunkPos);
        var roadGlobalPositions = new Vector3?[w * w];
        
        for (int x = 0; x < Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS; x++)
        {
            for (int z = 0; z < Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS; z++)
            {
                var i = z * w + x;
                if (mesoChunkIntersectingRoadSegments[i] != null)
                {
                    var mesoChunkPos = new Point2(minMesoChunk.x + x, minMesoChunk.z + z);
                    var rect = ChunkUtil.GetMesoChunkRect(mesoChunkPos);

                    var localPosMaybe = FindRoadLocalPositionUsingRoadSegment(rect, mesoChunkIntersectingRoadSegments[i]);
                    if (localPosMaybe.HasValue)
                    {
                        roadGlobalPositions[i] = localPosMaybe.Value + CoordUtil.MesoChunkToGlobalPosition(mesoChunkPos);
                    }
                    else
                    {
                        //A road tile placed by the highway would have been too close to the chunk border
                        //Place in the middle
                        var localPos = new Vector3(Const.MESO_CHUNK_WIDTH_IN_METERS / 2 + Const.SIDE_ROAD_WIDTH_IN_METERS / 2,
                            0, Const.MESO_CHUNK_WIDTH_IN_METERS / 2 + Const.SIDE_ROAD_WIDTH_IN_METERS / 2);

                        roadGlobalPositions[i] = localPos + CoordUtil.MesoChunkToGlobalPosition(mesoChunkPos);
//                        roadCoverage[i] = false;
                    }
                }
                else
                {
                    //This chunk intersects with no highways, consider it for road placement
                    var mesoChunkPos = new Point2(minMesoChunk.x + x, minMesoChunk.z + z);

                    if (mesoChunkCityData[i] == null)
                    {
                        //This meso chunk does not contain a city
                        var random = ChunkUtil.GetMesoChunkRandom(mesoChunkPos);
                        if (random.NextDouble() < Const.MESO_CHUNK_RURAL_ROAD_PROBABILITY)
                        {
                            //Prevent generating road positions too close to chunk border to prevent having to deal with messy road polys
                            var offset = Const.SIDE_ROAD_WIDTH_IN_METERS * 2f;
                            var multiplier = Const.MESO_CHUNK_WIDTH_IN_METERS - 2 * offset;
                            var localPos = new Vector3(offset + (float)random.NextDouble() * multiplier, 0, offset + (float)random.NextDouble() * multiplier);

                            roadGlobalPositions[i] = localPos + CoordUtil.MesoChunkToGlobalPosition(mesoChunkPos);
                        }
                    }
                    else
                    {
                        //This meso chunk contains a city

                        //TODO: Use better noise
                        //Based on noise, decide to use grid like streets or more freeflowing streets
                        var noise = NoiseService.GetNoiseAtPos(mesoChunkPos.x * 10000f, mesoChunkPos.z * 10000f);

                        if (noise < 0.5f)
                        {
                            //Place randomly

                            //Prevent generating road positions too close to chunk border to prevent having to deal with messy road polys
                            var offset = Const.SIDE_ROAD_WIDTH_IN_METERS * 2f;
                            var multiplier = Const.MESO_CHUNK_WIDTH_IN_METERS - 2 * offset;
                            var random = ChunkUtil.GetMesoChunkRandom(mesoChunkPos);
                            var localPos = new Vector3(offset + (float)random.NextDouble() * multiplier, 0, offset + (float)random.NextDouble() * multiplier);

                            roadGlobalPositions[i] = localPos + CoordUtil.MesoChunkToGlobalPosition(mesoChunkPos);
                        }
                        else
                        {
                            //Place in the middle
                            var localPos = new Vector3(Const.MESO_CHUNK_WIDTH_IN_METERS / 2 + Const.SIDE_ROAD_WIDTH_IN_METERS / 2,
                                0, Const.MESO_CHUNK_WIDTH_IN_METERS / 2 + Const.SIDE_ROAD_WIDTH_IN_METERS / 2);

                            roadGlobalPositions[i] = localPos + CoordUtil.MesoChunkToGlobalPosition(mesoChunkPos);

                        }
                    }
                }
            }
        }
        
        return roadGlobalPositions;
    }

    /// <summary>
    /// Determines the point of a side road node using the mesochunks rect and the road segment that intersects with it.
    /// If the side road node would be placed too close to the edge, a null is returned instead.
    /// </summary>
    /// <param name="chunkRect"></param>
    /// <param name="segment"></param>
    /// <returns></returns>
    private static Vector3? FindRoadLocalPositionUsingRoadSegment(Rect chunkRect, RoadSegment segment)
    {
        var intersectionPoints = ChunkUtil.GetRectIntersectionPointsWithSpline(chunkRect, segment.Spline);

        if (intersectionPoints.Count == 2)
        {
            //Midway between intersections
            var globalPos = (intersectionPoints[0] + intersectionPoints[1]) * 0.5f;

            //Decide, if the position is too close to the chunk border
            var localPos = CoordUtil.GlobalPositionToLocalMesoPosition(globalPos);
            var min = Const.SIDE_ROAD_WIDTH_IN_METERS * 2f;
            var max = Const.MESO_CHUNK_WIDTH_IN_METERS - Const.SIDE_ROAD_WIDTH_IN_METERS * 2f;

            if (localPos.x < min || localPos.z < min || localPos.z > max || localPos.z > max)
            {
                //Highway is too close to the chunk edge
                return null;

            }
            else
            {
                return localPos;
            }
        } else if (intersectionPoints.Count == 1)
        {
//            //The highway ends inside this chunk, place the road node there
//
//            var highwayEndPos = segment.Spline[0] + (segment.Spline[1] - segment.Spline[0]).normalized * 4f;
//
//            if (!ChunkUtil.InsideSimplePolygon(segment.Spline[0],
//                ClipperUtil.ToVectors(ChunkUtil.GetRectAsIntPoints(chunkRect))))
//            {
//                highwayEndPos = segment.Spline[segment.Spline.Count - 1] + (segment.Spline[segment.Spline.Count - 1] - segment.Spline[segment.Spline.Count - 2]).normalized * 4f;
//            }
//
//            var localPos = CoordUtil.GlobalPositionToLocalMesoPosition(highwayEndPos);
//            var min = Const.SIDE_ROAD_WIDTH_IN_METERS * 2f;
//            var max = Const.MESO_CHUNK_WIDTH_IN_METERS - Const.SIDE_ROAD_WIDTH_IN_METERS * 2f;
//
//            if (localPos.x < min || localPos.z < min || localPos.z > max || localPos.z > max)
//            {
//                //Highway is too close to the chunk edge
//                return null;
//
//            }
//            else
//            {
//                return localPos;
//            }
        }

        //default to null if somehow more than 2 intersection points exist
        return null;
    }

    /// <summary>
    /// </summary>
    /// <param name="centralNode"></param>
    /// <param name="centralRoadSegments"></param>
    /// <returns>A pair of outlines. The first outline is the central road segment outline, the second outline is the central shoulder region outline.</returns>
    private static Pair<List<IntPoint>> CalculateCentralRoadNodeOutlineAndCenterOutlinesAsync(Point2 macroChunkPos, MacroRoadNode centralNode, Dictionary<Vector3, RoadSegment> centralRoadSegments)
    {
        var centerRoadOutline = new List<IntPoint>();
        var centerShoulderOutline = new List<IntPoint>();
        var centerGlobalPos = centralNode.GlobalPos;
        var orderedNeighbours = centralNode.Neighbours.OrderBy(n => Math.Atan2(n.z - centerGlobalPos.z, n.x - centerGlobalPos.x)).ToList();
        var l = orderedNeighbours.Count;

        if (l > 1)
        {
            //At least 2 splines
            for (int i = 0; i < l; i++)
            {
                var leftGlobalPos = orderedNeighbours[(i - 1 + l)%l];
                var neighbourGlobalPos = orderedNeighbours[i];
                var rightGlobalPos = orderedNeighbours[(i + 1)%l];

                var leftSegment = centralRoadSegments[leftGlobalPos];
                var neighbourSegment = centralRoadSegments[neighbourGlobalPos];
                var rightSegment = centralRoadSegments[rightGlobalPos];

                //TODO: We actually only need the 2nd spline element from the center side
                //Reverse the spline if the left hand side is not the center
                var leftSpline = leftSegment.V1 == centerGlobalPos
                    ? leftSegment.Spline
                    : leftSegment.Reverse();
                var neighbourSpline = neighbourSegment.V1 == centerGlobalPos
                    ? neighbourSegment.Spline
                    : neighbourSegment.Reverse();
                var rightSpline = rightSegment.V1 == centerGlobalPos
                    ? rightSegment.Spline
                    : rightSegment.Reverse();

                var vertices = GenerateRoadSegmentRoadAndShoulderVerticesAsync(macroChunkPos, leftSpline, neighbourSpline, rightSpline, centerRoadOutline, centerShoulderOutline);
                neighbourSegment.SetRoadAndShoulderVertices(vertices.x, vertices.z);
            }
        }
        else
        {
            //Just 1 spline
            var neighbourGlobalPos = orderedNeighbours[0];
            var neighbourSegment = centralRoadSegments[neighbourGlobalPos];
            var neighbourSpline = neighbourSegment.V1 == centerGlobalPos
                ? neighbourSegment.Spline
                : neighbourSegment.Reverse();
            var vertices = GenerateRoadSegmentRoadAndShoulderVerticesAsync(macroChunkPos, neighbourSpline, true);
            neighbourSegment.SetRoadAndShoulderVertices(vertices.x, vertices.z);
        }

        return new Pair<List<IntPoint>>(centerRoadOutline, centerShoulderOutline);
    }

    /// <summary>
    /// Generates the outline for a given spline and does not take into account how either end should look like when merged with other splines
    /// </summary>
    /// <param name="macroChunkPos"></param>
    /// <param name="spline"></param>
    /// <param name="canCutEndIfOutOfChunk">If true, the outline will be cut short if the next spline segment starting point is outside of the chunk. 
    /// This is only useful if the spline starts inside the chunk and is guaranteed outgoing.</param>
    /// <returns>A pair of lists. The first list contains the road vertices and the second list contains the margin vertices.</returns>
    private static Pair<List<Pair<IntPoint>>> GenerateRoadSegmentRoadAndShoulderVerticesAsync(Point2 macroChunkPos, List<Vector3> spline, bool canCutEndIfOutOfChunk)
    {
        var roadRadius = Const.HIGHWAY_WIDTH_IN_METERS / 2;
        var shoulderRadius = Const.HIGHWAY_SHOULDER_WIDTH_IN_METERS / 2;

        var roadVertices = new List<Pair<IntPoint>>();
        var shoulderVertices = new List<Pair<IntPoint>>();

        for (int i = 0; i < spline.Count; i++)
        {
            if (canCutEndIfOutOfChunk && i > 0 && !CoordUtil.OnMacroChunk(spline[i - 1], macroChunkPos, Const.HIGHWAY_SHOULDER_WIDTH_IN_METERS * 2))
                //This spline segment is off this chunk so it can't affect this chunk anymore
                break;

            Vector3 v1 = Vector3.up;
            Vector3 v2 = Vector3.up;
            if (i > 0)
            {
                v1 = (spline[i] - spline[i - 1]).normalized;
                v2 = i < spline.Count - 1 ? (spline[i + 1] - spline[i]).normalized : v1;
            }
            else if (i < spline.Count - 1)
            {
                v2 = (spline[i + 1] - spline[i]).normalized;
                v1 = v2;
            }
            
            var mid = (v1 + v2).normalized;
            //Rotate 90 degrees
            mid = new Vector3(-mid.z, 0, mid.x);

            var roadLeftSideVector = spline[i] - mid * roadRadius;
            var roadRightSideVector = spline[i] + mid * roadRadius;
            var shoulderLeftSideVector = spline[i] - mid * shoulderRadius;
            var shoulderRightSideVector = spline[i] + mid * shoulderRadius;

            roadVertices.Add(new Pair<IntPoint>(new IntPoint(roadLeftSideVector), new IntPoint(roadRightSideVector)));
            shoulderVertices.Add(new Pair<IntPoint>(new IntPoint(shoulderLeftSideVector), new IntPoint(shoulderRightSideVector)));
        }

        return new Pair<List<Pair<IntPoint>>>(roadVertices, shoulderVertices);
    }

    //TODO: Update description
    //TODO: Need guarantee that the first segment length is at least highway width to prevent dependencies on the next segments
    /// <summary>
    /// Generates the outline of a given spline and takes into account the neighbouring splines
    /// </summary>
    /// <param name="centerTile"></param>
    /// <param name="leftSpline"></param>
    /// <param name="neighbourSpline"></param>
    /// <param name="rightSpline"></param>
    /// <param name="centerOutline"></param>
    /// <returns></returns>
    private static Pair<List<Pair<IntPoint>>> GenerateRoadSegmentRoadAndShoulderVerticesAsync(Point2 macroChunkPos, List<Vector3> leftSpline, 
        List<Vector3> neighbourSpline, List<Vector3> rightSpline, List<IntPoint> centerRoadOutline, List<IntPoint> centerShoulderOutline)
    {
        #region the first segment with the junction
        //Guaranteed to be the global macroChunkPos of 'centerTile'
        var center = neighbourSpline[0];

        var roadRadius = Const.HIGHWAY_WIDTH_IN_METERS / 2;
        var shoulderRadius = Const.HIGHWAY_SHOULDER_WIDTH_IN_METERS / 2;

        var forward = (center - neighbourSpline[1]).normalized;
        var right = new Vector3(forward.z, 0, -forward.x);

        //Left edge
        var leftAngleDiff = ChunkUtil.ClockwiseAngle(center, leftSpline[1], neighbourSpline[1]);
        var leftOffsetR = roadRadius / (float)Math.Tan(leftAngleDiff / 2);
        var leftOffsetS = shoulderRadius / (float)Math.Tan(leftAngleDiff / 2);
        var l1r = center - roadRadius * right - forward * leftOffsetR;
        var l1s = center - shoulderRadius * right - forward * leftOffsetS;

        //Right edge
        var rightAngleDiff = ChunkUtil.ClockwiseAngle(center, neighbourSpline[1], rightSpline[1]);
        var rightOffsetR = roadRadius / (float)Math.Tan(rightAngleDiff / 2);
        var rightOffsetS = shoulderRadius / (float)Math.Tan(rightAngleDiff / 2);
        var r1r = center + roadRadius * right - forward * rightOffsetR;
        var r1s = center + shoulderRadius * right - forward * rightOffsetS;


        //We derive a point that is slightly outward from l1 and r1 and store it in a collection
        //After processing all the central road segments, this wil be a nice lovely polygon covering the center of a junction
        //The offsetting is necessary to avoid some ugly floating point problems that might otherwise cause gaps
        centerRoadOutline.Add(new IntPoint(l1r - forward * 0.02f));
        centerRoadOutline.Add(new IntPoint(r1r - forward * 0.02f));
        centerShoulderOutline.Add(new IntPoint(l1s - forward * 0.02f));
        centerShoulderOutline.Add(new IntPoint(r1s - forward * 0.02f));

        #endregion

        var roadVertices = new List<Pair<IntPoint>>();
        var shoulderVertices = new List<Pair<IntPoint>>();

        roadVertices.Add(new Pair<IntPoint>(new IntPoint(l1r), new IntPoint(r1r)));
        shoulderVertices.Add(new Pair<IntPoint>(new IntPoint(l1s), new IntPoint(r1s)));

        #region middle of the spline

        for (int i = 1; i < neighbourSpline.Count; i++)
        {
            if ( !CoordUtil.OnMacroChunk(neighbourSpline[i - 1], macroChunkPos, Const.SIDE_ROAD_SHOULDER_WIDTH_IN_METERS*2))
                //This spline segment is off this chunk so it can't affect this chunk anymore
                break;

            var v1 = (neighbourSpline[i] - neighbourSpline[i - 1]).normalized;
            //Default to v2 if this is the last segment
            var v2 = i < neighbourSpline.Count - 1 ? (neighbourSpline[i + 1] - neighbourSpline[i]).normalized : v1;
            var mid = (v1 + v2).normalized;
            //Rotate 90 degrees
            mid = new Vector3(-mid.z, 0, mid.x);

            var roadLeftSideVector = neighbourSpline[i] - mid * roadRadius;
            var roadRightSideVector = neighbourSpline[i] + mid * roadRadius;
            var shoulderLeftSideVector = neighbourSpline[i] - mid * shoulderRadius;
            var shoulderRightSideVector = neighbourSpline[i] + mid * shoulderRadius;

            roadVertices.Add(new Pair<IntPoint>(new IntPoint(roadLeftSideVector), new IntPoint(roadRightSideVector)));
            shoulderVertices.Add(new Pair<IntPoint>(new IntPoint(shoulderLeftSideVector), new IntPoint(shoulderRightSideVector)));

        }

        #endregion

        return new Pair<List<Pair<IntPoint>>>(roadVertices, shoulderVertices); ;
    }

    /// <summary>
    /// Generate curves and instantiates roadsegment class instances for each road of the given macroroadnode.
    /// </summary>
    /// <param name="macroNode"></param>
    /// <returns></returns>
    private static List<RoadSegment> GenerateMacroRoadNodeRoadSegmentsAsync(MacroRoadNode macroNode)
    {
        var roadSegments = new List<RoadSegment>();

        foreach (var neighbour in macroNode.Neighbours)
        {

            if (neighbour.x < macroNode.GlobalPos.x ||
                neighbour.x == macroNode.GlobalPos.x && neighbour.z < macroNode.GlobalPos.z)
                //This edge has already been processed by a previous loop step in the parent function
                continue;

            var controlPoints = new List<Vector3>();
            var spline = new List<Vector3>();

            var neighbourChunk = CoordUtil.GlobalPositionToMacroChunk(neighbour);

            if (macroNode.ContinuousRoads.ContainsKey(neighbour))
            {
                var neighbourContinuousPartner = macroNode.ContinuousRoads[neighbour];

                //This node has at least 1 continuous road and this neighbour is a part of one of them
                var p1 = GetNextRoadPosOnRoadAsync(neighbourChunk, neighbour, macroNode.GlobalPos);
                controlPoints.Add(neighbourContinuousPartner);
                controlPoints.Add(macroNode.GlobalPos);
                controlPoints.Add(neighbour);
                controlPoints.Add(p1);
                var c = controlPoints;
                var segmentCount = GetNumberOfSplineSegmentsBetween(c[1], c[2]);
                spline = CurveUtil.SegmentWithJoinedStartAndEnd(c[0], c[1], c[2], c[3], segmentCount, true);
            }
            else
            {
                //Neither end point is on the main road of this chunk so we add it twice
                var p5 = GetNextRoadPosOnRoadAsync(neighbourChunk, neighbour, macroNode.GlobalPos);
                controlPoints.Add(macroNode.GlobalPos);
                controlPoints.Add(macroNode.GlobalPos);
                controlPoints.Add(neighbour);
                controlPoints.Add(p5);

                var c = controlPoints;
                var segmentCount = GetNumberOfSplineSegmentsBetween(c[1], c[2]);
                spline = CurveUtil.SegmentWithJoinedStartAndEnd(c[0], c[1], c[2], c[3], segmentCount);
            }
            
            var roadSegment = new RoadSegment(macroNode.GlobalPos, neighbour, spline, Const.HIGHWAY_WIDTH_IN_METERS / 2);
            roadSegments.Add(roadSegment);
        }
        
        return roadSegments;
    }

    /// <summary>
    /// Returns the number of spline segments for a curve with given start and end points. Minimum value is 2.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static int GetNumberOfSplineSegmentsBetween(Vector3 start, Vector3 end)
    {
        var dist = (end - start).magnitude;
        //        DebugRenderer.Add(start, end, Color.black);
        var val = (int)(dist / (Const.HIGHWAY_WIDTH_IN_METERS) * 0.15f);
        return val > 1 ? val : 2;
    }

    /// <summary>
    /// Generate a macro road node ofr the given macro chunk.
    /// </summary>
    /// <param name="macroChunkPos"></param>
    /// <returns></returns>
    private static MacroRoadNode GenerateMacroRoadNodeAsync(Point2 macroChunkPos)
    {
        var roadGlobalPos = ChunkUtil.GetMacroChunkRoadGlobalPos(macroChunkPos);

        if (!roadGlobalPos.HasValue)
            return null;

        var neighbours = CalculateNeighboursAsync(macroChunkPos, roadGlobalPos.Value);

        var continuousRoads = GenerateContinuousRoadsAsync(roadGlobalPos.Value, neighbours);

        var neighbourControlPoints = new Dictionary<Vector3, List<Vector3>>();
        foreach (var neighbour in neighbours)
        {
            var controlPoints = new List<Vector3>();
            controlPoints.Add(roadGlobalPos.Value);
            controlPoints.Add(neighbour);
            neighbourControlPoints.Add(neighbour, controlPoints);
        }

        return new MacroRoadNode(macroChunkPos, roadGlobalPos.Value, neighbours, continuousRoads);
    }

    /// <summary>
    /// Generates pairs of continuous roads for the given list of edges, defined as centerPos - neighbour in list neighbours
    /// </summary>
    /// <param name="centerPos"></param>
    /// <param name="neighbours"></param>
    /// <returns></returns>
    private static Dictionary<Vector3, Vector3> GenerateContinuousRoadsAsync(Vector3 centerPos, List<Vector3> neighbours)
    {
        var continuousRoads = new Dictionary<Vector3, Vector3>();

        //Base cases
        if (neighbours.Count < 2)
        {
            return continuousRoads;
        }
        else if (neighbours.Count == 2)
        {
            continuousRoads.Add(neighbours[0], neighbours[1]);
            continuousRoads.Add(neighbours[1], neighbours[0]);
            return continuousRoads;
        }

        var candidates = new List<Vector3>(neighbours);

        var culls = 0;
        while (candidates.Count > 1)
        {
            var angle = 0f;
            var continuousRoad = ChunkUtil.FindMostParallelPoints(centerPos, candidates, out angle);
            if (angle > 60 && culls > 0)
            {
                //Stop early if next continuous roads would have a very sharp angle
                break;
            }

            continuousRoads.Add(candidates[continuousRoad.x], candidates[continuousRoad.z]);
            continuousRoads.Add(candidates[continuousRoad.z], candidates[continuousRoad.x]);

            if (continuousRoad.x > continuousRoad.z)
            {
                candidates.RemoveAt(continuousRoad.x);
                candidates.RemoveAt(continuousRoad.z);
            }
            else
            {
                candidates.RemoveAt(continuousRoad.z);
                candidates.RemoveAt(continuousRoad.x);
            }

            culls++;
        }
        return continuousRoads;
    }

    /// <summary>
    /// Calculates the list of neighbours in a macrochunk window for the given macrochunk.
    /// </summary>
    /// <param name="macroChunkPos"></param>
    /// <param name="roadGlobalPos">the macrochunk's highway node position</param>
    /// <returns></returns>
    private static List<Vector3> CalculateNeighboursAsync(Point2 macroChunkPos, Vector3 roadGlobalPos) {
        var neighbours = new List<Vector3>();
        var neighboursFurther = new List<Vector3>();

        #region generate all potential neighbours
        var r = Const.ROAD_FAR_NEIGHBOUR_RADIUS;
        for (int x = macroChunkPos.x - r; x <= macroChunkPos.x + r; x++) {
            for (int z = macroChunkPos.z - r; z <= macroChunkPos.z + r; z++) {
                var otherMacroChunkPos = new Point2(x, z);
                if (macroChunkPos == otherMacroChunkPos)
                    continue;

                var otherRoadGlobalPos = ChunkUtil.GetMacroChunkRoadGlobalPos(x, z);
                if (otherRoadGlobalPos.HasValue) {
                    if (Math.Abs(x - macroChunkPos.x) <= Const.ROAD_NEIGHBOUR_RADIUS
                        && Math.Abs(z - macroChunkPos.z) <= Const.ROAD_NEIGHBOUR_RADIUS) {
                        neighbours.Add(otherRoadGlobalPos.Value);
                    }
                    neighboursFurther.Add(otherRoadGlobalPos.Value);
                }

            }
        }

        #endregion

        #region cull unsuitable neighbours
        var remainingNeighbours = new List<Vector3>();
        foreach (var p2 in neighbours) {
            var p1 = roadGlobalPos;
            //We are looking at an edge p1-p2

            //1. Compare it to other edges for intersects
            bool intersects = ChunkUtil.Intersects(p1, p2, neighboursFurther);

            //2. At each edge p1-p2, check if the angle p3-p1-p2 or p3-p2-p1 is less than x degrees
            var thresh = Const.ROAD_TRIANGLE_MINIMUM_ANGLE;// + neighboursFurther.Count;
            bool acute = ChunkUtil.Acute(p1, p2, neighboursFurther, thresh);

            if (!intersects && acute)
                remainingNeighbours.Add(p2);
        }
        #endregion

        return remainingNeighbours;
    }

    /// <summary>
    /// Gets the next road position on a continuous road through road nodes with positions centerGlobalPos -> neighbourGlobalPos.
    /// </summary>
    /// <param name="centerMacroChunkPos"></param>
    /// <param name="centerGlobalPos"></param>
    /// <param name="neighbourGlobalPos"></param>
    /// <returns></returns>
    private static Vector3 GetNextRoadPosOnRoadAsync(Point2 centerMacroChunkPos, Vector3 centerGlobalPos, Vector3 neighbourGlobalPos) {
        //Guaranteed to exist
        // ReSharper disable once PossibleInvalidOperationException
        var roadGlobalPos = ChunkUtil.GetMacroChunkRoadGlobalPos(centerMacroChunkPos).Value;

        var neighbours = CalculateNeighboursAsync(centerMacroChunkPos, roadGlobalPos);
        //have to find the neighbour that is most parallel to center-neighbour

        if (neighbours.Count == 1) {
            //This chunk only has 1 neighbour, default to the input value to avoid any curvature
            return centerGlobalPos;
        }

        var continuousRoads = GenerateContinuousRoadsAsync(centerGlobalPos, neighbours);
        if (continuousRoads.ContainsKey(neighbourGlobalPos))
            return continuousRoads[neighbourGlobalPos];

        //The center-neighbour road is not on the main road of this chunk, meaning it's not affected by the main road
        //so just default to the input value to avoid any curvature
        return centerGlobalPos;

    }
    
    /// <summary>
    /// Returns a list of mesochunks on the given macrochunk that are closer to the line segment 'start-end' than 'offset' units.
    /// </summary>
    /// <param name="macroChunkPos"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="offset"></param>
    /// <returns></returns>
    public static List<Point2> GetIntersectingMesoChunks(Point2 macroChunkPos, Vector3 start, Vector3 end, float offset)
    {
        var chunks = new List<Point2>();
        var macroMin = CoordUtil.MacroChunkToGlobalPosition(macroChunkPos);
        var macroMax = CoordUtil.MacroChunkToGlobalPosition(macroChunkPos + new Point2(1, 1)) - Vector3.one;


        var minPos = Vector3.Max(macroMin, Vector3.Min(start, end));
        //TODO: Subtraction is bad, only necessary when the start-end is near chunk border
        var min = CoordUtil.GlobalPositionToMesoChunk(minPos) - new Point2(1, 1);
        var maxPos = Vector3.Min(macroMax, Vector3.Max(start, end));
        var max = CoordUtil.GlobalPositionToMesoChunk(maxPos) + new Point2(1, 1);

        for (int chunkX = min.x; chunkX <= max.x; chunkX++)
        {
            for (int chunkZ = min.z; chunkZ <= max.z; chunkZ++)
            {
                var w = Const.MESO_CHUNK_WIDTH_IN_METERS;
                var rect = new Rect(chunkX * w - offset / 2, chunkZ * w - offset / 2, w + offset, w + offset);
                //TODO: Optimize
                if (ChunkUtil.LineSegmentIntersectsRect(start, end, rect))
                {
                    chunks.Add(new Point2(chunkX, chunkZ));
                }
            }
        }
        return chunks;
    }

}
