﻿using System.Collections.Generic;

public class MacroChunkData {
    

    public MacroRoadData RoadData { get; private set; }

    public MesoChunkCityData[] MesoChunkCities { get; private set; }

    public List<City> Cities { get; private set; }

    public MacroChunkData(List<City> cities, MacroRoadData roadData, MesoChunkCityData[] mesoChunkCities)
    {
        Cities = cities;
        RoadData = roadData;
        MesoChunkCities = mesoChunkCities;
    }
}
