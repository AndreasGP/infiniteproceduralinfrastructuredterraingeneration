﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public class MacroRoadData
{
    
    /// <summary>
    /// Dictionary of roadsegments, mapped by central macro road node's neighbour road global positions
    /// </summary>
    public Dictionary<Vector3, RoadSegment> CentralRoadSegments { get; private set; }

    public List<RoadSegment> RoadSegments { get; private set; }
    
    public List<RoadSegment> IntersectingRoadSegments { get; private set; }

    public MacroRoadNode CentralMacroRoadNode { get; private set; }

    /// <summary>
    /// Dictionary of macroroadnodes, mapped by corresponding macroroadnode positions
    /// </summary>
    public Dictionary<Point2, MacroRoadNode> MacroRoadNodes { get; private set; }

    public List<IntPoint> CenterRoadOutline { get; private set; }

    public List<IntPoint> CenterShoulderOutline { get; private set; }

    public Vector3?[] MesoChunkRoadGlobalPositions { get; private set; }

    public bool[] RoadCoverage { get; private set; }

    public MacroRoadData(Dictionary<Vector3, RoadSegment> centralRoadSegments, List<RoadSegment> roadSegments, List<RoadSegment> intersectingRoadSegments, MacroRoadNode centralMacroRoadNode, 
        Dictionary<Point2, MacroRoadNode> macroRoadNodes, List<IntPoint> centerRoadOutline, List<IntPoint> centerShoulderOutline, bool[] roadCoverage)
    {
        CentralRoadSegments = centralRoadSegments;
        RoadSegments = roadSegments;
        IntersectingRoadSegments = intersectingRoadSegments;
        CentralMacroRoadNode = centralMacroRoadNode;
        MacroRoadNodes = macroRoadNodes;
        CenterRoadOutline = centerRoadOutline;
        CenterShoulderOutline = centerShoulderOutline;
        RoadCoverage = roadCoverage;
    }

    public void SetMesoChunkRoadGlobalPositions(Vector3?[] globalPositions)
    {
        MesoChunkRoadGlobalPositions = globalPositions;
    }

    public bool MesoChunkContainsHighway(Point2 mesoChunk)
    {
        var localMesoChunk = CoordUtil.GlobalMesoChunkToLocalMesoChunk(mesoChunk);
        return RoadCoverage[localMesoChunk.z * Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS + localMesoChunk.x];
    }
}
