﻿using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

//NB: MacroChunk is assumed not to change any internal state once Ready has been set true.
public class MacroChunk {

    public Point2 Pos { get; private set; }
    
    public MacroChunkData Data { get; private set; }

    public bool Ready { get; private set; }

    public bool MarkedForDeletion { get; private set; }

    public Rect Rect { get; private set; }

    public List<IntPoint> ChunkRectOutline { get; private set; }

    /// <summary>
    /// Slightly bigger rect that can be used to check for collisions against roads that dont have a dilated outline yet
    /// </summary>
    public Rect RoadRect { get; private set; }

    public MacroChunk(Point2 macroChunkPos)
    {
        Pos = macroChunkPos;
        Rect = ChunkUtil.GetMacroChunkRect(macroChunkPos.x, macroChunkPos.z);
        ChunkRectOutline = ChunkUtil.GetRectAsIntPoints(Rect);
        RoadRect = ChunkUtil.GetMacroChunkRect(macroChunkPos.x, macroChunkPos.z, Const.HIGHWAY_SHOULDER_WIDTH_IN_METERS);
        Ready = false;
    }
    
    public void GenerateAsync()
    {
        //Generate intersecting highways
        var roadGenerator = new MacroChunkRoadGenerator(this);
        var roadData = roadGenerator.GenerateRoadDataAsync();

        //Generate any overlapping cities using the highway data
        var cityGenerator = new MacroChunkCityGenerator(this, roadData);
        var cities = cityGenerator.GenerateCitiesAsync();
        
        //Generate meso chunk city data
        var mesoChunkCityData = cityGenerator.CalculatePerMesoChunkRegionsAsync(cities);

        //Generate meso chunk road global tiles using the cities
        var mesoChunkRoadGlobalPositions = roadGenerator.GenerateCoverageAndMesoChunkRoadGlobalPositionsAsync(roadData.RoadCoverage, roadData.IntersectingRoadSegments, mesoChunkCityData);
        roadData.SetMesoChunkRoadGlobalPositions(mesoChunkRoadGlobalPositions);
        
        Data = new MacroChunkData(cities, roadData, mesoChunkCityData);

        Ready = true;
    }

    public void MarkForDeletion() {
        MarkedForDeletion = true;
    }

    private float[] GenerateNoiseAsync()
    {
        var noise = new float[Const.MACRO_CHUNK_AREA_IN_MICRO_CHUNKS];
        var w = Const.MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS;
        var offset = CoordUtil.MacroChunkToGlobalPosition(Pos);
        for (int x = 0; x < w; x++)
        {
            for (int z = 0; z < w; z++)
            {
                var i = z * w + x;
                var noiseX = offset.x + x * Const.MICRO_CHUNK_WIDTH_IN_METERS;
                var noiseZ = offset.z + z * Const.MICRO_CHUNK_WIDTH_IN_METERS;
                var val = NoiseService.GetNoiseAtPos(noiseX, noiseZ);
                noise[i] = val;
            }
        }
        return noise;
    }
    
    public bool InsideCity(Vector3 globalPos)
    {
        if (Data.Cities == null || Data.Cities.Count == 0)
            return false;

        foreach (var city in Data.Cities)
        {
            if (ChunkUtil.InsideSimplePolygon(globalPos, city.OutterOutline))
                return true;
        }
        return false;
    }

    public MesoChunkCityData GetMesoChunkCityData(Point2 mesoChunkPos)
    {
        var localPos = CoordUtil.GlobalMesoChunkToLocalMesoChunk(mesoChunkPos);
        var i = localPos.z * Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS + localPos.x;
        return Data.MesoChunkCities[i];
    }
    
    public static Vector3? GetMesoChunkRoadGlobalPosition(Point2 mesoChunkPos)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;

        var macroChunkPos = CoordUtil.MesoChunkToMacroChunk(mesoChunkPos);
        //TODO Parallelize this
        var macroChunk = ChunkManager.Ins.GetOrGenerateMacroChunk(macroChunkPos);
        var local = CoordUtil.GlobalMesoChunkToLocalMesoChunk(mesoChunkPos);
        var i = local.z * w + local.x;
        return macroChunk.Data.RoadData.MesoChunkRoadGlobalPositions[i];
    }

    public static Vector3? GetMesoChunkRoadGlobalPosition(Point2 mesoChunkPos, out bool onHighway)
    {
        var macroChunkPos = CoordUtil.MesoChunkToMacroChunk(mesoChunkPos);
        var macroChunk = ChunkManager.Ins.GetOrGenerateMacroChunk(macroChunkPos);

        return GetMesoChunkRoadGlobalPosition(mesoChunkPos, macroChunk.GetMesoChunkCityData(mesoChunkPos), out onHighway);
    }

    public static Vector3? GetMesoChunkRoadGlobalPosition(Point2 mesoChunkPos, MesoChunkCityData cityData, out bool onHighway)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;

        var macroChunkPos = CoordUtil.MesoChunkToMacroChunk(mesoChunkPos);
        //TODO Parallelize this
        var macroChunk = ChunkManager.Ins.GetOrGenerateMacroChunk(macroChunkPos);
        var local = CoordUtil.GlobalMesoChunkToLocalMesoChunk(mesoChunkPos);
        var i = local.z * w + local.x;

        onHighway = macroChunk.Data.RoadData.RoadCoverage[i];
        return macroChunk.Data.RoadData.MesoChunkRoadGlobalPositions[i];
    }

}
