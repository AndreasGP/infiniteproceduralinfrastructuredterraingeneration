﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MacroRoadNode
{

    public Point2 MacroChunkPos { get; private set; }

    public Vector3 GlobalPos { get; private set; }

    public List<Vector3> Neighbours { get; private set; }

    /// <summary>
    /// /A collection of road pairs that should be continuous through this road node.
    /// </summary>
    public Dictionary<Vector3, Vector3> ContinuousRoads { get; private set; }

    public MacroRoadNode(Point2 macroChunkPos, Vector3 globalPos, List<Vector3> neighbours, Dictionary<Vector3, Vector3> continuousRoads)
    {
        MacroChunkPos = macroChunkPos;
        GlobalPos = globalPos;
        Neighbours = neighbours;
        ContinuousRoads = continuousRoads;
    }

}