﻿using System;
using UnityEngine;

public class MacroChunkWrapper : MonoBehaviour {

    public MacroChunk Chunk { get; private set; }

    public Action ChunkInitialized;

    public void Initialize(MacroChunk chunk)
    {
        Chunk = chunk;
        name = "MacroChunk " + chunk.Pos;
        if(ChunkInitialized != null)
            ChunkInitialized();
    }
    

}
