﻿using System.Collections;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public class DebugRenderer : MonoBehaviour
{

    public static DebugRenderer Ins;

    public List<DebugLine> DebugLines { get; private set; }

    private System.Random rand;

	void Awake ()
	{
	    Ins = this;
        DebugLines = new List<DebugLine>();
	    rand = new System.Random(42);
	}

    public static void Add(Vector2 p1, Vector2 p2, Color color)
    {
        lock (Ins.DebugLines)
        {
            Ins.DebugLines.Add(new DebugLine(ChunkUtil.Vec2ToVec3(p1), ChunkUtil.Vec2ToVec3(p2), color));
        }
    }

    public static void Add(Vector3 p1, Vector3 p2, Color color)
    {
        lock (Ins.DebugLines)
        {
            Ins.DebugLines.Add(new DebugLine(p1, p2, color));
        }
    }

    public static void Add(Point2 p1, Point2 p2, Color color)
    {
        Add(p1, p2, color, true);
    }


    public static void Add(Point2 p1, Point2 p2, Color color, bool convertFromGlobalTiles)
    {
        lock (Ins.DebugLines)
        {
            if (convertFromGlobalTiles)
                Ins.DebugLines.Add(new DebugLine(CoordUtil.GlobalTileToGlobalPosition(p1),
                    CoordUtil.GlobalTileToGlobalPosition(p2), color));
            else
                Ins.DebugLines.Add(new DebugLine(new Vector3(p1.x, 0, p1.z), new Vector3(p2.x, 0, p2.z), color));
        }
    }


    public static void Add(IntPoint p1, IntPoint p2, Color color)
    {
        lock (Ins.DebugLines)
        {
            Ins.DebugLines.Add(new DebugLine(p1.ToVec3(), p2.ToVec3(), color));
        }
    }

    public static void AddMark(Vector3 p, float radius, Color color)
    {
        lock (Ins.DebugLines)
        {
            Ins.DebugLines.Add(new DebugLine(p + new Vector3(radius, 0, radius), p - new Vector3(radius, 0, radius), color));
            Ins.DebugLines.Add(new DebugLine(p + new Vector3(-radius, 0, radius), p - new Vector3(-radius, 0, radius), color));
        }
    }

    public static void AddMark(IntPoint p, float radius, Color color)
    {
        lock (Ins.DebugLines)
        {
            Ins.DebugLines.Add(new DebugLine(p.ToVec3() + new Vector3(radius, 0, radius), p.ToVec3() - new Vector3(radius, 0, radius), color));
            Ins.DebugLines.Add(new DebugLine(p.ToVec3() + new Vector3(-radius, 0, radius), p.ToVec3() - new Vector3(-radius, 0, radius), color));
        }
    }


    void OnDrawGizmos ()
	{
	    if (!Application.isPlaying)
	        return;

	    lock (DebugLines)
	    {
	        foreach (var line in DebugLines)
	        {
	            Gizmos.color = line.color;
	            Gizmos.DrawLine(transform.position + line.point1, transform.position + line.point2);
	        }
	    }
	}

    public static Color GetRandomColor(float alpha)
    {
        lock (Ins.rand)
        {
            return new Color((float)Ins.rand.NextDouble(), (float)Ins.rand.NextDouble(), (float)Ins.rand.NextDouble(), alpha);
        }
    }

    public static Color GetRandomColor(float minChannel, float alpha)
    {
        lock (Ins.rand)
        {
            return new Color(minChannel + (float)Ins.rand.NextDouble() * (1 - minChannel), minChannel + (float)Ins.rand.NextDouble() * (1 - minChannel), minChannel + (float)Ins.rand.NextDouble() * (1 - minChannel), alpha);
        }
    }
}
