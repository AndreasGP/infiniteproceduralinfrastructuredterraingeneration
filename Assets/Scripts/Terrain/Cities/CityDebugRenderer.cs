﻿using UnityEngine;

[RequireComponent(typeof(CityWrapper))]
public class CityDebugRenderer : MonoBehaviour {

    private CityWrapper cityWrapper;

    private City city;

    private void Awake()
    {
        cityWrapper = GetComponent<CityWrapper>();
        cityWrapper.CityInitialized += CityInitialized;
    }

    void CityInitialized()
    {
        city = cityWrapper.City;

        //GenerateMesh();
    }

    void OnDrawGizmos() {
        DrawGizmos(SettingsLevel.Always);
    }

    void OnDrawGizmosSelected() {
        DrawGizmos(SettingsLevel.WhenSelected);
    }

    private void DrawGizmos(SettingsLevel level)
    {
        if (!Application.isPlaying)
            return;

        if (Settings.DrawCityCenters == level && city.Centers != null)
        {
            Gizmos.color = new Color(0, 0, 0, 1);

            foreach (var center in city.Centers)
            {
                Gizmos.DrawSphere(center, 50);
            }
        }

        if (Settings.DrawCityBorders == level && city.OutterOutline != null)
        {
            Gizmos.color = new Color(0, 0, 0, 1);
            for (int i = 0; i < city.OutterOutline.Count; i++)
            {
                var p1 = city.OutterOutline[i];
                var p2 = city.OutterOutline[(i + 1) % city.OutterOutline.Count];

                var dir = (p2 - p1).normalized;
                var right = new Vector3(dir.z, 0, -dir.x);

                for (int j = -5; j <= 5; j++)
                {
                    Gizmos.DrawLine(p1 + right * j, p2 + right * j);
                }
            }
        }
    }
}
