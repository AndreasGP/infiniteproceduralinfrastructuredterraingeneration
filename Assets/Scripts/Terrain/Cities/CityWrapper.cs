﻿using System;
using UnityEngine;

public class CityWrapper : MonoBehaviour {

    public City City { get; private set; }

    public Action CityInitialized;

    public void Initialize(City city)
    {
        City = city;
        transform.position = City.Center;
        name = "City " + city.MacroChunkPos;

        if (CityInitialized != null)
            CityInitialized();
    }
    

}
