﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ClipperLib;
using UnityEngine;


public enum CityStatus
{
    OnlyOutline,
    Ready
}

public enum CityAreaType
{
    Commercial,
    Industral,
    LowResidential,
    HighResidential,
}

public class City
{
    public Rect Rect { get; private set; }

    public string Name { get; private set; }

    public CityStatus Status { get; private set; }

    public Point2 MacroChunkPos { get; private set; }

    /// <summary>
    /// Main center (index 0) + sub centers of the city that were used to generate the outline of the city
    /// </summary>
    public List<Vector3> Centers { get; private set; }

    public List<Vector3> AreaCenters { get; private set; }

    public List<CityAreaType> AreaCenterTypes { get; private set; }
    
    public List<Vector3> InnerOutline { get; private set; }

    public List<Vector3> OutterOutline { get; private set; }

    public List<IntPoint> InnerPointOutline { get; private set; }

    public List<IntPoint> OutterPointOutline { get; private set; }

    public Vector3 Center { get { return Centers[0]; } }
    
    public City(Point2 macroChunkPos, List<Vector3> centers, List<Vector3> innerOutline, List<IntPoint> innerPointOutline, List<Vector3> outterOutline, List<IntPoint> outterPointOutline)
    {
        Status = CityStatus.OnlyOutline;
        MacroChunkPos = macroChunkPos;
        Name = SettlementNameGenerator.GenerateName(centers[0]);
        Centers = centers;
        InnerOutline = innerOutline;
        InnerPointOutline = innerPointOutline;
        OutterOutline = outterOutline;
        OutterPointOutline = outterPointOutline;
        Rect = ChunkUtil.GetPointsRect(outterOutline);
        
        GenerateAreaCentersAndTheirTypes();
    }

    private void GenerateAreaCentersAndTheirTypes()
    {
        AreaCenters = new List<Vector3>();
        AreaCenterTypes = new List<CityAreaType>();

        //main center (first in centers) is marked as commercial
        AreaCenters.Add(Centers[0]);
        AreaCenterTypes.Add(CityAreaType.Commercial);

        var lastEdgeTypeWasIndustrial = true;

        if (Centers.Count > 1)
        {
            //Closest one to the city center is found
            var closestIndex = 1;
            var closestDist = 999999f;
            for (int i = 1; i < Centers.Count; i++)
            {
                var dist = (Centers[0] - Centers[i]).magnitude;
                if (dist < closestDist)
                {
                    closestDist = dist;
                    closestIndex = i;
                }
            }

            for (int i = 1; i < Centers.Count; i++)
            {
                if (i == closestIndex)
                {
                    //Center[i] is the closest subcenter to the city center
                    AreaCenters.Add(Centers[i]);
                    AreaCenterTypes.Add(CityAreaType.Commercial);
                    
                    AreaCenters.Add(Centers[i] + (Centers[i] - Centers[0]) * 1.2f);
                    AreaCenterTypes.Add(CityAreaType.Industral);


                    AreaCenters.Add(Centers[i] - (Centers[i] - Centers[0]) * 1.2f);
                    AreaCenterTypes.Add(CityAreaType.HighResidential);
                }
                else
                {
                    AreaCenters.Add(Centers[i]);
                    AreaCenterTypes.Add(CityAreaType.HighResidential);
                    
                    //Further one is 50:50 LowResidential or Industrial
                    AreaCenters.Add(Centers[i] + (Centers[i] - Centers[0]) * 0.5f);
                    AreaCenterTypes.Add(lastEdgeTypeWasIndustrial ? CityAreaType.LowResidential : CityAreaType.Industral);
                    lastEdgeTypeWasIndustrial = !lastEdgeTypeWasIndustrial;
                }
            }
        }
    }


    public void SetStatus(CityStatus status)
    {
        Status = status;
    }

    public CityAreaType GetClosestCityCenterType(Vector3 pos)
    {
        var closestIndex = 0;
        var closestDist = 999999f;
        for (int i = 0; i < AreaCenterTypes.Count; i++)
        {
            var diff = (pos - AreaCenters[i]);
            var noise = (NoiseService.GetNoiseAtPos((pos + AreaCenters[i]) * 256f) - 0.5f) * 750f;
            if (diff.x > diff.z)
                noise *= -1;

            var dist = diff.magnitude + noise;
            if (dist < closestDist)
            {
                closestDist = dist;
                closestIndex = i;
            }
        }

        return AreaCenterTypes[closestIndex];
    }
}
