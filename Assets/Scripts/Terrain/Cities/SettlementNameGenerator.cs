﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ClipperLib;
using UnityEngine;


public static class SettlementNameGenerator
{
    
    public static string GenerateName(Vector3 globalPosition)
    {
        var hash = ChunkUtil.Hash((int) (globalPosition.x * 97f), (int) (globalPosition.z * 97f));

        if (hash % 100 < 80)
            return GenerateCombinedName(globalPosition);

        return GenerateShortName(globalPosition);
    }

    private static string GenerateCombinedName(Vector3 globalPosition)
    {

        var hash1 = ChunkUtil.Hash((int)(globalPosition.x * 73f), (int)(globalPosition.z * 73f));
        var prefix = combinedNamesPrefixes[hash1 % combinedNamesPrefixes.Length];
        var hash2 = ChunkUtil.Hash((int)(globalPosition.x * 89f), (int)(globalPosition.z * 89f));
        var suffix = combinedNamesSuffixes[hash2 % combinedNamesSuffixes.Length];
        return prefix + suffix;
    }

    private static string GenerateShortName(Vector3 globalPosition)
    {
        var hash1 = ChunkUtil.Hash((int)(globalPosition.x * 73f), (int)(globalPosition.z * 73f));
        var prefix = shortNamePrefixes[hash1 % shortNamePrefixes.Length];
        var hash2 = ChunkUtil.Hash((int)(globalPosition.x * 89f), (int)(globalPosition.z * 89f));
        var suffix = shortNameSuffixes[hash2 % shortNameSuffixes.Length];
        return prefix + suffix;
    }


    private static string[] combinedNamesPrefixes =
    {
        "Valge",
        "Must",
        "Põllu",
        "Saan",
        "Pai",
        "Palu",
        "Kolu",
        "Ada",
        "Ima",
        "Kure",
        "Tam",
        "Lusti",
        "Väike",
        "Suure",
        "Vana",
        "Kohtla",
        "Kilingi-",
        "Paun",
        "Silla",
        "Vastse",
        "Metsa",
        "Jaama",
        "Koera",
        "Soo",
        "Männi",
        "Harju",
        "Savi",
        "Pika",
        "Aru",
        "Kassi",
        "Järva",
        "Pudu",
        "Taga",
        "Uue",
        "Mere",
    };

    private static string[] combinedNamesSuffixes =
    {
        "salu",
        "saare",
        "järve",
        "nõmme",
        "jaani",
        "vere",
        "metsa",
        "jõe",
        "vee",
        "oru",
        "küla",
        "kivi",
        "la",
        "mäe",
        "linna",
        "laane",
        "palu",
        "lepa",
        "kase",
        "oja",
        "maa",
        "mõisa",
        "jala",
        "välja",
        "soo",
        "nurme",
        "nurga",
        "ranna",
        "taguse"
    };

    private static string[] shortNamePrefixes =
    {
        "val",
        "tar",
        "tal",
        "nar",
        "kur",
        "kaba",
        "pär",
        "rap",
        "räp",
        "sal",
        "põr",
    };

    private static string[] shortNameSuffixes =
    {
        "linn",
        "ge",
        "va",
        "ste",
        "na",
        "sa",
        "la",
        "si",
        "nja"
    };
}
