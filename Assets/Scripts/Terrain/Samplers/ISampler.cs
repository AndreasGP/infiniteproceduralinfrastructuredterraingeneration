﻿using System.Collections;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;


public interface ISampler { 

    IEnumerable<Vector3> Samples();

}
