﻿using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public struct CityBuildingMetaData
{
    public List<IntPoint> PointOutline { get; private set; }

    public float Height { get; private set; }

    public int MaterialIndex { get; private set; }

    public CityBuildingMetaData(List<IntPoint> pointOutline, float height, int materialIndex) : this()
    {
        PointOutline = pointOutline;
        Height = height;
        MaterialIndex = materialIndex;
    }
}

public class MesoChunkCityData {

    public City City { get; private set; }

    public List<List<IntPoint>> CityPolys { get; private set; }

    public bool CityOnly { get; private set; }
    
    public List<List<IntPoint>> CityBlocks { get; private set; }

    public List<CityBuildingMetaData> CityBuildings { get; private set; }

    public MesoChunkCityData(City city, List<List<IntPoint>> cityPolys, bool cityOnly)
    {
        City = city;
        CityPolys = cityPolys;
        CityOnly = cityOnly;
    }

    public void SetCityBlocks(List<List<IntPoint>> blocks)
    {
        CityBlocks = blocks;
    }

    public void SetCityBuildings(List<CityBuildingMetaData> buildings)
    {
        CityBuildings = buildings;
    }

    public bool PointInsideCity(Vector3 point)
    {
        if (City == null)
            return false;
        if (CityOnly)
            return true;

        foreach (var poly in CityPolys)
        {
            if (ChunkUtil.InsideSimplePolygon(new IntPoint(point), poly))
                return true;
        }

        return false;
    }
}
