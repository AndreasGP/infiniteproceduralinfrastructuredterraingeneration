﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;

public class MesoChunkVillageGenerator
{
    private MesoChunk mesoChunk;

    private Point2 mesoChunkPos;

    private MacroChunk macroChunk;

    private MesoRoadData mesoRoadData;

    private MesoChunkRegion villageRegion;

    private bool churchPlaced;

    public MesoChunkVillageGenerator(MesoChunk mesoChunk, MesoRoadData mesoRoadData, MesoChunkRegion villageRegion)
    {
        this.mesoChunk = mesoChunk;
        this.mesoRoadData = mesoRoadData;
        this.villageRegion = villageRegion;
        macroChunk = mesoChunk.MacroChunk;
        mesoChunkPos = mesoChunk.Pos;
    }

    /// <summary>
    /// Generates village data of the given chunk if a village exists.
    /// </summary>
    /// <returns></returns>
    public MesoChunkVillageData GenerateVillageDataAsync()
    {
        //Trigger generation of crosswalks
        if(mesoRoadData.CentralMesoRoadNode.Neighbours.Count > 1)
            MesoChunkRoadGenerator.GenerateCrosswalks(mesoRoadData);

        var random = ChunkUtil.GetMesoChunkRandom(mesoChunkPos.x + 100, mesoChunkPos.z + 100);
        
        var roadPolys = mesoRoadData.RoadPolys;
        var shoulderPolys = mesoRoadData.ShoulderPolys;

        var unpavedRoadEdges = CalculateVillageUnpavedRoadEdges(roadPolys);

        var streetSegments = new List<StreetSegment>();

        var regionClipper = new Clipper();
        var plotClipper = new Clipper();
        var roadClipper = new Clipper();
        var roadPolysWithVillageRoads = new List<List<IntPoint>>();
        roadPolysWithVillageRoads.AddRange(roadPolys);
        var shoulderPolysWithVillageRoads = new List<List<IntPoint>>();
        shoulderPolysWithVillageRoads.AddRange(shoulderPolys);

        foreach (var unpavedRoadEdge in unpavedRoadEdges)
        {
            var v1 = unpavedRoadEdge.x.ToVec3();
            var v2 = unpavedRoadEdge.z.ToVec3();

            var r = Const.UNPAVED_ROAD_WIDTH_IN_METERS / 2;
            var extraLengthOffset = Const.UNPAVED_ROAD_WIDTH_IN_METERS;

            var fwd = (v2 - v1).normalized;
            var right = new Vector3(fwd.z, 0, -fwd.x);

            var initialPoly = new List<IntPoint>();
            initialPoly.Add(new IntPoint(v2 - right * r + fwd * extraLengthOffset));
            initialPoly.Add(new IntPoint(v1 - right * r - fwd * extraLengthOffset));
            initialPoly.Add(new IntPoint(v1 + right * r - fwd * extraLengthOffset));
            initialPoly.Add(new IntPoint(v2 + right * r + fwd * extraLengthOffset));

            //TODO: Comment (its to prevent ground tearing but have proper polys)
            var initialPoly2 = new List<IntPoint>();
            initialPoly2.Add(new IntPoint(v2 - right * r + fwd * extraLengthOffset / 2));
            initialPoly2.Add(new IntPoint(v1 - right * r - fwd * extraLengthOffset / 2));
            initialPoly2.Add(new IntPoint(v1 + right * r - fwd * extraLengthOffset / 2));
            initialPoly2.Add(new IntPoint(v2 + right * r + fwd * extraLengthOffset / 2));

            regionClipper.Clear();
//            regionClipper.AddPath(villageRegion.FullPointOutline, PolyType.ptClip, true);
            regionClipper.AddPath(initialPoly, PolyType.ptSubject, true);
            var chunkBoundPolys = new List<List<IntPoint>>();
            regionClipper.Execute(ClipType.ctUnion, chunkBoundPolys, PolyFillType.pftNonZero,
                PolyFillType.pftNonZero);

            roadClipper.Clear();
            roadClipper.AddPaths(roadPolys, PolyType.ptClip, true);
            roadClipper.AddPaths(chunkBoundPolys, PolyType.ptSubject, true);
            var streetPolys = new List<List<IntPoint>>();
            roadClipper.Execute(ClipType.ctDifference, streetPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);


            shoulderPolysWithVillageRoads.Add(initialPoly2);

            foreach (var streetPoly in streetPolys)
            {
                if (ChunkUtil.PolygonArea(streetPoly) < 30)
                    continue;


                roadPolys.Add(streetPoly);
                roadPolysWithVillageRoads.Add(streetPoly);

                var segment = new StreetSegment(streetPoly, ClipperUtil.ToVectors(streetPoly), v1 + right * r - fwd * extraLengthOffset, v1 - right * r - fwd * extraLengthOffset, true);
                streetSegments.Add(segment);
            }
        }
        mesoRoadData.SetStreetSegments(streetSegments);

        mesoRoadData.SetRoadAndStreetPolys(roadPolysWithVillageRoads);

        plotClipper.AddPath(villageRegion.PointOutline, PolyType.ptSubject, true);
        plotClipper.AddPaths(shoulderPolysWithVillageRoads, PolyType.ptClip, true);

        var initialPlotPolys = new List<List<IntPoint>>();
        plotClipper.Execute(ClipType.ctDifference, initialPlotPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

        var subregions = new List<MesoChunkRegion>();
        //Generate roadside meshes
        regionClipper.Clear();
        regionClipper.AddPath(villageRegion.PointOutline, PolyType.ptSubject, true);
        regionClipper.AddPaths(shoulderPolys, PolyType.ptClip, true);
        var roadSidePolys = new List<List<IntPoint>>();
        regionClipper.Execute(ClipType.ctIntersection, roadSidePolys, PolyFillType.pftNonZero,
            PolyFillType.pftNonZero);
        foreach(var roadSidePoly in roadSidePolys)
        {
            var polyVecs = ClipperUtil.ToVectors(roadSidePoly);
            var subregion = new MesoChunkRegion(RegionType.Plain, roadSidePoly, polyVecs, true);
            subregions.Add(subregion);
        }

        var offsetDist = 4;
        
        var blockPolys = new List<List<IntPoint>>();
        foreach (var initialPlotPoly in initialPlotPolys)
        {
            var area = ChunkUtil.PolygonArea(initialPlotPoly);
            if (area < 400)
            {
                var polyVecs = ClipperUtil.ToVectors(initialPlotPoly);
                var subregion = new MesoChunkRegion(RegionType.Plain, initialPlotPoly, polyVecs,
                    true);
                subregions.Add(subregion);
                continue;
            }

            blockPolys.Add(initialPlotPoly);
        }

        var terrainPopulator = new MesoChunkTerrainPopulator();

        var subdivideClipper = new Clipper();
        foreach (var blockPoly in blockPolys)
        {
            var blockPolyVec = ClipperUtil.ToVectors(blockPoly);
            var plotPolys = new List<List<IntPoint>>();

            SubdividePolygon(subdivideClipper, blockPoly, plotPolys, 2);

            foreach (var plotPoly in plotPolys)
            {

                var terrainObjects = new List<TerrainObjectMetaData>();

                var edgesNextToRoad = 0;
                
                var plotPolyVecs = ClipperUtil.ToVectors(plotPoly);

                var housePlaced = false;
                List<IntPoint> housePointOutline = null;

                for (int i = 0; i < plotPolyVecs.Count; i++)
                {
                    var v1 = plotPolyVecs[i];
                    var v2 = plotPolyVecs[(i + 1) % plotPolyVecs.Count];

                    var distFromVillageBorder1 = ChunkUtil.PointDistanceFromPolygon(v1, villageRegion.Outline);
                    var distFromVillageBorder2 = ChunkUtil.PointDistanceFromPolygon(v2, villageRegion.Outline);

                    var distFromBlockBorder1 = ChunkUtil.PointDistanceFromPolygon(v1, blockPolyVec);
                    var distFromBlockBorderMid = ChunkUtil.PointDistanceFromPolygon(v1 + (v2 - v1) * 0.5f, blockPolyVec);
                    var distFromBlockBorder2 = ChunkUtil.PointDistanceFromPolygon(v2, blockPolyVec);


                    if ((distFromVillageBorder1 < offsetDist * 1.5f && distFromVillageBorder2 < offsetDist * 1.5f) 
                        || distFromBlockBorder1 > 0.1f || distFromBlockBorder2 > 0.1f || distFromBlockBorderMid > 2f)
                    {
                        //Not a suitable edge for a house because it's not close to a road
//                        DebugRenderer.Add(v1, v2, Color.red);
                    }
                    else
                    {
                        //Suitable edge for a house by distance from roads and cell border
//                        DebugRenderer.Add(v1, v2, Color.white);
                        edgesNextToRoad++;

                        if ((v1 - v2).magnitude > 20)
                        {
                            //Edge is at least 20m long, consider it possibly suitable for house placement

                            var dir = (v1 - v2).normalized;
                            var right = new Vector3(dir.z, 0, -dir.x);

                            var housePos = v1 + (v2 - v1) * 0.5f + right * 8.5f;
                            var houseRotationRad = Mathf.Atan2(right.z, right.x);

                            var placingChurch = false;

                            var width = 12f;
                            var height = 12f;

                            if (!churchPlaced && (housePos - mesoRoadData.CentralMesoRoadNode.GlobalPos).magnitude < 50)
                            {
                                placingChurch = true;
                                housePos += right * 2f;

                                width = 18f;
                            }

                            var gatePos = v1 + (v2 - v1) * 0.5f + right;

                            if (ChunkUtil.IsValidHousePosition(housePos, houseRotationRad, plotPolyVecs, width, height, out housePointOutline))
                            {

                                if (placingChurch)
                                {
                                    var minHeight = (float) Const.HEIGHTMAP_RANGE_IN_METERS;
                                    foreach (var p in housePointOutline)
                                    {
                                        var h = mesoChunk.GetHeightAt(p.ToVec3());
                                        if (h < minHeight)
                                            minHeight = h;
                                    }

                                    housePos.y = minHeight - 0.2f;
                                }
                                else
                                {
                                    housePos.y = mesoChunk.GetHeightAt(housePos);
                                }

                                var housePrefabs = AssetManager.Ins.HousePrefabs;
                                var modelIndex = random.Next(0, Int32.MaxValue) % housePrefabs.Count;
                                var prefab = housePrefabs[modelIndex];
                                var scale = 1f;
   
                                if(placingChurch) {
                                    prefab = AssetManager.Ins.ChurchPrefab;
                                    scale = 2.5f;
                                    houseRotationRad -= Mathf.PI / 2;
                                    churchPlaced = true;
                                }

                                var obj = TerrainObjectMetaData.Generate(prefab, housePos, 180-houseRotationRad * Mathf.Rad2Deg, scale, 0, 0);
                                terrainObjects.Add(obj);

                                if (random.NextDouble() < 0.8f)
                                {
                                    ChunkUtil.GenerateFenceTerrainObjects(mesoChunk, plotPolyVecs, terrainObjects,
                                        gatePos, 2);
                                }

                                housePlaced = true;
                                break;
                            }
                        }
                    }
                }

                var type = RegionType.SubResidenceWithTrees;
                var typeRandom = random.NextDouble();
                if (typeRandom < 0.25)
                    type = RegionType.SubResidenceWithBushes;
                else if (typeRandom < 0.5)
                    type = RegionType.SubResidenceWithTreesAndBushes;
                else if (typeRandom < 0.75)
                    type = RegionType.SubResidenceWithSparsePlants;

                var edgeRatio = edgesNextToRoad / (float) plotPoly.Count;

                if (!housePlaced && (edgeRatio < 0.17f || (edgeRatio > 0f && edgeRatio < 0.35f && random.NextDouble() < 0.8)))
                {
                    typeRandom = random.NextDouble();
                    if (typeRandom < 0.33)
                        type = RegionType.SubMixedForest;
                    else if (typeRandom < 0.66)
                        type = RegionType.SubDeciduousForestNatural;
                    else
                        type = RegionType.SubConiferousForestNatural;
                }

                var subregion = new MesoChunkRegion(type, plotPoly, plotPolyVecs,
                    true);
                subregions.Add(subregion);
                subregion.SetTerrainObjectMetaDatas(terrainObjects);

                if (housePlaced)
                {
                    subregion.SetIgnorableOutlines(new List<List<Vector3>> { ClipperUtil.ToVectors(housePointOutline) });
                }

                terrainPopulator.GenerateSubregionTerrainObjectsAsync(mesoChunk, subregion, 0.8f);
            }
        }

        villageRegion.SetSubRegions(subregions);

        var villageAreas = new List<VillageArea>();

        return new MesoChunkVillageData(mesoChunk, villageRegion, villageAreas);
    }

    /// <summary>
    /// Subdivides th given polygon 'points' until depth 0 is reached.
    /// </summary>
    /// <param name="clipper"></param>
    /// <param name="points"></param>
    /// <param name="outPolygons"></param>
    /// <param name="depth"></param>
    private void SubdividePolygon(Clipper clipper, List<IntPoint> points, List<List<IntPoint>> outPolygons, int depth)
    {

        var vertices = ClipperUtil.ToVectors(points);
        if (depth == 0 || ChunkUtil.PolygonArea(points) < 5000)
        {
            outPolygons.Add(points);
            return;
        }

        var longestEdgeI = ChunkUtil.LongestEdge(points);
        var centroid = ChunkUtil.CentroidOfPolygon(vertices);

        var start = vertices[longestEdgeI];
        var end = vertices[(longestEdgeI + 1) % vertices.Count];

        var clipPoint1 = ChunkUtil.GetPointProjectionOnLine(centroid, start, end);

        var clipPoint2 = centroid;

        CutPoly(clipper, points, clipPoint1, clipPoint2, outPolygons, depth);
    }

    /// <summary>
    /// Cuts the polygon 'points' with a line through start and end until depth 0 is reached.
    /// </summary>
    /// <param name="clipper"></param>
    /// <param name="points"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="outPolygons"></param>
    /// <param name="depth"></param>
    private void CutPoly(Clipper clipper, List<IntPoint> points, Vector3 start, Vector3 end, List<List<IntPoint>> outPolygons, int depth)
    {
        var dir = (start - end).normalized;
        var right = new Vector3(-dir.z, 0, dir.x);

        var w = 2000f;
        var s1 = start + dir * w;
        var s2 = end - dir * w;
        var e1 = s1 + right * w * 1.42f;
        var e2 = s2 + right * w * 1.42f;

        var clip = new List<IntPoint>();
        clip.Add(new IntPoint(s1));
        clip.Add(new IntPoint(s2));
        clip.Add(new IntPoint(e2));
        clip.Add(new IntPoint(e1));

        clipper.Clear();
        clipper.AddPath(points, PolyType.ptSubject, true);
        clipper.AddPath(clip, PolyType.ptClip, true);

        var poly1s = new List<List<IntPoint>>();
        var poly2s = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctIntersection, poly1s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
        clipper.Execute(ClipType.ctDifference, poly2s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

        if (poly1s.Count > 0)
        {
            foreach (var poly in poly1s)
            {
                SubdividePolygon(clipper, poly, outPolygons, depth - 1);
            }
        }

        if (poly2s.Count > 0)
        {
            foreach (var poly in poly2s)
            {
                SubdividePolygon(clipper, poly, outPolygons, depth - 1);
            }
        }

    }

    /// <summary>
    /// Calculates the edges (the lines in the middle, defining the roads) of the village's unpaved roads.
    /// </summary>
    /// <param name="roadPolys"></param>
    /// <returns></returns>
    private List<Pair<IntPoint>> CalculateVillageUnpavedRoadEdges(List<List<IntPoint>> roadPolys)
    {
        var neighbourCount = mesoRoadData.CentralMesoRoadNode.Neighbours.Count;

        RoadSegment selectedRoadSegment = null;

        if (neighbourCount == 1)
        {
            //Simplest case
            selectedRoadSegment = mesoRoadData.CentralRoadSegments[mesoRoadData.CentralMesoRoadNode.Neighbours[0]];
        } else if (neighbourCount % 2 == 1)
        {
            var nonContinuousRoad = mesoRoadData.CentralMesoRoadNode.GetNonContinuousRoad();
            if (nonContinuousRoad.HasValue)
            {
                selectedRoadSegment = mesoRoadData.CentralRoadSegments[nonContinuousRoad.Value];
            }
        }
        
        var unpavedRoadEdges = new List<Pair<IntPoint>>();

        if (selectedRoadSegment != null)
        {
            //Construct a polygon for a street

            var dir = (selectedRoadSegment.Spline[0] - selectedRoadSegment.Spline[1]).normalized;
            var right = new Vector3(dir.z, 0, -dir.x);

            var l1 = selectedRoadSegment.RoadVertices[0].x;
            var r1 = selectedRoadSegment.RoadVertices[0].z;

            var start = (l1 + r1).ToVec3() / 2;
            var initialEnd = start + dir * Const.MESO_CHUNK_WIDTH_IN_METERS * 1.5f;

            var intersection =
                ChunkUtil.LineSegmentIntersectionWithPolygon(start, initialEnd, villageRegion.Outline);

            if (!intersection.HasValue)
                return unpavedRoadEdges;

            var end = intersection.Value + dir;

            var l2 = new IntPoint(end - right * Const.STREET_WIDTH_IN_METERS / 2);
            var r2 = new IntPoint(end + right * Const.STREET_WIDTH_IN_METERS / 2);

            unpavedRoadEdges.Add(new Pair<IntPoint>(new IntPoint(start), new IntPoint(end)));

            var firstStreetPoly = new List<IntPoint>();
            firstStreetPoly.Add(l1);
            firstStreetPoly.Add(r1);
            firstStreetPoly.Add(r2);
            firstStreetPoly.Add(l2);

            var clipper = new Clipper();
            clipper.AddPath(villageRegion.PointOutline, PolyType.ptSubject, true);
            clipper.AddPaths(roadPolys, PolyType.ptClip, true);
            clipper.AddPath(firstStreetPoly, PolyType.ptClip, true);

            var initialPlotPolys = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctDifference, initialPlotPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

            foreach (var initialPlotPoly in initialPlotPolys)
            {
                Subdivide(unpavedRoadEdges, clipper, initialPlotPoly, 2);
            }

        }
        else
        {
            //Place no additional road. Instead find all polygons between side roads and the outline and subdivide each.

            var clipper = new Clipper();
            clipper.AddPath(villageRegion.PointOutline, PolyType.ptSubject, true);
            clipper.AddPaths(roadPolys, PolyType.ptClip, true);

            var initialPlotPolys = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctDifference, initialPlotPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

            foreach (var initialPlotPoly in initialPlotPolys)
            {
                Subdivide(unpavedRoadEdges, clipper, initialPlotPoly, 2);
            }
        }
        return unpavedRoadEdges;
    }
    
    /// <summary>
    /// Subdivides the village region outline (points) based on the unpaved road edges.
    /// </summary>
    /// <param name="unpavedRoadEdges"></param>
    /// <param name="clipper"></param>
    /// <param name="points"></param>
    /// <param name="depth"></param>
    private void Subdivide(List<Pair<IntPoint>> unpavedRoadEdges, Clipper clipper, List<IntPoint> points, int depth)
    {
        var vertices = ClipperUtil.ToVectors(points);
        if (depth == 0)
        {
            return;
        }

        var longestEdgeI = ChunkUtil.LongestEdge(points);
        var centroid = ChunkUtil.CentroidOfPolygon(vertices);

        var start = vertices[longestEdgeI];
        var end = vertices[(longestEdgeI + 1) % vertices.Count];

        var clipPoint1 = ChunkUtil.GetPointProjectionOnLine(centroid, start, end);

        var clipPoint2 = centroid;

        CutPoly(unpavedRoadEdges, clipper, points, clipPoint1, clipPoint2, depth);
    }

    /// <summary>
    /// Cuts the given poly 'points' with a line through start and end until depth 0 is reached.
    /// </summary>
    /// <param name="unpavedRoadEdges"></param>
    /// <param name="clipper"></param>
    /// <param name="points"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="depth"></param>
    private void CutPoly(List<Pair<IntPoint>> unpavedRoadEdges, Clipper clipper, List<IntPoint> points, Vector3 start, Vector3 end, int depth)
    {
        var dir = (start - end).normalized;
        var right = new Vector3(-dir.z, 0, dir.x);

        var w = 2000f;
        var s1 = start + dir * w;
        var s2 = end - dir * w;
        var e1 = s1 + right * w * 1.42f;
        var e2 = s2 + right * w * 1.42f;

        var clip = new List<IntPoint>();
        clip.Add(new IntPoint(s1));
        clip.Add(new IntPoint(s2));
        clip.Add(new IntPoint(e2));
        clip.Add(new IntPoint(e1));

        clipper.Clear();
        clipper.AddPath(points, PolyType.ptSubject, true);
        clipper.AddPath(clip, PolyType.ptClip, true);

        var poly1s = new List<List<IntPoint>>();
        var poly2s = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctIntersection, poly1s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
        clipper.Execute(ClipType.ctDifference, poly2s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);


        if (poly1s.Count > 0)
        {
            foreach (var poly in poly1s)
            {
                //TODO: Reuse
                var roadPoints = new List<IntPoint>();
                foreach (var point in poly)
                {
                    if (!points.Contains(point))
                        roadPoints.Add(point);
                }
                if (roadPoints.Count == 2)
                {
                    unpavedRoadEdges.Add(new Pair<IntPoint>(roadPoints[0], roadPoints[1]));
                }


                Subdivide(unpavedRoadEdges, clipper, poly, depth - 1);
            }
        }

        if (poly2s.Count > 0)
        {
            foreach (var poly in poly2s)
            {
                Subdivide(unpavedRoadEdges, clipper, poly, depth - 1);
            }
        }

    }
}
