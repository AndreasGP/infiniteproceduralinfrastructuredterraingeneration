﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;
#pragma warning disable 649

public class MesoChunkTerrainPopulator
{

    /// <summary>
    /// Generates terrian objects for the given mesochunk subregion. Does not place any objects closer than 'minDistFromBorder' units from the border.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="subregion"></param>
    /// <param name="minDistFromBorder"></param>
    public void GenerateSubregionTerrainObjectsAsync(MesoChunk mesoChunk, MesoChunkRegion subregion, float minDistFromBorder)
    {
        if (!Settings.SpawnTerrainObjects)
            return;

        var configMaybe = AssetManager.GetRegionTerrainObjectSamplerConfig(subregion.Type);
        if (configMaybe == null)
            return;
        var config = configMaybe.Value;

        switch (config.SamplerType)
        {
            case SamplerType.Grid:
                var longestEdgeI = ChunkUtil.LongestEdge(subregion.PointOutline);
                var start = subregion.PointOutline[longestEdgeI];
                var end = subregion.PointOutline[(longestEdgeI + 1) % subregion.PointOutline.Count];
                var dir = (end - start);
                var angle = -Mathf.Atan2(dir.Y, dir.X);
                var gridSampler = new GridSampler(subregion.Outline, config.SamplerStepX,
                    config.SamplerStepY, config.VarianceX, config.VarianceY, angle);
                GenerateSubregionObjectsAsync(gridSampler, mesoChunk, subregion, minDistFromBorder, config);
                break;
            case SamplerType.NatureReserve:
            case SamplerType.PoissonDisc:
                var regionRect = ChunkUtil.GetPointsRect(subregion.Outline);
                var poissonDiscSampler = new PoissonDiskSampler(regionRect.xMin, regionRect.yMin, regionRect.width,
                    regionRect.height, config.SamplerStepX, ChunkUtil.Hash(mesoChunk.Pos));
                GenerateSubregionObjectsAsync(poissonDiscSampler, mesoChunk, subregion, minDistFromBorder, config);
                break;
            case SamplerType.Simplex:
                //TODO: Support?
                break;
        }

    }

    /// <summary>
    /// Generates objects for the given subregion using the given sampler (vegetation placement algorithm).
    /// </summary>
    /// <param name="sampler"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="subregion"></param>
    /// <param name="minDistFromBorder"></param>
    /// <param name="config"></param>
    private void GenerateSubregionObjectsAsync(ISampler sampler, MesoChunk mesoChunk, MesoChunkRegion subregion, float minDistFromBorder,
        TerrainObjectSamplerConfig config)
    {

        var checkIgnorables = subregion.IgnorableOutlines != null;

        var terrainObjects = subregion.TerrainObjectMetaDatas ?? new List <TerrainObjectMetaData>();

        var last = Vector3.zero;
        var lastPlaced = false;

        foreach (var pos in sampler.Samples())
        {
            var placed = ProcessSampledPosition(pos, mesoChunk, subregion, minDistFromBorder, checkIgnorables, terrainObjects,
                config.TerrainObjectConfigs, config, false);

            if (config.SecondaryTerrainObjectConfigs != null && placed && lastPlaced && sampler is PoissonDiskSampler)
            {
                if (last != Vector3.zero && (pos - last).magnitude < config.SamplerStepX * 1.75f)
                {
                    var newPos = pos;
                    newPos.y = mesoChunk.GetHeightAt(pos);

                    var newLast = last;
                    newLast.y = mesoChunk.GetHeightAt(last);
                    
                    var half = (pos + last) / 2;

                    ProcessSampledPosition(half, mesoChunk, subregion, minDistFromBorder, checkIgnorables, terrainObjects,
                        config.SecondaryTerrainObjectConfigs, config, true);
                    half.y = mesoChunk.GetHeightAt(half);
                }
                
            }


            last = pos;
            lastPlaced = placed;
        }

        if (config.SecondaryTerrainObjectConfigs != null && sampler is GridSampler)
        {
            var gridSampler = (GridSampler) sampler;

            //Also spawn secondary objects
            foreach (var pos in gridSampler.SecondarySamples())
            {
                ProcessSampledPosition(pos, mesoChunk, subregion, minDistFromBorder, checkIgnorables,
                    terrainObjects, config.SecondaryTerrainObjectConfigs, config, true);
            }
        }

        subregion.SetTerrainObjectMetaDatas(terrainObjects);
    }


    /// <summary>
    /// Processes the sampled position from a sampler. Does not place anytihng if its not inside the subregion or inside an ignorable area inside the subregion.
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="subregion"></param>
    /// <param name="minDistFromBorder"></param>
    /// <param name="checkIgnorables"></param>
    /// <param name="terrainObjects"></param>
    /// <param name="terrainObjectConfigs"></param>
    /// <param name="samplerConfig"></param>
    /// <param name="ignoreNatureReserveCheck"></param>
    /// <returns></returns>
    private bool ProcessSampledPosition(Vector3 pos, MesoChunk mesoChunk, MesoChunkRegion subregion, float minDistFromBorder, bool checkIgnorables, 
        List<TerrainObjectMetaData> terrainObjects, List<TerrainObjectConfig> terrainObjectConfigs, TerrainObjectSamplerConfig samplerConfig, bool ignoreNatureReserveCheck)
    {
        
        if (ChunkUtil.InsidePolygon(pos, subregion.Outline, subregion.IsConvex.Value, 1) &&
            (minDistFromBorder > 0.01f && ChunkUtil.PointDistanceFromPolygon(pos, subregion.Outline) > minDistFromBorder || minDistFromBorder <= 0.01f))
        {
            if (checkIgnorables)
            {
                var invalidPos = false;
                foreach (var ignorableRegion in subregion.IgnorableOutlines)
                {
                    //TODO: Currently only houses uses this, which are convex. If anything else is introduced that is not convex, this must be changed.
                    if (ChunkUtil.InsidePolygon(pos, ignorableRegion, true, 1))
                    {
                        //Unsuitable position
                        invalidPos = true;
                        break;
                    }
                }

                if (invalidPos)
                    return false;
            }

            var hash = ChunkUtil.Hash((int)pos.x, (int)pos.z);
            var yaw = hash % 360;

            TerrainObjectConfig? objConfigMaybe = null;

            if (!ignoreNatureReserveCheck && samplerConfig.SamplerType == SamplerType.NatureReserve)
            {
                var noise = NoiseService.GetRegionTypeNoise(pos);
                //roughly in [0.65, 1) because thats the range of nature reserve type noise
                if (noise < 0.7)
                {
                    //Mainly trees
                    if (hash % 2 == 0)
                        objConfigMaybe = samplerConfig.TerrainObjectConfigs[0];
                    else
                        objConfigMaybe = samplerConfig.TerrainObjectConfigs[1];
                }
                else if (noise < 0.8)
                {
                    //Mostly Bush
                    if(hash % 80 > 2)
                        objConfigMaybe = samplerConfig.TerrainObjectConfigs[2];
                    else
                    {
                        //A few trees
                        if (hash % 2 == 0)
                            objConfigMaybe = samplerConfig.TerrainObjectConfigs[0];
                        else
                            objConfigMaybe = samplerConfig.TerrainObjectConfigs[1];
                    }
                }
                else
                {
                    //Rare bush
                    if(hash % 100 > 96)
                        objConfigMaybe = samplerConfig.TerrainObjectConfigs[2];
                    else if(hash % 500 < 2)
                    {
                        //A few trees
                        if (hash % 2 == 0)
                            objConfigMaybe = samplerConfig.TerrainObjectConfigs[0];
                        else
                            objConfigMaybe = samplerConfig.TerrainObjectConfigs[1];
                    }
                }
            }
            else
            {
                objConfigMaybe = terrainObjectConfigs.Count > 1
                    ? GetTerrainObjectConfig(hash % 10000 / 10000f, terrainObjectConfigs)
                    : terrainObjectConfigs[0];
            }

            if (objConfigMaybe.HasValue)
            {
                var objConfig = objConfigMaybe.Value;
                var objPos = pos;
                objPos.y = mesoChunk.GetHeightAt(objPos);

                var obj = TerrainObjectMetaData.Generate(objConfig.Prefab, objPos, yaw, objConfig.MinScale, objConfig.ScaleVariance,
                    objConfig.HeightOffset);
                terrainObjects.Add(obj);

                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Spawns grass on the given mesochunk.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="subregion"></param>
    /// <param name="terrainObjects"></param>
    private void SpawnGrass(MesoChunk mesoChunk, MesoChunkRegion subregion, List<TerrainObjectMetaData> terrainObjects)
    {

        var regionRect = ChunkUtil.GetPointsRect(subregion.Outline);
        var poissonDiscSampler = new PoissonDiskSampler(regionRect.xMin, regionRect.yMin, regionRect.width,
            regionRect.height, 2, ChunkUtil.Hash(mesoChunk.Pos));

        foreach (var pos in poissonDiscSampler.Samples())
        {
            if (ChunkUtil.InsidePolygon(pos, subregion.Outline, subregion.IsConvex.Value, 1) && ChunkUtil.PointDistanceFromPolygon(pos, subregion.Outline) > 2)
            {

                var hash = ChunkUtil.Hash((int)pos.x, (int)pos.z);

                var objPos = pos;
                objPos.y = mesoChunk.GetHeightAt(objPos);
                var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.GrassPrefab, objPos, 0, 1, 0, 0);
                terrainObjects.Add(obj);
            }
        }
    }

    /// <summary>
    /// Fetches a terrain object config for using the given probability using the list of terrain object configs.
    /// </summary>
    /// <param name="probability"></param>
    /// <param name="terrainObjectConfigs"></param>
    /// <returns></returns>
    private TerrainObjectConfig GetTerrainObjectConfig(float probability,
        List<TerrainObjectConfig> terrainObjectConfigs)
    {
        var accum = 0f;

        foreach (var conf in terrainObjectConfigs)
        {
            accum += conf.Probability;
            if (accum >= probability)
                return conf;
        }

        Debug.LogError("Probabilities out of bounds");
        return terrainObjectConfigs[0];
    }

    /// <summary>
    /// Generates utility poles and traffic signs on the given chunk.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <returns></returns>
    public List<TerrainObjectMetaData> GenerateUtilityPolesAndTrafficSignsAsync(MesoChunk mesoChunk)
    {
        if (mesoChunk.Data.Regions.Count == 0)
            return new List<TerrainObjectMetaData>();


        //Pick the first region on the chunk as the parent
        var region = mesoChunk.Data.Regions[0];

        var objects = region.TerrainObjectMetaDatas ?? new List<TerrainObjectMetaData>();

        GenerateUtilityPolesAsync(mesoChunk, objects);

        GenerateTrafficSignsAsync(mesoChunk, objects);

        region.SetTerrainObjectMetaDatas(objects);


        return objects;
    }

    /// <summary>
    /// Generates utility poles and their connections on the given chunk.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="objects"></param>
    private void GenerateUtilityPolesAsync(MesoChunk mesoChunk, List<TerrainObjectMetaData> objects)
    {
        var roadData = mesoChunk.Data.RoadData;
        foreach (var segment in roadData.IntersectingRoadSegments)
        {
            var splineLength = CurveUtil.GetSplineLength(segment.Spline);

            //1 per roughly 40 meters
            var numberOfPoles = (int) splineLength / 40;

            Vector3 lastPos = Vector3.zero;
            float lastYaw = 0;

            for (int d = 1; d <= numberOfPoles - 1; d++)
            {
                var queryDist = d / (float) numberOfPoles;

                float yaw = 0;
                var polePositionMaybe = GetPolePosition(mesoChunk, roadData, segment.Spline, splineLength, queryDist, out yaw);

                if (!polePositionMaybe.HasValue || !mesoChunk.OnChunk(polePositionMaybe.Value))
                    continue;

                var polePos = polePositionMaybe.Value;

                List<object> data = new List<object>();
                if (lastPos != Vector3.zero)
                {
                    data.Add(lastPos);
                    data.Add(lastYaw);
                }
                else
                {
                    //lastPos not set - try to determine it
                    if (d > 1)
                    {
                        var lastPoleQueryDist = (d - 1) / (float)numberOfPoles;
                        var lastPolePositionMaybe = GetPolePosition(mesoChunk, roadData, segment.Spline, splineLength, lastPoleQueryDist, out lastYaw);
                        if (lastPolePositionMaybe.HasValue)
                        {
                            lastPos = lastPolePositionMaybe.Value;
                            data.Add(lastPos);
                            data.Add(lastYaw);
                        }
                    }

                    if (d < numberOfPoles - 1)
                    {
                        var nextPoleQueryDist = (d + 1) / (float)numberOfPoles;
                        var nextYaw = 0f;
                        var nextPolePositionMaybe = GetPolePosition(mesoChunk, roadData, segment.Spline, splineLength, nextPoleQueryDist, out nextYaw);
                        if (nextPolePositionMaybe.HasValue)
                        {
                            var nextPos = nextPolePositionMaybe.Value;
                            data.Add(nextPos);
                            data.Add(nextYaw);
                        }
                    }
                }
                
                var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.UtilityPolePrefab, polePos,
                    yaw, 1f, 0f, 0f, data.ToArray());
                objects.Add(obj);
                
                lastPos = polePos;
                lastYaw = yaw;
            }
        }

        //Special handling for junctions
        if (roadData.CentralMesoRoadNode != null && roadData.CentralMesoRoadNode.Neighbours.Count > 0)
        {
            var firstNeighbour = roadData.CentralMesoRoadNode.Neighbours[0];
            var firstSegment = roadData.CentralRoadSegments[firstNeighbour];

            List<object> data = new List<object>();

            var firstSplineLength = CurveUtil.GetSplineLength(firstSegment.Spline);
            var firstQueryDist = 0.2f / (float) ((int) firstSplineLength / 40);
            float firstYaw = 0;
            var firstPosMaybe = GetPolePosition(mesoChunk, roadData, firstSegment.Spline, null, firstQueryDist, out firstYaw);

            if (!firstPosMaybe.HasValue)
                return;

            var firstPos = firstPosMaybe.Value;
            
            //Determine yaw so that it is rotated towards the junction
            var dir = (roadData.CentralMesoRoadNode.GlobalPos - firstPos).normalized;
            firstYaw = -Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;


            for (int i = 0; i < roadData.CentralMesoRoadNode.Neighbours.Count; i++)
            {
                var otherSegment = roadData.CentralRoadSegments[roadData.CentralMesoRoadNode.Neighbours[i]];

                var otherSplineLength = CurveUtil.GetSplineLength(otherSegment.Spline);

                //1 per roughly 40 meters
                var numberOfPoles = (int) otherSplineLength / 40;

                Vector3 lastPos = Vector3.zero;
                float lastYaw = 0;

                var queryDist = 1 / (float) numberOfPoles;

                float otherYaw;
                var otherPolePositionMaybe = GetPolePosition(mesoChunk, roadData, otherSegment.Spline,
                    otherSplineLength, queryDist, out otherYaw);

                if (!otherPolePositionMaybe.HasValue)
                    continue;

                data.Add(otherPolePositionMaybe.Value);
                data.Add(otherYaw);
            }

            var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.UtilityPolePrefab, firstPos,
                firstYaw, 1f, 0f, 0f, data.ToArray());
            objects.Add(obj);
        }
    }

    /// <summary>
    /// Rreturns the utility pole position on the mesochunk if one is found. It can happen that all tested positions intersect with streets and a pole can not be placed.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="roadData"></param>
    /// <param name="spline"></param>
    /// <param name="splineLengthMaybe"></param>
    /// <param name="queryDist"></param>
    /// <param name="yaw"></param>
    /// <returns></returns>
    private Vector3? GetPolePosition(MesoChunk mesoChunk, MesoRoadData roadData, List<Vector3> spline, float? splineLengthMaybe, float queryDist, out float yaw)
    {
        var splineLength = splineLengthMaybe.HasValue ? splineLengthMaybe.Value : CurveUtil.GetSplineLength(spline);

        var segmentStartI = 0;
        var splinePos =
            CurveUtil.GetPointOnSplineByLength(spline, splineLength, queryDist, out segmentStartI);

        var fwd = (spline[segmentStartI + 1] - spline[segmentStartI]).normalized;

        if (fwd.x > fwd.z)
        {
            fwd *= -1;
        }

        var right = new Vector3(fwd.z, 0, -fwd.x);

        //5 attempts to place
        for (int i = -2; i <= 2; i++)
        {
            //0, -1, -2, 1, 2
            float offset = i <= 0 ? -2 - i : i;
            //try every 4 meters
            offset *= 4f;

            var polePos = splinePos + right * (Const.SIDE_ROAD_WIDTH_IN_METERS / 2 + 2f) + fwd * offset;

            //Validate position
            if (!ChunkUtil.IntersectsWithOutlines(new IntPoint(polePos), roadData.RoadAndStreetPolys))
            {
                polePos.y = mesoChunk.GetHeightAt(polePos);

                yaw = 90 - Mathf.Atan2(right.z, right.x) * Mathf.Rad2Deg;
                return polePos;
            }
        } 

        yaw = 0;
        return null;
    }

    /// <summary>
    /// Generates traffic signs for the given mesochunk.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="objects"></param>
    private void GenerateTrafficSignsAsync(MesoChunk mesoChunk, List<TerrainObjectMetaData> objects)
    {
        var roadData = mesoChunk.Data.RoadData;
        var centralNode = roadData.CentralMesoRoadNode;

        if (centralNode != null)
        {
            GenerateAheadLeftRightSignsAsync(mesoChunk, objects);

            if (mesoChunk.Data.VillageData != null)
            {
                GenerateVillageTrafficSignsAsync(mesoChunk, mesoChunk.Data.VillageData, objects);
            }

            if (mesoChunk.Data.CityData != null && !mesoChunk.Data.CityData.CityOnly)
            {
                //Chunk contains city and its not 100% city, meaning it contains a city border
                GenerateCityTrafficSignsAsync(mesoChunk, mesoChunk.Data.CityData, objects);
            }

            if (mesoChunk.Data.ContainsHighway)
            {
                var macroRoadData = mesoChunk.MacroChunk.Data.RoadData;
                if (macroRoadData.CentralMacroRoadNode != null)
                {
                    var macroRoadNodePos = macroRoadData.CentralMacroRoadNode.GlobalPos;
                    if (mesoChunk.Rect.Contains(new Vector2(macroRoadNodePos.x, macroRoadNodePos.z)))
                    {
                        GenerateCityAheadSignsAsync(mesoChunk, macroRoadData, objects);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Generates direction sings for the mesochunk, such as (city > 2 km, village ^ 1 km)
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="objects"></param>
    private void GenerateAheadLeftRightSignsAsync(MesoChunk mesoChunk, List<TerrainObjectMetaData> objects)
    {
        var mesoRoadData = mesoChunk.Data.RoadData;
        var centralNode = mesoRoadData.CentralMesoRoadNode;

        if (centralNode.Neighbours.Count == 2)
        {
            GenerateAheadLeftRightSignsFor2wayJunctionAsync(mesoChunk, objects);
        } else if (centralNode.Neighbours.Count == 3)
        {
            GenerateAheadLeftRightSignsFor3wayJunctionAsync(mesoChunk, objects);
        } else if (centralNode.Neighbours.Count == 4)
        {

            GenerateAheadLeftRightSignsFor4wayJunctionAsync(mesoChunk, objects);
        }
    }

    /// <summary>
    /// Generates direction sings for the mesochunk for 2-way junctions, such as (city > 2 km, village ^ 1 km)
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="objects"></param>
    private void GenerateAheadLeftRightSignsFor2wayJunctionAsync(MesoChunk mesoChunk,
    List<TerrainObjectMetaData> objects)
    {
        var mesoRoadData = mesoChunk.Data.RoadData;
        var centralNode = mesoRoadData.CentralMesoRoadNode;
        var macroRoadData = mesoChunk.MacroChunk.Data.RoadData;

        var neighbours = centralNode.Neighbours;
        var centerGlobalPos = centralNode.GlobalPos;

        for (int i = 0; i < neighbours.Count; i++)
        {
            var centerSegment = mesoRoadData.CentralRoadSegments[neighbours[(i + 1) % neighbours.Count]];
            var centerNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(centerSegment.V2);
            var centerNeighbourNode = mesoRoadData.MesoRoadNodes[centerNeighbourMesoPos];

            #region a settlement ahead on the opposite junction
            var aheadSegment = mesoRoadData.CentralRoadSegments[neighbours[i]];
            var aheadNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(aheadSegment.V2);
            var aheadNeighbourNode = mesoRoadData.MesoRoadNodes[aheadNeighbourMesoPos];
            var aheadName = GetSettlementNameAlongRoad(mesoChunk.MacroChunk, mesoRoadData, aheadNeighbourNode, aheadNeighbourMesoPos);
            if (aheadName != null)
            {
                var heightOffset = 0f;
                var dist = (int)Math.Ceiling(CurveUtil.GetSplineLength(aheadSegment.Spline) / 1000);
                CreateSignOnSpline(mesoChunk, mesoRoadData, centerSegment, AssetManager.Ins.SettlementAheadSignPrefab, aheadName, heightOffset, dist, objects);
            }
            #endregion
        }
    }

    /// <summary>
    /// Generates direction sings for the mesochunk for 3-way junctions, such as (city > 2 km, village ^ 1 km)
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="objects"></param>
    private void GenerateAheadLeftRightSignsFor3wayJunctionAsync(MesoChunk mesoChunk,
        List<TerrainObjectMetaData> objects)
    {
        var mesoRoadData = mesoChunk.Data.RoadData;
        var centralNode = mesoRoadData.CentralMesoRoadNode;
        var macroRoadData = mesoChunk.MacroChunk.Data.RoadData;

        var centerGlobalPos = centralNode.GlobalPos;
        var orderedNeighbours = centralNode.Neighbours.OrderBy(n => Math.Atan2(n.z - centerGlobalPos.z, n.x - centerGlobalPos.x)).ToList();

        for (int i = 0; i < orderedNeighbours.Count; i++)
        {
            var centerSegment = mesoRoadData.CentralRoadSegments[orderedNeighbours[(i + 1) % orderedNeighbours.Count]];
            var centerNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(centerSegment.V2);
            var centerNeighbourNode = mesoRoadData.MesoRoadNodes[centerNeighbourMesoPos];

            #region a settlement ahead on the left junction
            var leftSegment = mesoRoadData.CentralRoadSegments[orderedNeighbours[i]];
            var leftNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(leftSegment.V2);
            var leftNeighbourNode = mesoRoadData.MesoRoadNodes[leftNeighbourMesoPos];
            var leftName = GetSettlementNameAlongRoad(mesoChunk.MacroChunk, mesoRoadData, leftNeighbourNode, leftNeighbourMesoPos);

            var leftPlaced = false;

            if (leftName != null)
            {
                var prefab = AssetManager.Ins.SettlementLeftSignPrefab;
                var heightOffset = 0;
                //Decide if to use left or ahead sign depending on the side segment being continuous or nto
                if (centralNode.ContinuousRoads.ContainsKey(leftNeighbourNode.GlobalPos) &&
                    centralNode.ContinuousRoads[leftNeighbourNode.GlobalPos] == centerNeighbourNode.GlobalPos)
                {
                    //Continuous road, use ahead sign
                    prefab = AssetManager.Ins.SettlementAheadSignPrefab;
                }

                var dist = (int)Math.Ceiling(CurveUtil.GetSplineLength(leftSegment.Spline) / 1000);
                CreateSignOnSpline(mesoChunk, mesoRoadData, centerSegment, prefab, leftName, heightOffset, dist, objects);
                leftPlaced = true;
            }

            #endregion
            #region a settlement ahead on the right junction
            var rightSegment = mesoRoadData.CentralRoadSegments[orderedNeighbours[(i + 2) % orderedNeighbours.Count]];
            var rightNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(rightSegment.V2);
            var rightNeighbourNode = mesoRoadData.MesoRoadNodes[rightNeighbourMesoPos];
            var rightName = GetSettlementNameAlongRoad(mesoChunk.MacroChunk, mesoRoadData, rightNeighbourNode, rightNeighbourMesoPos);
            if (rightName != null)
            {
                var prefab = AssetManager.Ins.SettlementRightSignPrefab;
                var heightOffset = leftPlaced ? 0.26f : 0f;
                //Decide if to use right or ahead sign depending on the side segment being continuous or nto
                if (centralNode.ContinuousRoads.ContainsKey(rightNeighbourNode.GlobalPos) &&
                    centralNode.ContinuousRoads[rightNeighbourNode.GlobalPos] == centerNeighbourNode.GlobalPos)
                {
                    //Continuous road, use ahead sign
                    prefab = AssetManager.Ins.SettlementAheadSignPrefab;
                }

                var dist = (int)Math.Ceiling(CurveUtil.GetSplineLength(rightSegment.Spline) / 1000);
                CreateSignOnSpline(mesoChunk, mesoRoadData, centerSegment, prefab, rightName, heightOffset, dist, objects);
            }
            #endregion
        }
    }

    /// <summary>
    /// Generates direction sings for the mesochunk for 4-way junctions, such as (city > 2 km, village ^ 1 km)
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="objects"></param>
    private void GenerateAheadLeftRightSignsFor4wayJunctionAsync(MesoChunk mesoChunk,
        List<TerrainObjectMetaData> objects)
    {
        var mesoRoadData = mesoChunk.Data.RoadData;
        var centralNode = mesoRoadData.CentralMesoRoadNode;
        var macroRoadData = mesoChunk.MacroChunk.Data.RoadData;

        var centerGlobalPos = centralNode.GlobalPos;
        var orderedNeighbours = centralNode.Neighbours.OrderBy(n => Math.Atan2(n.z - centerGlobalPos.z, n.x - centerGlobalPos.x)).ToList();

        for (int i = 0; i < orderedNeighbours.Count; i++)
        {
            var centerSegment = mesoRoadData.CentralRoadSegments[orderedNeighbours[(i + 2) % orderedNeighbours.Count]];
            var centerNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(centerSegment.V2);
            var centerNeighbourNode = mesoRoadData.MesoRoadNodes[centerNeighbourMesoPos];

            #region a settlement ahead on the left junction
            var leftSegment = mesoRoadData.CentralRoadSegments[orderedNeighbours[(i + 1) % orderedNeighbours.Count]];
            var leftNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(leftSegment.V2);
            var leftNeighbourNode = mesoRoadData.MesoRoadNodes[leftNeighbourMesoPos];
            var leftName = GetSettlementNameAlongRoad(mesoChunk.MacroChunk, mesoRoadData, leftNeighbourNode, leftNeighbourMesoPos);

            var cumulativeOffset = 0f;

            if (leftName != null)
            {
                var heightOffset = 0;
                var dist = (int)Math.Ceiling(CurveUtil.GetSplineLength(leftSegment.Spline) / 1000);
                CreateSignOnSpline(mesoChunk, mesoRoadData, centerSegment, AssetManager.Ins.SettlementLeftSignPrefab, leftName, heightOffset, dist, objects);
                cumulativeOffset += 0.26f;
            }

            #endregion
            #region a settlement ahead on the right junction
            var rightSegment = mesoRoadData.CentralRoadSegments[orderedNeighbours[(i + 3) % orderedNeighbours.Count]];
            var rightNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(rightSegment.V2);
            var rightNeighbourNode = mesoRoadData.MesoRoadNodes[rightNeighbourMesoPos];
            var rightName = GetSettlementNameAlongRoad(mesoChunk.MacroChunk, mesoRoadData, rightNeighbourNode, rightNeighbourMesoPos);
            if (rightName != null)
            {
                var heightOffset = cumulativeOffset;
                var dist = (int)Math.Ceiling(CurveUtil.GetSplineLength(rightSegment.Spline) / 1000);
                CreateSignOnSpline(mesoChunk, mesoRoadData, centerSegment, AssetManager.Ins.SettlementRightSignPrefab, rightName, heightOffset, dist, objects);
                cumulativeOffset += 0.26f;
            }
            #endregion

            #region a settlement ahead on the opposite junction
            var aheadSegment = mesoRoadData.CentralRoadSegments[orderedNeighbours[i]];
            var aheadNeighbourMesoPos = CoordUtil.GlobalPositionToMesoChunk(aheadSegment.V2);
            var aheadNeighbourNode = mesoRoadData.MesoRoadNodes[aheadNeighbourMesoPos];
            var aheadName = GetSettlementNameAlongRoad(mesoChunk.MacroChunk, mesoRoadData, aheadNeighbourNode, aheadNeighbourMesoPos);
            if (aheadName != null)
            {
                var heightOffset = cumulativeOffset;
                var dist = (int) Math.Ceiling(CurveUtil.GetSplineLength(aheadSegment.Spline) / 1000);
                CreateSignOnSpline(mesoChunk, mesoRoadData, centerSegment, AssetManager.Ins.SettlementAheadSignPrefab, aheadName, heightOffset, dist, objects);
            }
            #endregion
        }
    }

    /// <summary>
    /// Places the sign (given as prefab) on the given chunk if a position for it can be found.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="roadData"></param>
    /// <param name="segment"></param>
    /// <param name="prefab"></param>
    /// <param name="name"></param>
    /// <param name="heightOffset"></param>
    /// <param name="dist"></param>
    /// <param name="objects"></param>
    private void CreateSignOnSpline(MesoChunk mesoChunk, MesoRoadData roadData, RoadSegment segment, GameObject prefab, String name, float heightOffset, int dist, List<TerrainObjectMetaData> objects)
    {
        var spline = segment.Spline;

        var fwd = (spline[1] - spline[0]).normalized;

        var right = new Vector3(fwd.z, 0, -fwd.x);
        
        var angle = 180-(float)Math.Atan2(fwd.z, fwd.x) * Mathf.Rad2Deg;

        var distInKm = dist.ToString();

        //5 attempts to place
        for (int i = -2; i <= 2; i++)
        {
            //0, -1, -2, 1, 2
            float offset = i <= 0 ? -2 - i : i;
            //try every 4 meters
            offset *= 4f;

            var pos = spline[0] + fwd * (10 + i * offset) - right * Const.HIGHWAY_WIDTH_IN_METERS * 0.55f;

            //Validate position
            if (!ChunkUtil.IntersectsWithOutlines(new IntPoint(pos), roadData.RoadAndStreetPolys))
            {
                pos.y = mesoChunk.GetHeightAt(pos);

                var obj = TerrainObjectMetaData.Generate(prefab, pos,
                    angle, 1f, 0f, heightOffset, new object[] { name.ToUpper(), distInKm });
                objects.Add(obj);
                break;
            }
        }
    }

    /// <summary>
    /// Returns the name of the next settlement on the road towards neighbourRoadNode, if one can be found.
    /// </summary>
    /// <param name="macroChunk"></param>
    /// <param name="mesoRoadData"></param>
    /// <param name="neighbourRoadNode"></param>
    /// <param name="neighbourMesoChunkPos"></param>
    /// <returns></returns>
    private string GetSettlementNameAlongRoad(MacroChunk macroChunk, MesoRoadData mesoRoadData, MesoRoadNode neighbourRoadNode, Point2 neighbourMesoChunkPos)
    {
        if (CoordUtil.MesoChunkToMacroChunk(neighbourMesoChunkPos) != macroChunk.Pos)
        {
            //The neighbouring mesochunk is not on this macrochunk. Find the correct macrochunk instead.
            macroChunk = ChunkManager.Ins.GetOrGenerateMacroChunk(CoordUtil.MesoChunkToMacroChunk(neighbourMesoChunkPos));
        }

        if (macroChunk.Data.RoadData.MesoChunkContainsHighway(neighbourMesoChunkPos))
            {
                //TODO:
                //find highway segment that this side road intersects with
                //determine if either highway end is a city
                //get the city name
            }


            if (MesoChunkRegionGenerator.IsVillage(macroChunk, neighbourRoadNode, neighbourMesoChunkPos))
            {
                return SettlementNameGenerator.GenerateName(CoordUtil.MesoChunkToGlobalPosition(neighbourMesoChunkPos));
            }
        else
        {
            //Check 1 segment further
            foreach (var pos in neighbourRoadNode.Neighbours)
            {

                var furtherNeighbourMesoChunkPos = CoordUtil.GlobalPositionToMesoChunk(pos);
                if (pos == mesoRoadData.CentralMesoRoadNode.GlobalPos ||
                    mesoRoadData.MesoRoadNodes.ContainsKey(furtherNeighbourMesoChunkPos) == false)
                    continue;

                if (CoordUtil.MesoChunkToMacroChunk(furtherNeighbourMesoChunkPos) == macroChunk.Pos)
                {
                    var furtherNeighbourRoadNode = mesoRoadData.MesoRoadNodes[furtherNeighbourMesoChunkPos];
                    if (MesoChunkRegionGenerator.IsVillage(macroChunk, furtherNeighbourRoadNode,
                        furtherNeighbourMesoChunkPos))
                    {
                        return SettlementNameGenerator.GenerateName(
                            CoordUtil.MesoChunkToGlobalPosition(furtherNeighbourMesoChunkPos));
                    }
                }
            }

        }

        return null;
    }

    /// <summary>
    /// Generates city ahead signs for highways and side roads.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="macroRoadData"></param>
    /// <param name="objects"></param>
    private void GenerateCityAheadSignsAsync(MesoChunk mesoChunk, MacroRoadData macroRoadData,
        List<TerrainObjectMetaData> objects)
    {

        foreach (var neighbour in macroRoadData.CentralMacroRoadNode.Neighbours)
        {
            //if neighbour is city -> place sign
            var neighbourChunkPos = CoordUtil.GlobalPositionToMacroChunk(neighbour);
            var neighbourNode = macroRoadData.MacroRoadNodes[neighbourChunkPos];
            if (!CityManager.Ins.WillContainCity(neighbourChunkPos, neighbourNode))
            {
                continue;
            }

            var neighbourName = SettlementNameGenerator.GenerateName(neighbourNode.GlobalPos);
            var segment = macroRoadData.CentralRoadSegments[neighbour];
            var spline = segment.Spline;

            var cityAheadFwd = (spline[1] - spline[0]).normalized;

            var cityAheadRight = new Vector3(cityAheadFwd.z, 0, -cityAheadFwd.x);

            var cityAheadPos = spline[0] + cityAheadFwd * 10 + cityAheadRight * Const.HIGHWAY_WIDTH_IN_METERS * 0.55f;

            var cityAheadAngle = -(float) Math.Atan2(cityAheadFwd.z, cityAheadFwd.x) * Mathf.Rad2Deg;

            var distInKm = ((int) Math.Ceiling(CurveUtil.GetSplineLength(spline) / 1000)).ToString();

            var cityAheadSign = TerrainObjectMetaData.Generate(AssetManager.Ins.SettlementAheadSignPrefab, cityAheadPos,
                cityAheadAngle, 1f, 0f, 0f, new object[] {neighbourName.ToUpper(), distInKm});
            objects.Add(cityAheadSign);
        }
        
    }


    /// <summary>
    /// Generates city begin and end signs.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="cityData"></param>
    /// <param name="objects"></param>
    private void GenerateCityTrafficSignsAsync(MesoChunk mesoChunk, MesoChunkCityData cityData,
        List<TerrainObjectMetaData> objects)
    {
        var roadData = mesoChunk.Data.RoadData;
        var centralNode = roadData.CentralMesoRoadNode;

        foreach (var road in centralNode.Neighbours)
        {
            var segment = roadData.CentralRoadSegments[road];
            var spline = segment.Spline;

            for (int i = 0; i < spline.Count - 1; i++)
            {
                var intersection =
                    ChunkUtil.LineSegmentIntersectionWithPolygon(spline[i], spline[i + 1],
                        cityData.City.OutterOutline);

                if (intersection.HasValue)
                {
                    //Found spline segment that intersects with city border

                    var cityBeginFwd = (spline[i + 1] - spline[i]).normalized;
                    //Determine which direction is in the city
                    if (ChunkUtil.InsideSimplePolygon(spline[i + 1], cityData.City.OutterOutline))
                    {
                        cityBeginFwd *= -1;
                    }

                    var cityBeginRight = new Vector3(cityBeginFwd.z, 0, -cityBeginFwd.x);

                    var cityBeginPos = intersection.Value - cityBeginRight * Const.SIDE_ROAD_WIDTH_IN_METERS * 0.7f;
//                    cityBeginPos.y = mesoChunk.GetHeightAt(cityBeginPos);
                    var cityEndPos = intersection.Value + cityBeginRight * Const.SIDE_ROAD_WIDTH_IN_METERS * 0.7f;
                    //                    cityEndPos.y = mesoChunk.GetHeightAt(cityEndPos);

                    var cityBeginAngle = -(float) Math.Atan2(cityBeginFwd.z, cityBeginFwd.x) * Mathf.Rad2Deg + 180;

                    //5 attempts to place begin sign
                    for (int j = -2; j <= 2; j++)
                    {
                        //0, -1, -2, 1, 2
                        float offset = j <= 0 ? -2 - j : j;
                        //try every 4 meters
                        offset *= 4f;

                        var pos = cityBeginPos + cityBeginFwd * (j * offset);

                        //Validate position
                        if (!ChunkUtil.IntersectsWithOutlines(new IntPoint(pos), roadData.RoadAndStreetPolys))
                        {
                            pos.y = mesoChunk.GetHeightAt(pos);

                            var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.SettlementBeginSignPrefab, pos,
                                cityBeginAngle, 1f, 0f, 0f, new object[] { cityData.City.Name.ToUpper() });
                            objects.Add(obj);
                            break;
                        }
                    }

                    //5 attempts to place end sign
                    for (int j = -2; j <= 2; j++)
                    {
                        //0, -1, -2, 1, 2
                        float offset = j <= 0 ? -2 - j : j;
                        //try every 4 meters
                        offset *= 4f;

                        var pos = cityEndPos + cityBeginFwd * (j * offset);

                        //Validate position
                        if (!ChunkUtil.IntersectsWithOutlines(new IntPoint(pos), roadData.RoadAndStreetPolys))
                        {
                            pos.y = mesoChunk.GetHeightAt(pos);

                            var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.SettlementEndSignPrefab, pos,
                                cityBeginAngle - 180, 1f, 0f, 0f);
                            objects.Add(obj);
                            break;
                        }
                    }
                    
                }
            }
        }

        var macroRoadData = mesoChunk.MacroChunk.Data.RoadData;
        var macroCentralNode = macroRoadData.CentralMacroRoadNode;

        foreach (var road in macroCentralNode.Neighbours)
        {
            var segment = macroRoadData.CentralRoadSegments[road];
            var spline = segment.Spline;

            for (int i = 0; i < spline.Count - 1; i++)
            {
                var intersection =
                    ChunkUtil.LineSegmentIntersectionWithPolygon(spline[i], spline[i + 1],
                        cityData.City.OutterOutline);

                if (intersection.HasValue && ChunkUtil.InsideSimplePolygon(new IntPoint(intersection.Value), ChunkUtil.GetRectAsIntPoints(mesoChunk.RoadRect)))
                {
                    //Found spline segment that intersects with city border

                    var cityBeginFwd = (spline[i + 1] - spline[i]).normalized;
                    //Determine which direction is in the city
                    if (ChunkUtil.InsideSimplePolygon(spline[i + 1], cityData.City.OutterOutline))
                    {
                        cityBeginFwd *= -1;
                    }

                    var cityBeginRight = new Vector3(cityBeginFwd.z, 0, -cityBeginFwd.x);

                    var cityBeginPos = intersection.Value - cityBeginRight * Const.HIGHWAY_WIDTH_IN_METERS * 0.7f;
//                    cityBeginPos.y = mesoChunk.GetHeightAt(cityBeginPos);
                    var cityEndPos = intersection.Value + cityBeginRight * Const.HIGHWAY_WIDTH_IN_METERS * 0.7f;
//                    cityEndPos.y = mesoChunk.GetHeightAt(cityEndPos);

                    var cityBeginAngle = -(float)Math.Atan2(cityBeginFwd.z, cityBeginFwd.x) * Mathf.Rad2Deg + 180;

                    //5 attempts to place begin sign
                    for (int j = -2; j <= 2; j++)
                    {
                        //0, -1, -2, 1, 2
                        float offset = j <= 0 ? -2 - j : j;
                        //try every 4 meters
                        offset *= 4f;

                        var pos = cityBeginPos + cityBeginFwd * (j * offset);

                        //Validate position
                        if (!ChunkUtil.IntersectsWithOutlines(new IntPoint(pos), roadData.RoadAndStreetPolys))
                        {
                            pos.y = mesoChunk.GetHeightAt(pos);

                            var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.SettlementBeginSignPrefab, pos,
                                cityBeginAngle, 1f, 0f, 0f, new object[] { cityData.City.Name.ToUpper() });
                            objects.Add(obj);
                            break;
                        }
                    }

                    //5 attempts to place end sign
                    for (int j = -2; j <= 2; j++)
                    {
                        //0, -1, -2, 1, 2
                        float offset = j <= 0 ? -2 - j : j;
                        //try every 4 meters
                        offset *= 4f;

                        var pos = cityEndPos + cityBeginFwd * (j * offset);

                        //Validate position
                        if (!ChunkUtil.IntersectsWithOutlines(new IntPoint(pos), roadData.RoadAndStreetPolys))
                        {
                            pos.y = mesoChunk.GetHeightAt(pos);

                            var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.SettlementEndSignPrefab, pos,
                                cityBeginAngle - 180, 1f, 0f, 0f);
                            objects.Add(obj);
                            break;
                        }
                    }
                    
                    //5 attempts to place speed limit sign
                    for (int j = -2; j <= 2; j++)
                    {
                        //0, -1, -2, 1, 2
                        float offset = j <= 0 ? -2 - j : j;
                        //try every 4 meters
                        offset *= 4f;

                        var pos = cityEndPos + cityBeginFwd * (20 + j * offset);

                        //Validate position
                        if (!ChunkUtil.IntersectsWithOutlines(new IntPoint(pos), roadData.RoadAndStreetPolys))
                        {
                            pos.y = mesoChunk.GetHeightAt(pos);

                            var obj = TerrainObjectMetaData.Generate(AssetManager.Ins.SpeedLimit110SignPrefab, pos,
                                cityBeginAngle - 180, 1f, 0f, 0f);
                            objects.Add(obj);
                            break;
                        }
                    }

                }
            }
        }
    }

    /// <summary>
    /// Generates village begin and end signs.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="villageData"></param>
    /// <param name="objects"></param>
    private void GenerateVillageTrafficSignsAsync(MesoChunk mesoChunk, MesoChunkVillageData villageData,
        List<TerrainObjectMetaData> objects)
    {
        var roadData = mesoChunk.Data.RoadData;
        var centralNode = roadData.CentralMesoRoadNode;

        foreach (var road in centralNode.Neighbours)
        {
            var segment = roadData.CentralRoadSegments[road];
            var spline = segment.Spline;

            #region village begin/end signs
            for (int i = 0; i < spline.Count - 2; i++)
            {
                var intersection =
                    ChunkUtil.LineSegmentIntersectionWithPolygon(spline[i], spline[i + 1],
                        villageData.VillageRegion.Outline);

                if (intersection.HasValue)
                {
                    //Found spline segment that intersects with village border

                    var villageBeginFwd = (spline[i + 1] - spline[i]).normalized;
                    var villageBeginRight = new Vector3(villageBeginFwd.z, 0, -villageBeginFwd.x);

                    var villageBeginPos =
                        intersection.Value - villageBeginRight * Const.SIDE_ROAD_WIDTH_IN_METERS * 0.7f;
                    villageBeginPos.y = mesoChunk.GetHeightAt(villageBeginPos);
                    var villageEndPos = intersection.Value + villageBeginRight * Const.SIDE_ROAD_WIDTH_IN_METERS * 0.7f;
                    villageEndPos.y = mesoChunk.GetHeightAt(villageEndPos);

                    var villageBeginAngle =
                        -(float) Math.Atan2(villageBeginFwd.z, villageBeginFwd.x) * Mathf.Rad2Deg + 180;

                    var villageBeginSign = TerrainObjectMetaData.Generate(AssetManager.Ins.SettlementBeginSignPrefab,
                        villageBeginPos, villageBeginAngle, 1f, 0f, 0f, new object[] { villageData.Name.ToUpper() });
                    objects.Add(villageBeginSign);

                    var villageEndSign = TerrainObjectMetaData.Generate(AssetManager.Ins.SettlementEndSignPrefab,
                        villageEndPos, villageBeginAngle - 180, 1f, 0f, 0f);
                    objects.Add(villageEndSign);

                    //Continue a bit along the spline and place a 70 sign
                    var j = Mathf.Clamp(i + 2, 0, spline.Count - 1);

                    var speedLimitFwd = (spline[j + 1] - spline[j]).normalized;
                    var speedLimitRight = new Vector3(speedLimitFwd.z, 0, -speedLimitFwd.x);

                    var speedLimitPos1 = spline[j] - speedLimitFwd * 2 -
                                        speedLimitRight * Const.SIDE_ROAD_WIDTH_IN_METERS * 0.7f;
                    speedLimitPos1.y = mesoChunk.GetHeightAt(speedLimitPos1);

                    var speedLimitSign1 = TerrainObjectMetaData.Generate(AssetManager.Ins.SpeedLimit70SignPrefab,
                        speedLimitPos1, villageBeginAngle, 1f, 0f, 0f);
                    objects.Add(speedLimitSign1);

                    var speedLimitPos2 = spline[j] - speedLimitFwd * 2 +
                                         speedLimitRight * Const.SIDE_ROAD_WIDTH_IN_METERS * 0.7f;
                    speedLimitPos2.y = mesoChunk.GetHeightAt(speedLimitPos2);

                    var speedLimitSign2 = TerrainObjectMetaData.Generate(AssetManager.Ins.SpeedLimit90SignPrefab,
                        speedLimitPos2, villageBeginAngle + 180, 1f, 0f, 0f);
                    objects.Add(speedLimitSign2);
                }
            }
            #endregion
        }
    }
}