﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;

public class MesoChunkRoadGenerator
{
    private MesoChunk mesoChunk;

    private MesoChunkCityData cityData;

    private Point2 mesoChunkPos;

    public MesoChunkRoadGenerator(MesoChunk mesoChunk, MesoChunkCityData cityData)
    {
        this.mesoChunk = mesoChunk;
        this.cityData = cityData;
        mesoChunkPos = mesoChunk.Pos;
    }

    /// <summary>
    /// Generates and returns the mesoroaddata for a specific mesochunk.
    /// </summary>
    /// <returns></returns>
    public MesoRoadData GenerateRoadsAsync()
    {
        var roadSegments = new List<RoadSegment>();
        var intersectingRoadSegments = new List<RoadSegment>();
        //Central road node neighbour -> road segment
        var centralRoadSegments = new Dictionary<Vector3, RoadSegment>();
        //Chunk position -> chunk meso road node
        var mesoRoadNodes = new Dictionary<Point2, MesoRoadNode>();
        var centerRoadOutline = new List<IntPoint>();
        var centerShoulderOutline = new List<IntPoint>();
        
        #region Generate neighbouring road nodes and check if any of their segments intersect this chunk
        for (int x = mesoChunkPos.x - 2; x <= mesoChunkPos.x + 2; x++)
        {
            for (int z = mesoChunkPos.z - 2; z <= mesoChunkPos.z + 2; z++)
            {
                var pos = new Point2(x, z);

                var mesoNode = GenerateMesoRoadNodeAsync(pos);
                if (mesoNode != null)
                {
                    mesoRoadNodes.Add(pos, mesoNode);
                    var segments = GenerateMesoRoadNodeRoadSegmentsAsync(mesoNode);
                    roadSegments.AddRange(segments);
                }
            }
        }
        #endregion

        #region Separate central road segments and calculate their outline
        var centralRoadNode = mesoRoadNodes.ContainsKey(mesoChunkPos) ? mesoRoadNodes[mesoChunkPos] : null;
        if (centralRoadNode != null)
        {
            var center = centralRoadNode.GlobalPos;

            foreach (var roadSegment in roadSegments)
            {
                if (roadSegment.V1 == center)
                {
                    centralRoadSegments.Add(roadSegment.V2, roadSegment);
                    intersectingRoadSegments.Add(roadSegment);
                }
                else if (roadSegment.V2 == center)
                {
                    centralRoadSegments.Add(roadSegment.V1, roadSegment);
                    intersectingRoadSegments.Add(roadSegment);
                }
            }

            var outlines = CalculateCentralRoadNodeOutlineAndCenterOutlinesAsync(centralRoadNode, centralRoadSegments);
            centerRoadOutline = outlines.x;
            centerShoulderOutline = outlines.z;
        }
        #endregion

        #region Separate road segments that are not intersecting with this chunk and generate an outline for the rest
        for (int i = roadSegments.Count - 1; i >= 0; i--)
        {
            var roadSegment = roadSegments[i];
            if (roadSegment.RoadOutline != null)
            {
                //Road segment is a central road segment and already generated
                continue;
            }

            if (ChunkUtil.RoadSegmentIntersectsChunk(mesoChunk.RoadRect, roadSegment))
            {
                var vertices = GenerateRoadSegmentRoadAndShoulderVerticesAsync(roadSegment.Spline, false);
                roadSegment.SetRoadAndShoulderVertices(vertices.x, vertices.z);
                intersectingRoadSegments.Add(roadSegment);
            }
        }
        
        if (centerRoadOutline.Count < 5)
        {
            centerRoadOutline = null;
            centerShoulderOutline = null;
        }
        #endregion

        #region Add highway segments

//        if (MacroChunk.Data.RoadData.MesoChunkContainsHighway(Pos))
//        {
//            var clipper = new Clipper();
//            //TODO: Replace with intersectingroadsegments once implemented
//            foreach (var highwaySegment in MacroChunk.Data.RoadData.RoadSegments)
//            {
//                for (int i = 0; i < highwaySegment.RoadRects.Count - 1; i++)
//                {
//                    var segmentRect = highwaySegment.RoadRects[i];
//
//                    if (segmentRect.Overlaps(Rect))
//                    {
//                        //Relevant vertices for this segment are:
//                        var highwayVertices = new List<IntPoint> {
//                        highwaySegment.RoadVertices[i].x,
//                        highwaySegment.RoadVertices[i].z,
//                        highwaySegment.RoadVertices[i+1].z,
//                        highwaySegment.RoadVertices[i+1].x };
//
//                        clipper.Clear();
//                        clipper.AddPath(ChunkRectOutline, PolyType.ptClip, true);
//                        clipper.AddPath(highwayVertices, PolyType.ptSubject, true);
//                        var polys = new List<List<IntPoint>>();
//                        clipper.Execute(ClipType.ctIntersection, polys, PolyFillType.pftNonZero,
//                            PolyFillType.pftNonZero);
//
//                        if (polys.Count == 0)
//                        {
//                            //No intersection 
//                            continue;
//                        }
//                        if (polys.Count > 1)
//                        {
//                            Debug.Log("Shouldn't happen");
//                            continue;
//                        }
//
//                        Debug.Log("Adding");
//                        roadSegments.Add(highwaySegment);
//
//                    }
//
//                }
//            }
//        }

        #endregion
        
        var mesoRoadData = new MesoRoadData(centralRoadSegments, roadSegments, intersectingRoadSegments, centralRoadNode, mesoRoadNodes, centerRoadOutline, centerShoulderOutline);
        GenerateRoadAndShoulderPolysAsync(mesoRoadData, mesoChunk.MacroChunk.Data.RoadData);

        if(mesoRoadData.CentralMesoRoadNode != null && cityData != null)
            GenerateCrosswalks(mesoRoadData);

        return mesoRoadData;
    }

    /// <summary>
    /// Generates crosswalks based on the given mesoroaddata.
    /// </summary>
    /// <param name="mesoRoadData"></param>
    public static void GenerateCrosswalks(MesoRoadData mesoRoadData)
    {

        if (mesoRoadData.CentralMesoRoadNode == null)
            return;

        var crosswalks = mesoRoadData.Crosswalks ?? new List<Crosswalk>();
        var clipper = new Clipper();
        foreach (var neighbour in mesoRoadData.CentralMesoRoadNode.Neighbours)
        {

            var segment = mesoRoadData.CentralRoadSegments[neighbour];
            var v1 = segment.Spline[0];
            var v2 = segment.Spline[1];
            var fwd = (v2 - v1).normalized;
            var mid = v1 + fwd * Const.SIDE_ROAD_WIDTH_IN_METERS;
            var right = new Vector3(fwd.z, 0, -fwd.x);

            var crosswalkInitialPoly = new List<IntPoint>();
            crosswalkInitialPoly.Add(new IntPoint(mid + fwd * 2 + right * Const.SIDE_ROAD_WIDTH_IN_METERS));
            crosswalkInitialPoly.Add(new IntPoint(mid + fwd * 2 - right * Const.SIDE_ROAD_WIDTH_IN_METERS));
            crosswalkInitialPoly.Add(new IntPoint(mid - fwd * 2 - right * Const.SIDE_ROAD_WIDTH_IN_METERS));
            crosswalkInitialPoly.Add(new IntPoint(mid - fwd * 2 + right * Const.SIDE_ROAD_WIDTH_IN_METERS));

            GenerateCrosswalk(clipper, crosswalkInitialPoly, segment.RoadOutline,
                mid - fwd * 2 - right * Const.SIDE_ROAD_WIDTH_IN_METERS / 2,
                mid + fwd * 2 - right * Const.SIDE_ROAD_WIDTH_IN_METERS / 2, crosswalks);

            //Stop after 1 iteration if there are exactly 2 neighbours
            //In this case there is no junction but one crossing is still logical
            //However 2 very close by would not be
            if (mesoRoadData.CentralMesoRoadNode.Neighbours.Count == 2)
                break;
        }

        mesoRoadData.SetCrosswalks(crosswalks);
    }

    /// <summary>
    /// Generates a crosswalk using the road outline, the direction of the crosswalk (defined via left and right edge).
    /// </summary>
    /// <param name="clipper"></param>
    /// <param name="crosswalkInititalPoly"></param>
    /// <param name="roadPointOutline"></param>
    /// <param name="left"></param>
    /// <param name="right"></param>
    /// <param name="crosswalks"></param>
    public static void GenerateCrosswalk(Clipper clipper, List<IntPoint> crosswalkInititalPoly, List<IntPoint> roadPointOutline, Vector3 left, Vector3 right, List<Crosswalk> crosswalks)
    {
        clipper.Clear();

        clipper.AddPath(roadPointOutline, PolyType.ptClip, true);

        clipper.AddPath(crosswalkInititalPoly, PolyType.ptSubject, true);

        var crosswalkPolys = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctIntersection, crosswalkPolys, PolyFillType.pftNonZero,
            PolyFillType.pftNonZero);

        foreach (var crosswalkPoly in crosswalkPolys)
        {
            var crosswalk = new Crosswalk(crosswalkPoly, left, right);
            crosswalks.Add(crosswalk);
        }
    }


    /// <summary>
    /// Generates the outlines of roads around the central meso road node and also the outline of the center polygon of the side road node junction.
    /// </summary>
    /// <param name="centralNode"></param>
    /// <param name="centralRoadSegments"></param>
    /// <returns>A pair of outlines. The first outline is the central road segment outline, the second outline is the central shoulder region outline.</returns>
    private Pair<List<IntPoint>> CalculateCentralRoadNodeOutlineAndCenterOutlinesAsync(MesoRoadNode centralNode, Dictionary<Vector3, RoadSegment> centralRoadSegments)
    {

        var centerRoadOutline = new List<IntPoint>();
        var centerShoulderOutline = new List<IntPoint>();
        var centerGlobalPos = centralNode.GlobalPos;
        var orderedNeighbours = centralNode.Neighbours.OrderBy(n => Math.Atan2(n.z - centerGlobalPos.z, n.x - centerGlobalPos.x)).ToList();
        var l = orderedNeighbours.Count;

        if (l > 1)
        {
            //At least 2 splines
            for (int i = 0; i < l; i++)
            {
                var leftGlobalPos = orderedNeighbours[(i - 1 + l) % l];
                var neighbourGlobalPos = orderedNeighbours[i];
                var rightGlobalPos = orderedNeighbours[(i + 1) % l];

                var leftSegment = centralRoadSegments[leftGlobalPos];
                var neighbourSegment = centralRoadSegments[neighbourGlobalPos];
                var rightSegment = centralRoadSegments[rightGlobalPos];

                //TODO: We actually only need the 2nd spline element from the center side
                //Reverse the spline if the left hand side is not the center
                var leftSpline = leftSegment.V1 == centerGlobalPos
                    ? leftSegment.Spline
                    : leftSegment.Reverse();
                var neighbourSpline = neighbourSegment.V1 == centerGlobalPos
                    ? neighbourSegment.Spline
                    : neighbourSegment.Reverse();
                var rightSpline = rightSegment.V1 == centerGlobalPos
                    ? rightSegment.Spline
                    : rightSegment.Reverse();

                var vertices = GenerateRoadSegmentRoadAndShoulderVerticesAsync(leftSpline, neighbourSpline, 
                    rightSpline, centerRoadOutline, centerShoulderOutline);
                neighbourSegment.SetRoadAndShoulderVertices(vertices.x, vertices.z);
            }
        }
        else if (l != 0)
        {
            //Just 1 spline
            var neighbourGlobalPos = orderedNeighbours[0];
            var neighbourSegment = centralRoadSegments[neighbourGlobalPos];
            var neighbourSpline = neighbourSegment.V1 == centerGlobalPos
                ? neighbourSegment.Spline
                : neighbourSegment.Reverse();
            var vertices = GenerateRoadSegmentRoadAndShoulderVerticesAsync(neighbourSpline, false);
            neighbourSegment.SetRoadAndShoulderVertices(vertices.x, vertices.z);
        }

        if (!ChunkUtil.IsConvex(centerRoadOutline))
        {
            centerRoadOutline = HullUtil.ConvexHull(centerRoadOutline);
        }
        if (!ChunkUtil.IsConvex(centerShoulderOutline))
        {
            centerShoulderOutline = HullUtil.ConvexHull(centerShoulderOutline);
        }
        
        return new Pair<List<IntPoint>>(centerRoadOutline, centerShoulderOutline);
    }
    
    /// <summary>
    /// Generates the outline for a given spline and does not take into account how either end should look like when merged with other splines
    /// </summary>
    /// <param name="spline"></param>
    /// <param name="canCutEndIfOutOfChunk">If true, the outline will be cut short if the next spline segment starting point is outside of the chunk. 
    /// This is only useful if the spline starts inside the chunk and is guaranteed outgoing.</param>
    /// <returns></returns>
    private Pair<List<Pair<IntPoint>>> GenerateRoadSegmentRoadAndShoulderVerticesAsync(List<Vector3> spline, bool canCutEndIfOutOfChunk)
    {
        var roadRadius = Const.SIDE_ROAD_WIDTH_IN_METERS / 2;
        var roadWithSidewalkRadius = Const.SIDE_ROAD_WIDTH_IN_METERS / 2 + 3;
        var shoulderRadius = Const.SIDE_ROAD_SHOULDER_WIDTH_IN_METERS / 2;

        var roadVertices = new List<Pair<IntPoint>>();
        var shoulderVertices = new List<Pair<IntPoint>>();

        for (int i = 0; i < spline.Count; i++)
        {
            if (canCutEndIfOutOfChunk && i > 0 && !mesoChunk.OnChunk(spline[i - 1], Const.SIDE_ROAD_SHOULDER_WIDTH_IN_METERS * 2))
                //This spline segment is off this chunk so it can't affect this chunk anymore
                break;

            Vector3 v1 = Vector3.up;
            Vector3 v2 = Vector3.up;
            if (i > 0)
            {
                v1 = (spline[i] - spline[i - 1]).normalized;
                v2 = i < spline.Count - 1 ? (spline[i + 1] - spline[i]).normalized : v1;
            }
            else if (i < spline.Count - 1)
            {
                v2 = (spline[i + 1] - spline[i]).normalized;
                v1 = v2;
            }

            var fwd = (v1 + v2).normalized;
            //Rotate 90 degrees
            var right = new Vector3(-fwd.z, 0, fwd.x);

            var roadLeftSideVector = spline[i] - right * roadRadius;
            var roadRightSideVector = spline[i] + right * roadRadius;
            var shoulderLeftSideVector = spline[i] - right * shoulderRadius;
            var shoulderRightSideVector = spline[i] + right * shoulderRadius;

            roadVertices.Add(new Pair<IntPoint>(new IntPoint(roadLeftSideVector), new IntPoint(roadRightSideVector)));
            shoulderVertices.Add(new Pair<IntPoint>(new IntPoint(shoulderLeftSideVector), new IntPoint(shoulderRightSideVector)));

        }
        return new Pair<List<Pair<IntPoint>>>(roadVertices, shoulderVertices);
    }
    
    /// <summary>
    /// Generates the outline of a given spline and takes into account the neighbouring splines
    /// </summary>
    /// <param name="leftSpline"></param>
    /// <param name="neighbourSpline"></param>
    /// <param name="rightSpline"></param>
    /// <param name="centerOutline"></param>
    /// <returns></returns>
    private Pair<List<Pair<IntPoint>>> GenerateRoadSegmentRoadAndShoulderVerticesAsync(List<Vector3> leftSpline, List<Vector3> neighbourSpline, List<Vector3> rightSpline,
        List<IntPoint> centerRoadOutline, List<IntPoint> centerShoulderOutline)
    {
        #region the first segment with the junction
        //Guaranteed to be the global pos of 'centerTile'
        var center = neighbourSpline[0];

        var roadRadius = Const.SIDE_ROAD_WIDTH_IN_METERS / 2;
        var shoulderRadius = Const.SIDE_ROAD_SHOULDER_WIDTH_IN_METERS / 2;

        var forward = (center - neighbourSpline[1]).normalized;
        var right = new Vector3(forward.z, 0, -forward.x);

        //Left edge
        var leftAngleDiff = ChunkUtil.ClockwiseAngle(center, leftSpline[1], neighbourSpline[1]);
        var leftOffsetR = roadRadius / (float)Math.Tan(leftAngleDiff / 2);
        var leftOffsetS = shoulderRadius / (float)Math.Tan(leftAngleDiff / 2);
        var l1r = center - roadRadius * right - forward * leftOffsetR;
        var l1s = center - shoulderRadius * right - forward * leftOffsetS;

        //Right edge
        var rightAngleDiff = ChunkUtil.ClockwiseAngle(center, neighbourSpline[1], rightSpline[1]);
        var rightOffsetR = roadRadius / (float)Math.Tan(rightAngleDiff / 2);
        var rightOffsetS = shoulderRadius / (float)Math.Tan(rightAngleDiff / 2);
        var r1r = center + roadRadius * right - forward * rightOffsetR;
        var r1s = center + shoulderRadius * right - forward * rightOffsetS;


        //We derive a point that is slightly outward from l1 and r1 and store it in a collection
        //After processing all the central road segments, this wil be a nice lovely polygon covering the center of a junction
        //The offsetting is necessary to avoid some ugly floating point problems that might otherwise cause gaps
        centerRoadOutline.Add(new IntPoint(l1r - forward * 0.02f));
        centerRoadOutline.Add(new IntPoint(r1r - forward * 0.02f));
        centerShoulderOutline.Add(new IntPoint(l1s - forward * 0.02f));
        centerShoulderOutline.Add(new IntPoint(r1s - forward * 0.02f));

        #endregion

        var roadVertices = new List<Pair<IntPoint>>();
        var shoulderVertices = new List<Pair<IntPoint>>();

        roadVertices.Add(new Pair<IntPoint>(new IntPoint(l1r), new IntPoint(r1r)));
        shoulderVertices.Add(new Pair<IntPoint>(new IntPoint(l1s), new IntPoint(r1s)));

        #region middle of the spline
        for (int i = 1; i < neighbourSpline.Count; i++)
        {
            //TODO: Disabled due to village generation - only needs to be active if chunk contains a village
            //See -2927 8564 for an example
//            if (!OnChunk(neighbourSpline[i - 1], Const.SIDE_ROAD_SHOULDER_WIDTH_IN_METERS * 2))
//                //This spline segment is off this chunk so it can't affect this chunk anymore
//                break;

            var v1 = (neighbourSpline[i] - neighbourSpline[i - 1]).normalized;
            //Default to v2 if this is the last segment
            var v2 = i < neighbourSpline.Count - 1 ? (neighbourSpline[i + 1] - neighbourSpline[i]).normalized : v1;
            var fwd = (v1 + v2).normalized;
            //Rotate 90 degrees
            right = new Vector3(-fwd.z, 0, fwd.x);

            var roadLeftSideVector = neighbourSpline[i] - right * roadRadius;
            var roadRightSideVector = neighbourSpline[i] + right * roadRadius;

            roadVertices.Add(new Pair<IntPoint>(new IntPoint(roadLeftSideVector), new IntPoint(roadRightSideVector)));

            if (i > 1)
            {
                var shoulderLeftSideVector = neighbourSpline[i] - right * shoulderRadius;
                var shoulderRightSideVector = neighbourSpline[i] + right * shoulderRadius;
                shoulderVertices.Add(new Pair<IntPoint>(new IntPoint(shoulderLeftSideVector),
                    new IntPoint(shoulderRightSideVector)));
            }

        }
        #endregion

        return new Pair<List<Pair<IntPoint>>>(roadVertices, shoulderVertices);
    }

    /// <summary>
    /// Generates all the road segments for the given mesochunk using the given mesoroadnode
    /// </summary>
    /// <param name="mesoNode"></param>
    /// <returns></returns>
    private List<RoadSegment> GenerateMesoRoadNodeRoadSegmentsAsync(MesoRoadNode mesoNode)
    {
        var roadSegments = new List<RoadSegment>();

        foreach (var neighbour in mesoNode.Neighbours)
        {
            if (neighbour.x < mesoNode.GlobalPos.x ||
                neighbour.x <= mesoNode.GlobalPos.x && neighbour.z < mesoNode.GlobalPos.z)
            {
                //This edge has already been processed by a previous loop step in the parent function
                continue;
            }


            var controlPoints = new List<Vector3>();
            var spline = new List<Vector3>();

            var neighbourChunk = CoordUtil.GlobalPositionToMesoChunk(neighbour);

            if (mesoNode.ContinuousRoads.ContainsKey(neighbour))
            {
                var neighbourContinuousPartner = mesoNode.ContinuousRoads[neighbour];
                
                //This node has at least 1 continuous road and this neighbour is a part of one of them
                var p1 = GetNextRoadPosOnRoadAsync(neighbourChunk, neighbour, mesoNode.GlobalPos);
                controlPoints.Add(neighbourContinuousPartner);
                controlPoints.Add(mesoNode.GlobalPos);
                controlPoints.Add(neighbour);
                controlPoints.Add(p1);
                var c = controlPoints;
                var segmentCount = GetNumberOfSplineSegmentsBetween(c[1], c[2]);
                spline = CurveUtil.SegmentWithJoinedStartAndEnd(c[0], c[1], c[2], c[3], segmentCount);
            }
            else
            {
                //Neither end point is on the main road of this chunk so we add it twice
                var p5 = GetNextRoadPosOnRoadAsync(neighbourChunk, neighbour, mesoNode.GlobalPos);
                controlPoints.Add(mesoNode.GlobalPos);
                controlPoints.Add(mesoNode.GlobalPos);
                controlPoints.Add(neighbour);
                controlPoints.Add(p5);

                var c = controlPoints;
                var segmentCount = GetNumberOfSplineSegmentsBetween(c[1], c[2]);
                spline = CurveUtil.SegmentWithJoinedStartAndEnd(c[0], c[1], c[2], c[3], segmentCount);
            }

            var roadSegment = new RoadSegment(mesoNode.GlobalPos, neighbour, spline, Const.SIDE_ROAD_WIDTH_IN_METERS / 2);
            roadSegments.Add(roadSegment);
        }

        return roadSegments;
    }

    /// <summary>
    /// Returns the numberof spline segments to be placed on a side road with the given start and end point. Returns a minimum of 2.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private static int GetNumberOfSplineSegmentsBetween(Vector3 start, Vector3 end)
    {
        var dist = (end - start).magnitude;
        var val = (int)(dist / (Const.SIDE_ROAD_WIDTH_IN_METERS) * 0.3f);
        return val > 1 ? val : 2;
    }

    /// <summary>
    /// Generates a mesoroadnode for the given mesochunk.
    /// </summary>
    /// <param name="mesoChunkPos"></param>
    /// <returns></returns>
    private static MesoRoadNode GenerateMesoRoadNodeAsync(Point2 mesoChunkPos)
    {
        var onHighway = false;
        var roadGlobalPos = MacroChunk.GetMesoChunkRoadGlobalPosition(mesoChunkPos, out onHighway);

        if (!roadGlobalPos.HasValue)
            return null;

        var neighbours = CalculateNeighboursAsync(mesoChunkPos, roadGlobalPos.Value, onHighway);

        var continuousRoads = GenerateContinuousRoadsAsync(roadGlobalPos.Value, neighbours);

        var neighbourControlPoints = new Dictionary<Vector3, List<Vector3>>();
        foreach (var neighbour in neighbours)
        {
            var controlPoints = new List<Vector3>();
            controlPoints.Add(roadGlobalPos.Value);
            controlPoints.Add(neighbour);
            neighbourControlPoints.Add(neighbour, controlPoints);
        }

        return new MesoRoadNode(mesoChunkPos, roadGlobalPos.Value, neighbours, continuousRoads);
    }

    /// <summary>
    /// Generates continuous road pairs between edges defined through centerPos - neighbour in neighbours
    /// </summary>
    /// <param name="centerPos"></param>
    /// <param name="neighbours"></param>
    /// <returns></returns>
    private static Dictionary<Vector3, Vector3> GenerateContinuousRoadsAsync(Vector3 centerPos, List<Vector3> neighbours)
    {
        var continuousRoads = new Dictionary<Vector3, Vector3>();

        //Base cases
        if (neighbours.Count < 2)
        {
            return continuousRoads;
        } else if (neighbours.Count == 2)
        {
            continuousRoads.Add(neighbours[0], neighbours[1]);
            continuousRoads.Add(neighbours[1], neighbours[0]);
            return continuousRoads;
        }

        var candidates = new List<Vector3>(neighbours);

        var culls = 0;
        while (candidates.Count > 1)
        {
            var angle = 0f;
            var continuousRoad = ChunkUtil.FindMostParallelPoints(centerPos, candidates, out angle);
            if (angle > 60 && culls > 0)
            {
                //Stop early if next continuous roads would have a very sharp angle
                break;
            }

            continuousRoads.Add(candidates[continuousRoad.x], candidates[continuousRoad.z]);
            continuousRoads.Add(candidates[continuousRoad.z], candidates[continuousRoad.x]);

            if (continuousRoad.x > continuousRoad.z)
            {
                candidates.RemoveAt(continuousRoad.x);
                candidates.RemoveAt(continuousRoad.z);
            }
            else
            {
                candidates.RemoveAt(continuousRoad.z);
                candidates.RemoveAt(continuousRoad.x);
            }

            culls++;
        }
        return continuousRoads;
    }

    /// <summary>
    /// Generates the neighbouring mesochunk side road nodes for the given mesochunk.
    /// </summary>
    /// <param name="mesoChunkPos"></param>
    /// <param name="roadGlobalPos"></param>
    /// <param name="onHighway"></param>
    /// <returns></returns>
    private static List<Vector3> CalculateNeighboursAsync(Point2 mesoChunkPos, Vector3 roadGlobalPos, bool onHighway)
    {
        var neighbours = new List<Vector3>();
        var neighboursFurther = new List<Vector3>();

        #region generate all potential neighbours
        var r = Const.ROAD_FAR_NEIGHBOUR_RADIUS;
        for (int x = mesoChunkPos.x - r; x <= mesoChunkPos.x + r; x++)
        {
            for (int z = mesoChunkPos.z - r; z <= mesoChunkPos.z + r; z++)
            {
                var otherMesoChunkPos = new Point2(x, z);
                if (mesoChunkPos == otherMesoChunkPos)
                    continue;

                var otherOnHighway = false;
                var otherRoadPos = MacroChunk.GetMesoChunkRoadGlobalPosition(otherMesoChunkPos, out otherOnHighway);
                if (otherRoadPos.HasValue)
                {
                    if (Math.Abs(x - mesoChunkPos.x) <= Const.ROAD_NEIGHBOUR_RADIUS
                        && Math.Abs(z - mesoChunkPos.z) <= Const.ROAD_NEIGHBOUR_RADIUS && (!onHighway || !otherOnHighway))
                    {
                        neighbours.Add(otherRoadPos.Value);
                    }
                    neighboursFurther.Add(otherRoadPos.Value);
                }

            }
        }

        #endregion

        #region cull unsuitable neighbours
        var remainingNeighbours = new List<Vector3>();
        foreach (var p2 in neighbours)
        {
            var p1 = roadGlobalPos;
            //We are looking at an edge p1-p2

            //1. Compare it to other edges for intersects
            bool intersects = ChunkUtil.Intersects(p1, p2, neighboursFurther);

            //2. At each edge p1-p2, check if the angle p3-p1-p2 or p3-p2-p1 is less than x degrees
            var thresh = Const.ROAD_TRIANGLE_MINIMUM_ANGLE;// + neighboursFurther.Count;
            bool acute = ChunkUtil.Acute(p1, p2, neighboursFurther, thresh);

            if (!intersects && acute)
            {
                remainingNeighbours.Add(p2);
            }
        }
        #endregion


        return remainingNeighbours;
    }

    /// <summary>
    /// Gets the next position on a road defined by centerGlobalTile-edge on the given centerMesoChunk by 
    /// </summary>
    /// <returns></returns>
    private Vector3 GetNextRoadPosOnRoadAsync(Point2 centerMesoChunkPos, Vector3 centerGlobalPos, Vector3 neighbourGlobalPos)
    {
        //Guaranteed to exist
        var onHighway = false;
        var roadGlobalPositionMaybe = MacroChunk.GetMesoChunkRoadGlobalPosition(centerMesoChunkPos, out onHighway);

        //        if (!roadGlobalTileMaybe.HasValue)
        //            return centerGlobalTile;

        // ReSharper disable once PossibleInvalidOperationException
        var roadGlobalPos= roadGlobalPositionMaybe.Value;

        var neighbours = CalculateNeighboursAsync(centerMesoChunkPos, roadGlobalPos, onHighway);
        //have to find the neighbour that is most parallel to center-neighbour

        if (neighbours.Count <= 1)
        {
            //This chunk only has <=1 neighbour, default to the input value to avoid any curvature
            return centerGlobalPos;
        }

        var continuousRoads = GenerateContinuousRoadsAsync(centerGlobalPos, neighbours);
        if (continuousRoads.ContainsKey(neighbourGlobalPos))
            return continuousRoads[neighbourGlobalPos];

        //The center-neighbour road is not on the main road of this chunk, meaning it's not affected by the main road
        //so just default to the input value to avoid any curvature
        return centerGlobalPos;

    }

    /// <summary>
    /// Generates the road and road's margin polygons using the road data.
    /// </summary>
    /// <param name="mesoRoadData"></param>
    /// <param name="macroRoadData"></param>
    private void GenerateRoadAndShoulderPolysAsync(MesoRoadData mesoRoadData, MacroRoadData macroRoadData)
    {
        var roadPolys = new List<List<IntPoint>>();
        var shoulderPolys = new List<List<IntPoint>>();

        if (mesoRoadData != null)
        {
            foreach (var segment in mesoRoadData.RoadSegments)
            {
                if (segment.RoadOutline != null)
                    roadPolys.Add(segment.RoadOutline);
                if (segment.ShoulderOutline != null)
                    shoulderPolys.Add(segment.ShoulderOutline);
            }
            if (mesoRoadData.CenterRoadOutline != null)
                roadPolys.Add(mesoRoadData.CenterRoadOutline);
            if (mesoRoadData.CenterShoulderOutline != null)
            {
                shoulderPolys.Add(mesoRoadData.CenterShoulderOutline);
            }
        }
        
        if (macroRoadData != null)
        {
            foreach (var segment in macroRoadData.IntersectingRoadSegments)
            {

                if (segment.RoadOutline != null)
                    roadPolys.Add(segment.RoadOutline);
                if (segment.ShoulderOutline != null)
                    shoulderPolys.Add(segment.ShoulderOutline);
            }

            if (macroRoadData.CenterRoadOutline != null)
                roadPolys.Add(macroRoadData.CenterRoadOutline);
            if (macroRoadData.CenterShoulderOutline != null)
                shoulderPolys.Add(macroRoadData.CenterShoulderOutline);
        }

        mesoRoadData.SetRoadAndShoulderPolys(roadPolys, shoulderPolys);
    }
}
