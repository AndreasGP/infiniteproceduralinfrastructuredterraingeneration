﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;
#pragma warning disable 649

public class MesoTerrainMeshBuilder
{

    public MeshBuilder GenerateTerrainMesh(MesoChunk mesoChunk)
    {
        var builder = new MeshBuilder();


        GenerateRegionMeshes(builder, mesoChunk);

        return builder;
    }
    
    private void GenerateRegionMeshes(MeshBuilder builder, MesoChunk mesoChunk)
    {
        foreach (var region in mesoChunk.Data.Regions)
        {
            if (region.SubRegions != null)
            {
                foreach (var subregion in region.SubRegions)
                {
                    if (!subregion.GenerateTerrainMesh)
                        continue;
                    GenerateRegionMesh(builder, mesoChunk, subregion.PointOutline,
                        subregion.Type, subregion.IsConvex.Value);
                }
            }
            else
            {
                if (!region.GenerateTerrainMesh)
                    continue;
                GenerateRegionMesh(builder, mesoChunk, region.PointOutline, region.Type,
                    region.IsConvex.Value);
            }
        }


    }
    /// <summary>
    /// Generates a terrain mesh for the given region with outline 'outline'
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="outline"></param>
    /// <param name="type"></param>
    /// <param name="isConvex"></param>
    private void GenerateRegionMesh(MeshBuilder builder, MesoChunk mesoChunk, List<IntPoint> outline, RegionType type, bool isConvex)
    {

        var vertices = ClipperUtil.ToVectors(outline);
        
        #region tile intersections
        var intersectingTiles = ChunkUtil.IntersectingTiles(vertices, isConvex);

        var vertexIndices = new Dictionary<Vector3, int>();

        var polygonSides = new Pair<Vector3>[vertices.Count];
        for(int i = 0; i < vertices.Count; i++)
        {
            polygonSides[i] = new Pair<Vector3>(vertices[i], vertices[(i + 1) % vertices.Count]);
        }
        
        var tileSides = new Pair<Vector3>[4];

        var intersectionPoints = new List<Vector3>();

        var len = Const.TILE_WIDTH_IN_METERS;

        foreach (var tile in intersectingTiles)
        {
            intersectionPoints.Clear();

            var upperLeft = new Vector3(tile.x * len, 0, (tile.z + 1) * len);
            var lowerRight = new Vector3((tile.x + 1) * len, 0, tile.z * len);
            var lowerLeft = new Vector3(tile.x * len, 0, tile.z * len);
            var upperRight = new Vector3((tile.x + 1) * len, 0, (tile.z + 1) * len);

            tileSides[0] = new Pair<Vector3>(lowerLeft, upperLeft);
            tileSides[1] = new Pair<Vector3>(lowerRight, lowerLeft);
            tileSides[2] = new Pair<Vector3>(upperLeft, upperRight);
            tileSides[3] = new Pair<Vector3>(upperRight, lowerRight);

            #region tile corner - poly intersections

            var allInside = true;
            foreach (var side in tileSides)
            {
                var tileCorner = side.x;
                //TODO: Could be optimized (same test done up)
                if (ChunkUtil.InsidePolygon(tileCorner, vertices, isConvex, 1))
                {
                    intersectionPoints.Add(tileCorner);
                }
                else
                {
                    allInside = false;
                }
            }


            #endregion

            if (!allInside)
            {
                foreach (var polygonSide in polygonSides)
                {

                    #region tile edge - poly edge intersections

                    foreach (var side in tileSides)
                    {
                        var intersect =
                            ChunkUtil.IntersectionPointWithLineSegments(side.x, side.z, polygonSide.x, polygonSide.z);
                        if (intersect.HasValue)
                        {
                            intersectionPoints.Add(intersect.Value);
                        }
                    }

                    #endregion

                    #region poly corner - tile intersections

                    if (ChunkUtil.IsBetweenTwoVectors(polygonSide.x, lowerLeft, upperRight))
                    {
                        intersectionPoints.Add(polygonSide.x);
                    }

                    #endregion
                }
            }

            #region mesh stuff

            var intersectionCenter = ChunkUtil.CentroidOfPolygon(intersectionPoints);
            intersectionPoints = intersectionPoints.OrderBy(p => Math.Atan2(p.z - intersectionCenter.z, p.x - intersectionCenter.x)).ToList();

            GenerateFan(builder, mesoChunk, intersectionPoints, vertices, vertexIndices, Vector3.zero, Vector3.right, type, 1 / (Const.TILE_WIDTH_IN_METERS * 1.5f));
            #endregion
        }
        #endregion

        vertexIndices.Clear();
    }

    /// <summary>
    /// Returns the vertex color depending on the regionType.
    /// </summary>
    /// <param name="regionType"></param>
    /// <returns></returns>
    public static Color GetVertexColor(RegionType regionType)
    {
        var minorWeight = 0.001f;

        if (regionType.IsForest())
        {
            return new Color(1 - minorWeight, minorWeight, minorWeight, minorWeight);
        }
        if (regionType == RegionType.Plain || regionType.IsVillage())
        {
            return new Color(1 - minorWeight, minorWeight, minorWeight, minorWeight);
        }
        if (regionType == RegionType.Cultivation || regionType == RegionType.SubFarmEmpty || regionType == RegionType.SubFarmBig || regionType == RegionType.SubFarmSmall)
        {
            return new Color(minorWeight, 1 - minorWeight, minorWeight, minorWeight);
        }
        if (regionType == RegionType.NatureReserve)
        {
            return new Color(1 - minorWeight, minorWeight, minorWeight, minorWeight);
        }
        if (regionType == RegionType.City || regionType == RegionType.Sidewalk)
        {
            return new Color(minorWeight, minorWeight,  minorWeight, 1 - minorWeight);
        }
        return new Color(1, 1, 1, 1);
    }
    
    /// <summary>
    /// Generates a fan through orderedVertices.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="orderedVertices"></param>
    /// <param name="polygonVertices"></param>
    /// <param name="vertexIndices"></param>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="regionType"></param>
    /// <param name="texCoordMultiplier"></param>
    private void GenerateFan(MeshBuilder builder, MesoChunk mesoChunk, List<Vector3> orderedVertices, List<Vector3> polygonVertices, Dictionary<Vector3, int> vertexIndices,
        Vector3 leftEdge, Vector3 rightEdge, RegionType regionType, float texCoordMultiplier)
    {
        if (orderedVertices.Count < 3)
        {
            return;
        }

        var vert0Exists = false;
        var vert0 = orderedVertices[0] + new Vector3(0, mesoChunk.GetHeightAt(orderedVertices[0]), 0);
        var index0 = GetVertexIndex(builder, vert0, vertexIndices, out vert0Exists);

        if (!vert0Exists)
        {
            builder.AddVertex(vert0);
            var texCoord0 = GetTextureCoords(leftEdge, rightEdge, orderedVertices[0], texCoordMultiplier);
            builder.AddTexCoord(texCoord0);

            if(ChunkUtil.PointDistanceFromPolygon(orderedVertices[0], polygonVertices) > Const.MAX_TERRAIN_BLEND_DIST)
            {
                var color = GetVertexColor(regionType);
                builder.AddColor(color);
            } else
            {
                var color = GetVertexColor(regionType);
                builder.AddColor(color);
            }
        }

        for (int i = 1; i < orderedVertices.Count - 1; i++)
        {
            var vert1Exists = false;
            var vert1 = orderedVertices[i] + new Vector3(0, mesoChunk.GetHeightAt(orderedVertices[i]), 0);
            var index1 = GetVertexIndex(builder, vert1, vertexIndices, out vert1Exists);
            if (!vert1Exists)
            {
                builder.AddVertex(vert1);
                var texCoord1 = GetTextureCoords(leftEdge, rightEdge, orderedVertices[i], texCoordMultiplier);
                builder.AddTexCoord(texCoord1);
                builder.AddColor(GetVertexColor(regionType));
            }

            var vert2Exists = false;
            var vert2 = orderedVertices[i + 1] + new Vector3(0, mesoChunk.GetHeightAt(orderedVertices[i + 1]), 0);
            var index2 = GetVertexIndex(builder, vert2, vertexIndices, out vert2Exists);
            if (!vert2Exists)
            {
                builder.AddVertex(vert2);
                var texCoord2 = GetTextureCoords(leftEdge, rightEdge, orderedVertices[i + 1], texCoordMultiplier);
                builder.AddTexCoord(texCoord2);
                builder.AddColor(GetVertexColor(regionType));
            }
            builder.AddTriangleIndices(0, index0, index2, index1);
        }
    }

    /// <summary>
    /// Returns a stored vertex index or creates a new vertex index if one did not previously exist.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="vertex"></param>
    /// <param name="vertexIndices"></param>
    /// <param name="exists"></param>
    /// <returns></returns>
    private int GetVertexIndex(MeshBuilder builder, Vector3 vertex, Dictionary<Vector3, int> vertexIndices, out bool exists)
    {
        if (vertexIndices != null && vertexIndices.ContainsKey(vertex))
        {
            exists = true;
            return vertexIndices[vertex];
        }

        var index = builder.getNextIndex();

        if(vertexIndices != null)
        vertexIndices.Add(vertex, index);

        exists = false;
        return index;
    }
    
    /// <summary>
    /// returns the texture coordinates at the given point
    /// </summary>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="point"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <returns></returns>
    private Vector2 GetTextureCoords(Vector3 leftEdge, Vector3 rightEdge, Vector3 point, float texCoordMultiplier)
    {
        var right = (rightEdge - leftEdge).normalized;
        var fwd = new Vector3(-right.z, 0, right.x);


        var middle = (leftEdge + rightEdge) / 2;

        var maxDist = (rightEdge - middle).magnitude;

        var xProjection = ChunkUtil.GetPointProjectionOnLine(point, middle, middle + fwd);
        var yProjection = ChunkUtil.GetPointProjectionOnLine(point, leftEdge, rightEdge);

        var distX = (xProjection - point).magnitude;
        if (ChunkUtil.PointToTheRightOfLine(point, middle, middle + fwd))
        {
            distX *= -1f;
        }

        var distY = (yProjection - point).magnitude;

        return new Vector2(distX / maxDist * texCoordMultiplier, distY / maxDist * texCoordMultiplier);

    }

}
