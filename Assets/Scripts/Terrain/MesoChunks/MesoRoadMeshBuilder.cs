﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;
#pragma warning disable 649

public class MesoRoadMeshBuilder
{

    public MeshBuilder GenerateRoadMesh(MesoChunk mesoChunk)
    {
        var builder = new MeshBuilder();

        if (mesoChunk.Data.ContainsHighway)
        {
            GenerateMacroChunkRoads(builder, mesoChunk);
        }

        GenerateMesoChunkRoads(builder, mesoChunk);

        GenerateStreets(builder, mesoChunk);

        GenerateCrosswalks(builder, mesoChunk);

        return builder;
    }

    private void GenerateMacroChunkRoads(MeshBuilder builder, MesoChunk mesoChunk)
    {
        var roadSegments = mesoChunk.MacroChunk.Data.RoadData.IntersectingRoadSegments;


        var mesoChunkRoadRect = ChunkUtil.GetMesoChunkRect(mesoChunk.Pos.x, mesoChunk.Pos.z,
            Const.HIGHWAY_WIDTH_IN_METERS);
        foreach (var segment in roadSegments)
        {
            for (int i = 0; i < segment.Spline.Count - 1; i++)
            {
                var s1 = segment.Spline[i];
                var s2 = segment.Spline[i + 1];

                if (ChunkUtil.LineSegmentIntersectsRect(s1, s2, mesoChunkRoadRect))
                {

                    if (i + 1 < segment.RoadVertices.Count)
                    {

                        //Relevant vertices for this segment are:
                        var roadVertices = new List<IntPoint>
                        {
                            segment.RoadVertices[i].x,
                            segment.RoadVertices[i].z,
                            segment.RoadVertices[i + 1].z,
                            segment.RoadVertices[i + 1].x
                        };


                        var vertices = roadVertices;
                        if (!ChunkUtil.RectContainsPolygon(mesoChunk.Rect, vertices))
                        {
                            //TODO: make reusable clipper
                            //The polygon defined by roadVertices only partially intersects with the chunk, let's find the exact intersection
                            var clipper = new Clipper();
                            clipper.AddPath(mesoChunk.ChunkRectOutline, PolyType.ptClip, true);
                            clipper.AddPath(roadVertices, PolyType.ptSubject, true);
                            var polys = new List<List<IntPoint>>();
                            clipper.Execute(ClipType.ctIntersection, polys, PolyFillType.pftNonZero,
                                PolyFillType.pftNonZero);

                            if (polys.Count == 0)
                            {
                                //No actual intersection 
                                continue;
                            }

                            if (polys.Count > 1)
                            {
                                Debug.Log("Shouldn't happen");
                                continue;
                            }

                            vertices = polys[0];
                        }

                        var fwd = (s2 - s1).normalized;
                        var right = new Vector3(fwd.z, 0, -fwd.x);

                        GenerateRoadSplineSegment(builder, mesoChunk, vertices,
                            s1 - right * Const.HIGHWAY_WIDTH_IN_METERS / 2,
                            s1 + right * Const.HIGHWAY_WIDTH_IN_METERS / 2, 1, 0);
                    }
                    else
                    {
                        Debug.Log("Shouldnt happen");
                    }

                }
            }
        }

        var roadData = mesoChunk.MacroChunk.Data.RoadData;
        var centerOutline = roadData.CenterRoadOutline;
        if (centerOutline != null)
        {
            var firstContinuousRoad1 = roadData.CentralMacroRoadNode.ContinuousRoads.Keys.First();
            var firstContinuousRoad2 = roadData.CentralMacroRoadNode.ContinuousRoads[firstContinuousRoad1];


            RoadSegment seg1 = null, seg2 = null;

            foreach (var segment in roadData.RoadSegments)
            {
                if (segment.V1 == roadData.CentralMacroRoadNode.GlobalPos &&
                    (segment.V2 == firstContinuousRoad1 || segment.V2 == firstContinuousRoad2))
                {
                    if (seg1 == null)
                        seg1 = segment;
                    else
                    {
                        seg2 = segment;
                        break;
                    }
                }
            }

            if (seg1 != null && seg2 != null)
            {
                if (centerOutline.Count > 2 && seg1.RoadVertices != null && seg2.RoadVertices != null)
                {
                    GenerateJunction(builder, mesoChunk, seg1.RoadVertices[0].x.ToVec3(),
                        seg1.RoadVertices[0].z.ToVec3(),
                        seg2.RoadVertices[0].x.ToVec3(), seg2.RoadVertices[0].z.ToVec3(), 2,
                        Const.HIGHWAY_WIDTH_IN_METERS / 2);

                }

            }
        }

    }

    private void GenerateMesoChunkRoads(MeshBuilder builder, MesoChunk mesoChunk)
    {
        var roadSegments = mesoChunk.Data.RoadData.IntersectingRoadSegments;

        var mesoChunkClip = ChunkUtil.GetRectAsIntPoints(mesoChunk.Rect);

        var mesoChunkRoadRect = mesoChunk.RoadRect;

        foreach (var segment in roadSegments)
        {
            for (int i = 0; i < segment.Spline.Count - 1; i++)
            {
                var s1 = segment.Spline[i];
                var s2 = segment.Spline[i + 1];

                if (ChunkUtil.LineSegmentIntersectsRect(s1, s2, mesoChunkRoadRect))
                {
                    if (i + 1 < segment.RoadVertices.Count)
                    {
                        //Relevant vertices for this segment are:
                        var roadVertices = new List<IntPoint>
                        {
                            segment.RoadVertices[i].x,
                            segment.RoadVertices[i].z,
                            segment.RoadVertices[i + 1].z,
                            segment.RoadVertices[i + 1].x
                        };


                        var vertices = roadVertices;
                        if (!ChunkUtil.RectContainsPolygon(mesoChunk.Rect, vertices))
                        {
                            //The polygon defined by roadVertices only partially intersects with the chunk, let's find the exact intersection
                            var chunkClipper = new Clipper();
                            chunkClipper.AddPath(mesoChunkClip, PolyType.ptClip, true);
                            chunkClipper.AddPath(roadVertices, PolyType.ptSubject, true);
                            var polys = new List<List<IntPoint>>();
                            chunkClipper.Execute(ClipType.ctIntersection, polys, PolyFillType.pftNonZero,
                                PolyFillType.pftNonZero);

                            if (polys.Count == 0)
                            {
                                //No actual intersection 
                                continue;
                            }

                            if (polys.Count > 1)
                            {
                                Debug.Log("Shouldn't happen");
                                continue;
                            }

                            vertices = polys[0];
                        }

                        //Also cull by any highways
                        var highwaySegments = mesoChunk.MacroChunk.Data.RoadData.IntersectingRoadSegments;

                        var clipper = new Clipper();
                        var sideroadRect = segment.RoadRects[i];

                        var intersectsWithHighway = false;
                        foreach (var highwaySegment in highwaySegments)
                        {
                            for (int j = 0; j < highwaySegment.RoadRects.Count; j++)
                            {
                                if (highwaySegment.RoadRects[j].Overlaps(sideroadRect))
                                {
                                    var highwayVertices = new List<IntPoint>
                                    {
                                        highwaySegment.RoadVertices[j].x,
                                        highwaySegment.RoadVertices[j].z,
                                        highwaySegment.RoadVertices[j + 1].z,
                                        highwaySegment.RoadVertices[j + 1].x
                                    };

                                    clipper.AddPath(highwayVertices, PolyType.ptClip, true);
                                    intersectsWithHighway = true;
                                }
                            }
                        }

                        if (intersectsWithHighway)
                        {
                            clipper.AddPath(vertices, PolyType.ptSubject, true);

                            var polys = new List<List<IntPoint>>();
                            clipper.Execute(ClipType.ctDifference, polys, PolyFillType.pftNonZero,
                                PolyFillType.pftNonZero);

                            if (polys.Count == 0)
                            {
                                //No actual intersection 
                                continue;
                            }

                            if (polys.Count > 1)
                            {
                                Debug.Log("Shouldn't happen");
                                continue;
                            }

                            vertices = polys[0];
                        }

                        var fwd = (s2 - s1).normalized;
                        var right = new Vector3(fwd.z, 0, -fwd.x);

                        GenerateRoadSplineSegment(builder, mesoChunk, vertices,
                            s1 - right * Const.SIDE_ROAD_WIDTH_IN_METERS / 2,
                            s1 + right * Const.SIDE_ROAD_WIDTH_IN_METERS / 2, 0.5f, 0);
                    }
                    else
                    {
                        Debug.Log("Shouldnt happen");
                    }

                }
            }
        }

        var roadData = mesoChunk.Data.RoadData;
        var centerOutline = roadData.CenterRoadOutline;
        if (centerOutline != null)
        {
            var firstContinuousRoad1 = roadData.CentralMesoRoadNode.ContinuousRoads.Keys.First();
            var firstContinuousRoad2 = roadData.CentralMesoRoadNode.ContinuousRoads[firstContinuousRoad1];


            RoadSegment seg1 = null, seg2 = null;

            foreach (var segment in roadData.RoadSegments)
            {
                if (segment.V1 == roadData.CentralMesoRoadNode.GlobalPos &&
                    (segment.V2 == firstContinuousRoad1 || segment.V2 == firstContinuousRoad2))
                {
                    if (seg1 == null)
                        seg1 = segment;
                    else
                    {
                        seg2 = segment;
                        break;
                    }
                }
            }

            if (seg1 != null && seg2 != null)
            {
                if (centerOutline.Count > 4 && seg1.RoadVertices != null && seg2.RoadVertices != null)
                {
                    GenerateJunction(builder, mesoChunk, seg1.RoadVertices[0].x.ToVec3(),
                        seg1.RoadVertices[0].z.ToVec3(),
                        seg2.RoadVertices[0].x.ToVec3(), seg2.RoadVertices[0].z.ToVec3(), 1,
                        Const.SIDE_ROAD_WIDTH_IN_METERS / 2);

//                    GenerateRoadSplineSegment(builder, centerOutline, Vector3.zero, Vector3.one, 1);
                }

            }
        }
    }

    private void GenerateStreets(MeshBuilder builder, MesoChunk mesoChunk)
    {
        var streetSegments = mesoChunk.Data.RoadData.StreetSegments;

        if (streetSegments == null)
            return;

        foreach (var street in streetSegments)
        {
            GenerateRoadSplineSegment(builder, mesoChunk, street.PointOutline, street.Left, street.Right, 0.5f,
                street.UseDirtRoadTexture ? 1 : 0);
        }
    }

    private void GenerateCrosswalks(MeshBuilder builder, MesoChunk mesoChunk)
    {
        if (mesoChunk.Data.RoadData.Crosswalks == null)
            return;

        foreach (var crosswalk in mesoChunk.Data.RoadData.Crosswalks)
        {
            GenerateRoadSplineSegment(builder, mesoChunk, crosswalk.PointOutline, crosswalk.Left, crosswalk.Right, 1f,
                2, 0.15f);

        }
    }

    /// <summary>
    /// Generates a junction using the 4 points defining the junction direction.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="mainRoad1l"></param>
    /// <param name="mainRoad1r"></param>
    /// <param name="mainRoad2l"></param>
    /// <param name="mainRoad2r"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <param name="radius"></param>
    private void GenerateJunction(MeshBuilder builder, MesoChunk mesoChunk, Vector3 mainRoad1l, Vector3 mainRoad1r,
        Vector3 mainRoad2l, Vector3 mainRoad2r, float texCoordMultiplier, float radius)
    {
        builder.AddQuadIndices(0);
        var vertices = new Vector3[4];
        vertices[0] = mainRoad1l + new Vector3(0, mesoChunk.GetHeightAt(mainRoad1l), 0);
        vertices[1] = mainRoad1r + new Vector3(0, mesoChunk.GetHeightAt(mainRoad1r), 0);
        vertices[2] = mainRoad2r + new Vector3(0, mesoChunk.GetHeightAt(mainRoad2r), 0);
        vertices[3] = mainRoad2l + new Vector3(0, mesoChunk.GetHeightAt(mainRoad2l), 0);
        builder.AddVertices(vertices);

        var texCoords = new Vector2[4];
        var mainRoad1c = (mainRoad1l + mainRoad1r) / 2;
        var mainRoad2c = (mainRoad2l + mainRoad2r) / 2;
        var fwd = ((mainRoad1l + mainRoad1r) * 0.5f - (mainRoad2l + mainRoad2r) * 0.5f).normalized;
        var right = new Vector3(fwd.z, 0, -fwd.x);

        var offset1l = CalculateTexCoordOffset(mainRoad1c, mainRoad1c + right * radius, mainRoad1l, radius);
        texCoords[0] = new Vector2(0, offset1l); //mainRoad1l

        var offset1r = CalculateTexCoordOffset(mainRoad1c, mainRoad1c - right * radius, mainRoad1r, radius);
        texCoords[1] = new Vector2(texCoordMultiplier, offset1r); //mainRoad1r

        var mainOffset = (mainRoad2c - mainRoad1c).magnitude / radius;

        var offset2r = CalculateTexCoordOffset(mainRoad2c, mainRoad2c + right * radius, mainRoad2r, radius);
        texCoords[2] = new Vector2(0, mainOffset + offset2r); //mainRoad2r

        var offset2l = CalculateTexCoordOffset(mainRoad2c, mainRoad2c - right * radius, mainRoad2l, radius);
        texCoords[3] = new Vector2(texCoordMultiplier, mainOffset + offset2l); //mainRoad2l

        builder.AddTexCoords(texCoords);
    }

    /// <summary>
    /// calculates the tex coord offset at the given queryPoint when the offset from the center of the road and the radius of the road is known.
    /// </summary>
    /// <param name="center"></param>
    /// <param name="offset"></param>
    /// <param name="queryPoint"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    private float CalculateTexCoordOffset(Vector3 center, Vector3 offset, Vector3 queryPoint, float radius)
    {
        var val = (queryPoint - offset).magnitude / (radius * 2f);
        //Now determine if val should be negated or not
        return ChunkUtil.PointToTheRightOfLine(queryPoint, center, offset) ? val : val * -1;
    }

    /// <summary>
    /// Generates a mesh for the given road spline segment with 2 edge points (leftEdge, rightEdge) defined for tex coords.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="verticesAsPoints"></param>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <param name="materialIndex"></param>
    private void GenerateRoadSplineSegment(MeshBuilder builder, MesoChunk mesoChunk, List<IntPoint> verticesAsPoints,
        Vector3 leftEdge, Vector3 rightEdge, float texCoordMultiplier, int materialIndex)
    {
        GenerateRoadSplineSegment(builder, mesoChunk, verticesAsPoints, leftEdge, rightEdge, texCoordMultiplier,
            materialIndex, 0);
    }

    /// <summary>
    /// Generates a mesh for the given road spline segment with 2 edge points (leftEdge, rightEdge) defined for tex coords..
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="verticesAsPoints"></param>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <param name="materialIndex"></param>
    /// <param name="heightOffset"></param>
    private void GenerateRoadSplineSegment(MeshBuilder builder, MesoChunk mesoChunk, List<IntPoint> verticesAsPoints, Vector3 leftEdge, Vector3 rightEdge, float texCoordMultiplier, int materialIndex, float heightOffset)
    {
        var vertices = ClipperUtil.ToVectors(verticesAsPoints);

        var center = ChunkUtil.CentroidOfPolygon(vertices);
        var orderedVertices = vertices.OrderBy(p => Math.Atan2(p.z - center.z, p.x - center.x)).ToList();

        //Easy big mesh:
        //        GenerateFan(builder, orderedVertices, center);
        //        return;

        #region tile intersections
        var intersectingTiles = ChunkUtil.IntersectingTiles(orderedVertices, true);

        var polygonSides = new Pair<Vector3>[orderedVertices.Count];
        for (int i = 0; i < orderedVertices.Count; i++)
        {
            polygonSides[i] = new Pair<Vector3>(orderedVertices[i], orderedVertices[(i + 1) % orderedVertices.Count]);
        }

        var tileSides = new Pair<Vector3>[4];

        var intersectionPoints = new List<Vector3>();

        var len = Const.TILE_WIDTH_IN_METERS;

        foreach (var tile in intersectingTiles)
        {
            intersectionPoints.Clear();

            var upperLeft = new Vector3(tile.x * len, 0, (tile.z + 1) * len);
            var lowerRight = new Vector3((tile.x + 1) * len, 0, tile.z * len);
            var lowerLeft = new Vector3(tile.x * len, 0, tile.z * len);
            var upperRight = new Vector3((tile.x + 1) * len, 0, (tile.z + 1) * len);

            tileSides[0] = new Pair<Vector3>(lowerLeft, upperLeft);
            tileSides[1] = new Pair<Vector3>(lowerRight, lowerLeft);
            tileSides[2] = new Pair<Vector3>(upperLeft, upperRight);
            tileSides[3] = new Pair<Vector3>(upperRight, lowerRight);

            #region tile corner - road intersections

            var allInside = true;
            foreach (var side in tileSides)
            {
                var tileCorner = side.x;

                if (ChunkUtil.InsideConvexPolygon(tileCorner, orderedVertices))
                {
                    intersectionPoints.Add(tileCorner);
                }
                else
                {
                    allInside = false;
                }
            }
            #endregion

            if (!allInside)
            {
                foreach (var polygonSide in polygonSides)
                {
                    #region tile edge - road edge intersections

                    foreach (var side in tileSides)
                    {
                        var intersect =
                            ChunkUtil.IntersectionPointWithLineSegments(side.x, side.z, polygonSide.x, polygonSide.z);
                        if (intersect.HasValue)
                            intersectionPoints.Add(intersect.Value);
                    }

                    #endregion

                    #region road corner - tile intersections

                    if (ChunkUtil.IsBetweenTwoVectors(polygonSide.x, lowerLeft, upperRight))
                        intersectionPoints.Add(polygonSide.x);

                    #endregion
                }
            }

            #region mesh stuff

//            Debug.Log("Intersection points " + intersectionPoints.Count);

            var intersectionCenter = ChunkUtil.CentroidOfPolygon(intersectionPoints);
            intersectionPoints = intersectionPoints.OrderBy(p => Math.Atan2(p.z - intersectionCenter.z, p.x - intersectionCenter.x)).ToList();

            GenerateFan(builder, mesoChunk, intersectionPoints, leftEdge, rightEdge, texCoordMultiplier, materialIndex, heightOffset);
            #endregion
        }
        #endregion

    }

    /// <summary>
    /// Generates a fan mesh through orderedVertices and uses leftEdge, rightEdge to determine the tex coords.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="orderedVertices"></param>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <param name="materialIndex"></param>
    /// <param name="heightoffset"></param>
    private void GenerateFan(MeshBuilder builder, MesoChunk mesoChunk, List<Vector3> orderedVertices, Vector3 leftEdge, Vector3 rightEdge, float texCoordMultiplier, 
        int materialIndex, float heightoffset)
    {
        if (orderedVertices.Count < 3)
        {
            return;
        }

        var vertices = new Vector3[3];
        vertices[0] = orderedVertices[0] + new Vector3(0, mesoChunk.GetHeightAt(orderedVertices[0]) + heightoffset, 0);
        var texCoords = new Vector2[3];
        texCoords[0] = GetTextureCoords(leftEdge, rightEdge, orderedVertices[0], texCoordMultiplier, materialIndex);

        for (int i = 1; i < orderedVertices.Count - 1; i++)
        {
            vertices[1] = orderedVertices[i + 1] + new Vector3(0, mesoChunk.GetHeightAt(orderedVertices[i + 1]) + heightoffset, 0);
            vertices[2] = orderedVertices[i] + new Vector3(0, mesoChunk.GetHeightAt(orderedVertices[i]) + heightoffset, 0);
            builder.AddTriangleIndices(materialIndex);
            builder.AddVertices(vertices);

            texCoords[1] = GetTextureCoords(leftEdge, rightEdge, orderedVertices[i + 1], texCoordMultiplier, materialIndex);
            texCoords[2] = GetTextureCoords(leftEdge, rightEdge, orderedVertices[i], texCoordMultiplier, materialIndex);
            builder.AddTexCoords(texCoords);
        }
    }

    /// <summary>
    /// Gets the tex coords usign the left and right edge by sampling a rotated plane with (0,0) at leftEdge and (1,0) at rightEdge
    /// </summary>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="point"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <param name="materialIndex"></param>
    /// <returns></returns>
    private Vector2 GetTextureCoords(Vector3 leftEdge, Vector3 rightEdge, Vector3 point, float texCoordMultiplier, int materialIndex)
    {
        //TODO: Optimize by avoiding recalculating same stuff for each tile
        var right = (rightEdge - leftEdge).normalized;
        var fwd = new Vector3(-right.z, 0, right.x);


        var middle = (leftEdge + rightEdge) / 2;

        var maxDist = (rightEdge - middle).magnitude;

        var xProjection = ChunkUtil.GetPointProjectionOnLine(point, middle, middle + fwd);
        var yProjection = ChunkUtil.GetPointProjectionOnLine(point, leftEdge, rightEdge);

        var distX = (xProjection - point).magnitude;
        if (ChunkUtil.PointToTheRightOfLine(point, middle, middle + fwd))
        {
            distX *= -1f;
        }

        var distY = (yProjection - point).magnitude;

        //Special handling for crosswalks
        if (materialIndex == 2)
        {
            return new Vector2((maxDist - distX) / maxDist * texCoordMultiplier * 0.5f, distY / maxDist * texCoordMultiplier * 2f);
        }

        //TODO: bad that we're differentiating highway and sideroad here
        if (texCoordMultiplier == 1f)
        {
            return new Vector2(distX / maxDist * texCoordMultiplier, distY / maxDist * texCoordMultiplier);
        }
        else
        {

            return new Vector2((maxDist - distX) / maxDist * texCoordMultiplier, distY / maxDist * texCoordMultiplier);
        }

    }

}
