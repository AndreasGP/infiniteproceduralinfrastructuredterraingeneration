﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Profiling;

// ReSharper disable PossibleLossOfFraction

[RequireComponent(typeof(MesoChunkWrapper))]
public class MesoChunkRenderer : MonoBehaviour
{

    [SerializeField]
    private MeshFilter terrainMeshFilter;

    [SerializeField]
    private MeshFilter roadMeshFilter;

    [SerializeField]
    private MeshFilter cityMeshFilter;

    private MesoChunkWrapper mesoChunkWrapper;

    private MesoChunk mesoChunk;

    private void Awake()
    {
        mesoChunkWrapper = GetComponent<MesoChunkWrapper>();
        mesoChunkWrapper.ChunkInitialized += ChunkInitialized;
    }

    void ChunkInitialized()
    {
        mesoChunk = mesoChunkWrapper.Chunk;

        if (Settings.GenerateMesoChunkTerrainMeshes)
        {
            CustomSampler sampler = null;
            if(Settings.DebugThreads)
                sampler = CustomSampler.Create("GenerateMesoChunkMeshSampler");

            ThreadPool.QueueUserWorkItem((obj) => {
                if (Settings.DebugThreads)
                {
                    Profiler.BeginThreadProfiling("MesoChunkRendererThreads", "Thread " + mesoChunk.Pos);
                    sampler.Begin();
                }

                try
                {
                    GenerateChunkTerrainMeshAsync();
                    GenerateChunkRoadMeshAsync();
                    if (Settings.GenerateCityBlockMeshes)
                        GenerateCityBlockMeshesAsync();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
                if (Settings.DebugThreads)
                {
                    sampler.End();
                    Profiler.EndThreadProfiling();
                }
            }, null);
        }
    }
    
    private void GenerateChunkTerrainMeshAsync()
    {
        var terrainBuilder = new MesoTerrainMeshBuilder();

        var meshBuilder = terrainBuilder.GenerateTerrainMesh(mesoChunk);

        ActionQueue.Ins.EnqueueHeavy(() => StartCoroutine(GenerateTerrainMeshCoroutine(meshBuilder)));
    }
    
    private IEnumerator GenerateTerrainMeshCoroutine(MeshBuilder meshBuilder)
    {
        if (this == null)
        {
            yield break;
        }
        
        //TODO: Not generate with an offset
        terrainMeshFilter.transform.localPosition = new Vector3(0, -0.2f, 0);

        var mesh = meshBuilder.ToBaseMeshWithoutSubMeshes();
        var indices = meshBuilder.GetIndices();

        if (indices.Count == 0)
            yield break;

        mesh.subMeshCount = indices.Count;

        for (int i = 0; i < mesh.subMeshCount; i++)
        {
            if (indices[i].Count > 0)
            {
                mesh.SetTriangles(indices[i].ToArray(), i);
                yield return null;
            }
        }

        if (mesh != null)
        {
            terrainMeshFilter.sharedMesh = mesh;
            terrainMeshFilter.sharedMesh.name = "Terrain Mesh";
            var collider = terrainMeshFilter.gameObject.GetComponent<MeshCollider>();
            if (collider != null)
                collider.sharedMesh = mesh;
            terrainMeshFilter.sharedMesh.RecalculateNormals();

            var renderer = terrainMeshFilter.gameObject.GetComponent<MeshRenderer>();

            var mat = renderer.sharedMaterial;

            var mats = new Material[mesh.subMeshCount];
            for (int i = 0; i < mats.Length; i++)
            {
                mats[i] = mat;
            }

            renderer.sharedMaterials = mats;
        }

    }

    private void GenerateChunkRoadMeshAsync()
    {
        
        var roadBuilder = new MesoRoadMeshBuilder();
        
        var meshBuilder = roadBuilder.GenerateRoadMesh(mesoChunk);

        ActionQueue.Ins.EnqueueHeavy(() => GenerateRoadMesh(meshBuilder));
    }


    private void GenerateRoadMesh(MeshBuilder meshBuilder)
    {
        if (this == null)
        {
            return;
        }
        
        roadMeshFilter.transform.localPosition = new Vector3();

        var mesh = meshBuilder.ToMesh();
        if (mesh != null)
        {
            roadMeshFilter.sharedMesh = mesh;
            roadMeshFilter.sharedMesh.name = "Road Mesh";
            var collider = roadMeshFilter.gameObject.GetComponent<MeshCollider>();
            if (collider != null)
                collider.sharedMesh = mesh;
            roadMeshFilter.sharedMesh.RecalculateNormals();

            var renderer = roadMeshFilter.GetComponent<MeshRenderer>();
            var matIndices = meshBuilder.GetIndices().Keys;

            var mats = new Material[matIndices.Count];
            var i = 0;
            foreach (var matIndex in matIndices)
            {
                mats[i++] = AssetManager.Ins.RoadMaterials[matIndex];
            }
            
            renderer.sharedMaterials = mats;
        }

        mesoChunk.SetMeshReady();
    }

    private void GenerateCityBlockMeshesAsync()
    {
        var cityBuilder = new MesoCityMeshBuilder();

        var meshBuilder = cityBuilder.GenerateCityMesh(mesoChunk);

        ActionQueue.Ins.EnqueueHeavy(() => GenerateCityMesh(meshBuilder));
    }


    private void GenerateCityMesh(MeshBuilder meshBuilder)
    {
        if (this == null)
        {
            return;
        }
        
        cityMeshFilter.transform.position = new Vector3();

        var mesh = meshBuilder.ToMesh();
        if (mesh != null)
        {
            cityMeshFilter.sharedMesh = mesh;
            cityMeshFilter.sharedMesh.name = "City Mesh";
            var collider = cityMeshFilter.gameObject.GetComponent<MeshCollider>();
            if (collider != null)
                collider.sharedMesh = mesh;
            cityMeshFilter.sharedMesh.RecalculateNormals();

            var renderer = cityMeshFilter.GetComponent<MeshRenderer>();
            var matIndices = meshBuilder.GetIndices().Keys;

            var mats = new Material[matIndices.Count];
            var i = 0;
            foreach (var matIndex in matIndices)
            {
                mats[i++] = AssetManager.Ins.CityMaterials[matIndex];
            }

            renderer.sharedMaterials = mats;

        }
        else
        {
            Destroy(cityMeshFilter.gameObject);
        }
    }

    void OnDrawGizmos()
    {
        if (mesoChunk == null || !mesoChunk.DataReady || !Application.isPlaying)
            return;
        
    }

}
