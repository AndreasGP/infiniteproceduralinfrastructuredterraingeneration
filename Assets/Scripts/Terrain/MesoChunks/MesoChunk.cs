﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;


//NB: MesoChunk is assumed not to change any internal state once DataReady has been set true.
public class MesoChunk
{
    public Action ChunkGeneratedAsync;

    public Point2 Pos { get; private set; }

    public MacroChunk MacroChunk { get; private set; }

    public MesoChunkData Data { get; private set; }

    public bool DataReady { get; private set; }

    public bool MeshReady { get; private set; }

    public bool MarkedForDeletion { get; private set; }

    public Rect Rect { get; private set; }

    public List<IntPoint> RegionInitialOutline { get; private set; }

    //TODO: use it more often
    public List<IntPoint> ChunkRectOutline { get; private set; }

    /// <summary>
    /// Slightly bigger rect that can be used to check for collisions against roads that dont have width yet
    /// </summary>
    public Rect RoadRect { get; private set; }

    public MesoChunk(Point2 mesoChunkPos, MacroChunk macroChunk)
    {
        Pos = mesoChunkPos;
        MacroChunk = macroChunk;
        DataReady = false;
        MeshReady = false;
        MarkedForDeletion = false;
        Rect = ChunkUtil.GetMesoChunkRect(mesoChunkPos.x, mesoChunkPos.z, 0);
        ChunkRectOutline = ChunkUtil.GetRectAsIntPoints(Rect);
        RoadRect = ChunkUtil.GetMesoChunkRect(mesoChunkPos.x, mesoChunkPos.z, Const.MESO_CHUNK_WIDTH_IN_METERS);
    }

    public bool OnChunk(Vector3 pos)
    {
        return pos.x >= Rect.xMin && pos.x < Rect.xMax && pos.z >= Rect.yMin && pos.z < Rect.yMax;
    }
    
    public bool OnChunk(Vector3 pos, float maxOffset)
    {
        return pos.x >= Rect.xMin - maxOffset && pos.x < Rect.xMax + maxOffset && pos.z >= Rect.yMin - maxOffset && pos.z < Rect.yMax + maxOffset;
    }

    public void GenerateAsync()
    {
        var cityData = MacroChunk.GetMesoChunkCityData(Pos);
        
        var roadGenerator = new MesoChunkRoadGenerator(this, cityData);
        var roadData = roadGenerator.GenerateRoadsAsync();

        List<MesoChunkRegion> cityRegions = null;
        if (cityData != null)
        {
            var cityGenerator = new MesoChunkCityGenerator(this, roadData, cityData);
            cityRegions = cityGenerator.GenerateCityStreetsAndBlocksAsync();
        }

        var containsHighway = MacroChunk.Data.RoadData.MesoChunkContainsHighway(Pos);

        var regionGenerator = new MesoChunkRegionGenerator(this);
        MesoChunkRegion villageRegion = null;
        var regions = regionGenerator.GenerateRegionsAsync(roadData, out villageRegion);
        if (cityRegions != null)
            regions.AddRange(cityRegions);

        MesoChunkVillageData villageData = null;
        if (villageRegion != null && roadData != null)
        {
            var villageGenerator = new MesoChunkVillageGenerator(this, roadData, villageRegion);
            villageData = villageGenerator.GenerateVillageDataAsync();
        }
        

        Data = new MesoChunkData(new float[0], roadData, containsHighway, regions, cityData, villageData);

        var terrainPopulator = new MesoChunkTerrainPopulator();
        terrainPopulator.GenerateUtilityPolesAndTrafficSignsAsync(this);

        DataReady = true;



        if (ChunkGeneratedAsync != null)
        {
            ChunkGeneratedAsync();
        }

    }

    public float GetHeightAt(Vector3 globalPos)
    {
        return GetHeightAt(globalPos.x, globalPos.z);
    }

    public float GetHeightAt(float globalX, float globalZ)
    {
        return GetHeightAtS(globalX, globalZ);
    }

    public static float GetHeightAtS(Vector3 globalPos)
    {
        return GetHeightAtS(globalPos.x, globalPos.z);
    }

    public static float GetHeightAtS(float globalX, float globalZ)
    {
//        return 0;
        return NoiseService.GetTerrainNoiseAtPos(globalX, globalZ) * Const.HEIGHTMAP_RANGE_IN_METERS;

    }

    private float[] GenerateNoiseAsync()
    {
        var w = Const.MESO_CHUNK_WIDTH_IN_TILES;
        var noise = new float[(w + 1) * (w + 1)];
        var offset = CoordUtil.MesoChunkToGlobalPosition(Pos);
        for (int x = 0; x < w + 1; x++)
        {
            for (int z = 0; z < w + 1; z++)
            {
                var i = z * (w + 1) + x;
                var noiseX = offset.x + x * Const.TILE_WIDTH_IN_METERS;
                var noiseZ = offset.z + z * Const.TILE_WIDTH_IN_METERS;
                var val = NoiseService.GetNoiseAtPos(noiseX, noiseZ);
                noise[i] = val;
            }
        }
        return noise;
    }

    public void MarkForDeletion()
    {
        MarkedForDeletion = true;
    }

    public void SetMeshReady()
    {
        MeshReady = true;
    }

}