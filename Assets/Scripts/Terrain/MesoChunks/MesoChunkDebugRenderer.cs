﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[RequireComponent(typeof(MesoChunkWrapper))]
public class MesoChunkDebugRenderer : MonoBehaviour
{

    private static Color chunkBorderColor = new Color(0f, 0f, 0.55f, 0.7f);
    private static Color roadColor = new Color(64 / 256f, 32 / 256f, 99 / 256f);
    private MesoChunkWrapper mesoChunkWrapper;

    private MesoChunk mesoChunk;

    private void Awake()
    {
        mesoChunkWrapper = GetComponent<MesoChunkWrapper>();
        mesoChunkWrapper.ChunkInitialized += ChunkInitialized;
    }

    void ChunkInitialized()
    {
        mesoChunk = mesoChunkWrapper.Chunk;

    }

    void OnDrawGizmos()
    {
        DrawGizmos(SettingsLevel.Always, transform.parent.parent.position);
    }

    void OnDrawGizmosSelected()
    {
        DrawGizmos(SettingsLevel.WhenSelected, transform.parent.parent.position);
    }

    private void ThickLine(Vector3 v1, Vector3 v2, float w)
    {
        var dir = (v2 - v1).normalized;
        var right = new Vector3(dir.z, 0, -dir.x);

        for (int j = -10; j <= 10; j++)
        {
            Gizmos.DrawLine(v1 + right * j * w, v2 + right * j * w);
        }
    }

    private void ThickLineV(Vector3 v1, Vector3 v2, float w)
    {

        for (int i = -10; i <= 10; i++)
        {
            for (int j = -10; j <= 10; j++)
            {
                Gizmos.DrawLine(v1 + new Vector3((i - 5) * w, 0, (j - 5) * w), v2 + new Vector3((i - 5) * w, 0, (j - 5) * w));
            }
        }
    }


    private void DrawGizmos(SettingsLevel level, Vector3 basePosition)
    {
        if (mesoChunk == null || !mesoChunk.DataReady || !Application.isPlaying)
            return;


#if UNITY_EDITOR
        if (Settings.DrawMesoChunkPosition == level)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = chunkBorderColor;

            var p = (transform.position +
                     new Vector3(Const.MESO_CHUNK_WIDTH_IN_METERS*0.4f, 0, Const.MESO_CHUNK_WIDTH_IN_METERS*0.45f));
            Handles.Label(p, mesoChunk.Pos.ToString(), style);
        }

        if(Settings.DrawMesoRegionSubregionNames == level)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = chunkBorderColor;

            foreach (var region in mesoChunk.Data.Regions)
            {
                if (region.SubRegions == null)
                    continue;

                foreach (var subregion in region.SubRegions)
                {
                    var centroid = ChunkUtil.CentroidOfPolygon(subregion.Outline);
                    var p = basePosition + centroid - new Vector3(Camera.main.transform.position.y / 15, 0, 0);
                    Handles.Label(p, subregion.Type.ToString(), style);
                }
            }
        }
#endif

        if (Settings.DrawMesoChunkBorders == level) {
            Gizmos.color = chunkBorderColor;
            var start = basePosition + CoordUtil.MesoChunkToGlobalPosition(mesoChunk.Pos);
            Gizmos.DrawLine(start, start + new Vector3(Const.MESO_CHUNK_WIDTH_IN_METERS, 0, 0));
            Gizmos.DrawLine(start, start + new Vector3(0, 0, Const.MESO_CHUNK_WIDTH_IN_METERS));
        }



        if (mesoChunk.Data.RoadData != null)
            DrawRoadGizmos(level, basePosition);

        DrawRegionBorderGizmos(level, basePosition);

        DrawVillageGizmos(level, basePosition);

        if (Settings.DrawCityBlocks == level && mesoChunk.Data.CityData != null && mesoChunk.Data.CityData.CityBlocks != null)
        {
            Gizmos.color = new Color(0.5f, 0f, 1, 1f);
            var blocks = mesoChunk.Data.CityData.CityBlocks;
            lock (blocks)
            {
                foreach (var block in blocks)
                {
                    for (int i = 0; i < block.Count; i++)
                    {
                        var p1 = block[i].ToVec3();
                        var p2 = block[(i + 1) % block.Count].ToVec3();
                        Gizmos.DrawLine(basePosition + p1, basePosition + p2);
                    }
                }
            }
        }

    }

    private void DrawVillageGizmos(SettingsLevel level, Vector3 basePosition)
    {
        var villageData = mesoChunk.Data.VillageData;

        if (villageData == null || Settings.DrawVillageBorders != level)
            return;

        Gizmos.color = new Color(1, 0, 0, 0.7f);
        foreach (var villageArea in villageData.VillageAreas)
        {
            for (int i = 0; i < villageArea.Outline.Count; i++)
            {
                var v1 = villageArea.Outline[i].ToVec3();
                var v2 = villageArea.Outline[(i + 1) % villageArea.Outline.Count].ToVec3();
                Gizmos.DrawLine(basePosition + v1, basePosition + v2);
            }

        }


    }

    private void DrawRegionBorderGizmos(SettingsLevel level, Vector3 basePosition)
    {
        var regions = mesoChunk.Data.Regions;

        if (regions == null)
            return;

        foreach (var region in regions)
        {
            if (Settings.DrawMesoRegionBorders == level)
            {
                if (region.Type == RegionType.City)
                    Gizmos.color = new Color(1, 0, 0, 0.2f);
                else
                    Gizmos.color = new Color(0, 1, 0, 0.2f);

                for (int i = 0; i < region.Outline.Count; i++)
                {
                    var v1 = region.Outline[i];
                    var v2 = region.Outline[(i + 1) % region.Outline.Count];
                    Gizmos.DrawLine(basePosition + v1, basePosition + v2);
                }
            }

            if (Settings.DrawMesoRegionSubregions == level && region.SubRegions != null)
            {
                Gizmos.color = new Color(0, 1, 0, 1f);
                foreach (var subregion in region.SubRegions)
                {

                    for (int i = 0; i < subregion.Outline.Count; i++)
                    {
                        var v1 = subregion.Outline[i];
                        var v2 = subregion.Outline[(i + 1) % subregion.Outline.Count];
                        
                        Gizmos.DrawLine(basePosition + v1, basePosition + v2);
                    }
                }
            }
        }
    }


    private void DrawRoadGizmos(SettingsLevel level, Vector3 basePosition)
    {
        if (Settings.DrawSideRoadSplines == level)
        {
            var roadSegments = mesoChunk.Data.RoadData.IntersectingRoadSegments;
            if (roadSegments != null && roadSegments.Count > 0)
            {

                foreach (var segment in roadSegments)
                {
                    for (int i = 0; i < segment.Spline.Count - 1; i++)
                    {
                        Gizmos.color = roadColor;
                        var v1 = segment.Spline[i];
                        var v2 = segment.Spline[i + 1];

                        var dir = (v2 - v1).normalized;
                        var right = new Vector3(dir.z, 0, -dir.x);

                        for (int j = -5; j <= 5; j++)
                        {
                            Gizmos.DrawLine(basePosition + v1 + right * j * 0.5f, basePosition + v2 + right * j * 0.5f);
                        }
                    }

                }
            }
        }

        if(Settings.DrawSideRoadEdges == level)
        {
            Gizmos.color = new Color(0.5f, 0.5f, 0.9f, 0.8f);
            var segments = mesoChunk.Data.RoadData.IntersectingRoadSegments;
            foreach (var segment in segments)
            {
                for (int i = 0; i < segment.RoadOutline.Count; i++)
                {
                    var v1 = segment.V1;
                    var v2 = segment.V2;
                    Gizmos.DrawLine(basePosition + v1, basePosition + v2);
                }
            }
        }

        if (Settings.DrawSideRoadOutlines == level)
        {
            Gizmos.color = new Color(0.3f, 0f, 0.3f, 0.8f);
            var segments = mesoChunk.Data.RoadData.IntersectingRoadSegments;
            foreach (var segment in segments)
            {
                if (segment.RoadOutline == null)
                    continue;

                for (int i = 0; i < segment.RoadVertices.Count - 1; i++)
                {
                    var v1 = segment.RoadVertices[i].x.ToVec3();
                    var v2 = segment.RoadVertices[i].z.ToVec3();
                    Gizmos.DrawLine(basePosition + v1, basePosition + v2);

                    var v3 = segment.RoadVertices[(i + 1) % segment.RoadVertices.Count].x.ToVec3();
                    var v4 = segment.RoadVertices[(i + 1) % segment.RoadVertices.Count].z.ToVec3();

                    Gizmos.DrawLine(basePosition + v3, basePosition + v4);

                    Gizmos.DrawLine(basePosition + v1, basePosition + v3);
                    Gizmos.DrawLine(basePosition + v2, basePosition + v4);
                }
            }
        }
    }

}