﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

static class RegionTypeExtensions
{
    public static bool IsVillage(this RegionType type)
    {
        return type == RegionType.SubResidenceWithBushes || 
               type == RegionType.SubResidenceWithTrees ||
               type == RegionType.SubResidenceWithSparsePlants ||
               type == RegionType.SubResidenceWithTreesAndBushes ||
               type == RegionType.Village;
    }

    public static bool IsForest(this RegionType type)
    {
        return type == RegionType.SubConiferousForestPlanted || 
               type == RegionType.SubConiferousForestNatural ||
               type == RegionType.SubDeciduousForestPlanted ||
               type == RegionType.SubDeciduousForestNatural ||
               type == RegionType.SubMixedForest ||
               type == RegionType.Forestry;
    }
}

public enum RegionType
{
    City,
    Rural,

    Sidewalk,

    NatureReserve,
    Forestry,
    Cultivation,
    Plain,
    Village,

    SubResidenceWithTrees,
    SubResidenceWithBushes,
    SubResidenceWithTreesAndBushes,
    SubResidenceWithSparsePlants,

    SubConiferousForestPlanted,
    SubConiferousForestNatural,
    SubDeciduousForestPlanted,
    SubDeciduousForestNatural,
    SubMixedForest,

    SubFarmBig,
    SubFarmSmall,
    SubFarmEmpty
}

//TODO: Struct?
public class MesoChunkRegion
{
    public RegionType Type { get; private set; }

    public List<IntPoint> PointOutline { get; private set; }

    public List<Vector3> Outline { get; private set; }

    public List<MesoChunkRegion> SubRegions { get; private set; }

    public bool? IsConvex { get; private set; }
    
    public List<TerrainObjectMetaData> TerrainObjectMetaDatas { get; private set; }

    public List<GameObject> TerrainObjects { get; private set; }

    /// <summary>
    /// If set, these inner polygons will not be populated with terrain objects. E.g. useful for placing houses or driveways.
    /// </summary>
    public List<List<Vector3>> IgnorableOutlines { get; private set; }

    public bool ObjectsPlaced { get; private set; }

    public Coroutine SpawnerCoroutine { get; private set; }

    public bool GenerateTerrainMesh { get; private set; }

    public MesoChunkRegion(RegionType type, List<IntPoint> pointOutline, List<Vector3> outline, bool calculateIsConvex)
    {
        Type = type;
        PointOutline = pointOutline;
        Outline = outline;
        if (calculateIsConvex)
        {
            IsConvex = ChunkUtil.IsConvex(pointOutline);
        }

        GenerateTerrainMesh = true;
    }

    public void SetGenerateTerrainMesh(bool value)
    {
        GenerateTerrainMesh = value;
    }

    public void SetSubRegions(List<MesoChunkRegion> subRegions)
    {
        SubRegions = subRegions;
    }

    public void SetTerrainObjects(List<GameObject> terrainObjects)
    {
        TerrainObjects = terrainObjects;
    }

    public void SetSpawnerCoroutine(Coroutine coroutine)
    {
        SpawnerCoroutine = coroutine;
    }

    public void SetTerrainObjectMetaDatas(List<TerrainObjectMetaData> terrainObjectMetaDatas)
    {
        TerrainObjectMetaDatas = terrainObjectMetaDatas;
    }

    public void SetObjectsPlaced(bool objectsPlaced)
    {
        ObjectsPlaced = objectsPlaced;
    }

    public void SetIgnorableOutlines(List<List<Vector3>> ignorableOutlines)
    {
        IgnorableOutlines = ignorableOutlines;
    }

}
