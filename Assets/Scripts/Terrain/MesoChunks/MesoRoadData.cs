﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public struct Crosswalk
{
    public List<IntPoint> PointOutline;

    public Vector3 Left;

    public Vector3 Right;

    public Crosswalk(List<IntPoint> pointOutline, Vector3 left, Vector3 right)
    {
        PointOutline = pointOutline;
        Left = left;
        Right = right;
    }
}

public class MesoRoadData
{
    
    /// <summary>
    /// Dictionary of roadsegments, mapped by central meso road node's neighbour global tile positions
    /// </summary>
    public Dictionary<Vector3, RoadSegment> CentralRoadSegments { get; private set; }

    public List<RoadSegment> RoadSegments { get; private set; }

    public List<RoadSegment> IntersectingRoadSegments { get; private set; }

    public List<StreetSegment> StreetSegments { get; private set; }

    public MesoRoadNode CentralMesoRoadNode { get; private set; }

    public Dictionary<Point2, MesoRoadNode> MesoRoadNodes { get; private set; }

    public List<IntPoint> CenterRoadOutline { get; private set; }

    public List<IntPoint> CenterShoulderOutline { get; private set; }

    public List<List<IntPoint>> RoadPolys { get; private set; }

    public List<List<IntPoint>> ShoulderPolys { get; private set; }

    ///Also includes city and village roads if they exist.
    public List<List<IntPoint>> RoadAndStreetPolys { get; private set; }

    public List<Crosswalk> Crosswalks { get; private set; }

    public MesoRoadData(Dictionary<Vector3, RoadSegment> centralRoadSegments, List<RoadSegment> roadSegments, 
        List<RoadSegment> intersectingRoadSegments, MesoRoadNode centralMesoRoadNode, 
        Dictionary<Point2, MesoRoadNode> mesoRoadNodes, List<IntPoint> centerRoadOutline, List<IntPoint> centerShoulderOutline)
    {
        CentralRoadSegments = centralRoadSegments;
        RoadSegments = roadSegments;
        IntersectingRoadSegments = intersectingRoadSegments;
        CentralMesoRoadNode = centralMesoRoadNode;
        MesoRoadNodes = mesoRoadNodes;
        CenterRoadOutline = centerRoadOutline;
        CenterShoulderOutline = centerShoulderOutline;
    }

    public void SetStreetSegments(List<StreetSegment> streetSegments)
    {
        StreetSegments = streetSegments;
    }

    public void SetCrosswalks(List<Crosswalk> crosswalks)
    {
        Crosswalks = crosswalks;
    }
    public void SetRoadAndShoulderPolys(List<List<IntPoint>> roadPolys, List<List<IntPoint>> shoulderPolys)
    {
        RoadPolys = roadPolys;
        RoadAndStreetPolys = roadPolys;
        ShoulderPolys = shoulderPolys;
    }

    public void SetRoadAndStreetPolys(List<List<IntPoint>> roadAndStreetPolys)
    {
        RoadAndStreetPolys = roadAndStreetPolys;
    }
}
