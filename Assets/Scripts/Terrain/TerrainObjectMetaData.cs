﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public struct TerrainObjectMetaData
{
    public GameObject Prefab { get; private set; }

    public Vector3 Position { get; private set; }

    public Quaternion Rotation { get; private set; }

    public Vector3 Scale { get; private set; }

    public object[] Data { get; private set; }

    public TerrainObjectMetaData(GameObject prefab, Vector3 position)
        : this(prefab, position, Quaternion.identity, Vector3.one) { }

    public TerrainObjectMetaData(GameObject prefab, Vector3 position, Vector3 scale)
        : this(prefab, position, Quaternion.identity, scale) { }

    public TerrainObjectMetaData(GameObject prefab, Vector3 position, Quaternion rotation)
        : this(prefab, position, rotation, Vector3.one) { }

    public TerrainObjectMetaData(GameObject prefab, Vector3 position, Quaternion rotation, Vector3 scale) : this()
    {
        Prefab = prefab;
        Position = position;
        Rotation = rotation;
        Scale = scale;
    }

    public static TerrainObjectMetaData Generate(GameObject prefab, Vector3 pos, float yawRotationDegrees, Vector3 scale, float heightOffset)
    {
        var rot = Quaternion.Euler(0, yawRotationDegrees, 0);
        
        pos.y -= heightOffset;
        var obj = new TerrainObjectMetaData(prefab, pos,
            rot, scale);
        return obj;
    }

    public static TerrainObjectMetaData Generate(GameObject prefab, Vector3 pos, float yawRotationDegrees, float scaleMin, float scaleVariance, float heightOffset)
    {
        var rot = Quaternion.Euler(0, yawRotationDegrees, 0);

        var scaleFactor = scaleMin + ((ChunkUtil.Hash((int)pos.x, (int)pos.z) % 10000) / 10000f) * scaleVariance;

        pos.y -= heightOffset * scaleFactor;
        var obj = new TerrainObjectMetaData(prefab, pos,
            rot, new Vector3(scaleFactor, scaleFactor, scaleFactor));
        return obj;
    }

    public static TerrainObjectMetaData Generate(GameObject prefab, Vector3 pos, float yawRotationDegrees, float scaleMin, float scaleVariance, float heightOffset, object[] data)
    {
        var rot = Quaternion.Euler(0, yawRotationDegrees, 0);

        var scaleFactor = scaleMin + ((ChunkUtil.Hash((int)pos.x, (int)pos.z) % 10000) / 10000f) * scaleVariance;

        pos.y -=- heightOffset * scaleFactor;
        var obj = new TerrainObjectMetaData(prefab, pos,
            rot, new Vector3(scaleFactor, scaleFactor, scaleFactor));
        obj.Data = data;
        return obj;
    }
}