﻿using System;
using System.Xml.Serialization;
using UnityEngine;

public static class NoiseService
{
    private static FastNoise baseNoise;
    
    private static FastNoise terrainNoise;
    private static FastNoiseSIMD terrainNoiseSIMD;

    private static FastNoise regionTypeNoise;
    private static FastNoiseSIMD regionTypeNoiseSIMD;

    private static FastNoise terrainObjectNoise;
    private static FastNoiseSIMD terrainObjectNoiseSIMD;

    public static bool Initialized { get; private set; }

    public static void Initialize()
    {
        if (Initialized)
            return;

        baseNoise = new FastNoise();
        baseNoise.SetNoiseType(FastNoise.NoiseType.Simplex);
        baseNoise.SetFractalOctaves(2);
        baseNoise.SetFractalLacunarity(2);
        baseNoise.SetFractalGain(0.5f);
        baseNoise.SetFrequency(1 / 8192f);

        terrainNoise = new FastNoise();
        terrainNoise.SetNoiseType(FastNoise.NoiseType.Simplex);
        terrainNoise.SetFractalOctaves(4);
        terrainNoise.SetFractalLacunarity(2);
        terrainNoise.SetFractalGain(0.5f);
        terrainNoise.SetFrequency(1 / 16384f);

        regionTypeNoiseSIMD = new FastNoiseSIMD();
        regionTypeNoiseSIMD.SetNoiseType(FastNoiseSIMD.NoiseType.Simplex);
        regionTypeNoiseSIMD.SetFractalOctaves(4);
        regionTypeNoiseSIMD.SetFractalLacunarity(2);
        regionTypeNoiseSIMD.SetFractalGain(0.5f);
        regionTypeNoiseSIMD.SetFrequency(1 / 16384f);

        regionTypeNoise = new FastNoise();
        regionTypeNoise.SetNoiseType(FastNoise.NoiseType.Simplex);
        regionTypeNoise.SetFractalOctaves(2);
        regionTypeNoise.SetFractalLacunarity(2);
        regionTypeNoise.SetFractalGain(0.5f);
        regionTypeNoise.SetFrequency(1 / 16384f);

        regionTypeNoiseSIMD = new FastNoiseSIMD();
        regionTypeNoiseSIMD.SetNoiseType(FastNoiseSIMD.NoiseType.Simplex);
        regionTypeNoiseSIMD.SetFractalOctaves(2);
        regionTypeNoiseSIMD.SetFractalLacunarity(2);
        regionTypeNoiseSIMD.SetFractalGain(0.5f);
        regionTypeNoiseSIMD.SetFrequency(1 / 16384f);

        terrainObjectNoise = new FastNoise();
        terrainObjectNoise.SetNoiseType(FastNoise.NoiseType.Simplex);
        terrainObjectNoise.SetFractalOctaves(2);
        terrainObjectNoise.SetFractalLacunarity(2);
        terrainObjectNoise.SetFractalGain(0.5f);
        terrainObjectNoise.SetFrequency(1 / 1024f);

        terrainObjectNoiseSIMD = new FastNoiseSIMD();
        terrainObjectNoiseSIMD.SetNoiseType(FastNoiseSIMD.NoiseType.Simplex);
        terrainObjectNoiseSIMD.SetFractalOctaves(2);
        terrainObjectNoiseSIMD.SetFractalLacunarity(2);
        terrainObjectNoiseSIMD.SetFractalGain(0.5f);
        terrainObjectNoiseSIMD.SetFrequency(1 / 1024f);

        Initialized = true;

    }

    //TODO: Remove "AtPos" from names
    public static float GetTerrainNoiseAtPos(Vector3 globalPos)
    {
        return GetTerrainNoiseAtPos(globalPos.x, globalPos.z);
    }

    public static float GetTerrainNoiseAtPos(float x, float z)
    {
        return terrainNoise.GetSimplex(x, z) * 0.5f + 0.5f;
    }

    public static float GetRegionTypeNoise(Vector3 globalPos)
    {
        return GetRegionTypeNoise(globalPos.x, globalPos.z);
    }

    public static float GetRegionTypeNoise(float x, float z)
    {
        return regionTypeNoise.GetSimplex(x, z) * 0.5f + 0.5f;
    }

    public static float GetTerrainObjectNoise(Vector3 globalPos)
    {
        return GetTerrainObjectNoise(globalPos.x, globalPos.z);
    }

    public static float GetTerrainObjectNoise(float x, float z)
    {
        return terrainObjectNoise.GetSimplex(x, z) * 0.5f + 0.5f;
    }


    public static float GetNoiseAtPos(Vector3 globalPos)
    {
        return GetNoiseAtPos(globalPos.x, globalPos.z);
    }

    public static float GetNoiseAtPos(float x, float z) 
    {
        return baseNoise.GetSimplex(x, z) * 0.5f + 0.5f;
    }

    public static float GetNoiseAtTile(Point2 globalTile)
    {
        return GetNoiseAtTile(globalTile.x, globalTile.z);
    }

    public static float GetNoiseAtTile(int x, int z)
    {
        return GetNoiseAtPos(CoordUtil.GlobalTileToGlobalPosition(x, z));
    }

    public static float[] GetRegionTypeNoise(int x, int z, int width, int height)
    {
        return regionTypeNoiseSIMD.GetNoiseSet(x, 0, z, width, 0, height);
    }
    
    public static float[] GetTerrainObjectNoise(int x, int z, int width, int height)
    {
        return terrainObjectNoiseSIMD.GetNoiseSet(x, 0, z, width, 0, height);
    }

    public static float[] GetNoiseForMacroChunk(Point2 macroChunkPos)
    {
        var globalPos = CoordUtil.MacroChunkToGlobalPosition(macroChunkPos);
        var width = Const.MACRO_CHUNK_AREA_IN_MICRO_CHUNKS;
        return terrainNoiseSIMD.GetNoiseSet((int)globalPos.x, 0, (int)globalPos.z, width, 0, width);
    }
}