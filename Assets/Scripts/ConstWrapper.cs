﻿using System;
using UnityEngine;
// ReSharper disable PossibleLossOfFraction

public class ConstWrapper : MonoBehaviour
{

    public float TileWidthInMeters = 2f;

    public int MicroChunkWidthInTiles = 32;

    public int MesoChunkWidthInMicroChunks = 8;

    public int MacroChunkWidthInMesoChunks = 16;

    void Awake()
    {
        Refresh();
    }

    void OnValidate()
    {
        Refresh();
    }

    private void Refresh()
    {
        //Debug.Log("Updating constants");
        Const.Refresh(TileWidthInMeters, MicroChunkWidthInTiles, MesoChunkWidthInMicroChunks, MacroChunkWidthInMesoChunks);
    }

}
