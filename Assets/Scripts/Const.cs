﻿using System;
using UnityEngine;
// ReSharper disable PossibleLossOfFraction

public static class Const
{

    public static float TILE_WIDTH_IN_METERS = 4f;
    #region microchunk variables
    public static float MICRO_CHUNK_WIDTH_IN_METERS;

    public static int MICRO_CHUNK_WIDTH_IN_TILES;

    public static int MICRO_CHUNK_AREA_IN_TILES;

    public static int MICRO_CHUNK_AREA_IN_VERTICES;
    #endregion

    #region mesochunk variables

    public static float MESO_CHUNK_WIDTH_IN_METERS;

    public static int MESO_CHUNK_WIDTH_IN_TILES;

    public static int MESO_CHUNK_WIDTH_IN_MICRO_CHUNKS;

    public static int MESO_CHUNK_AREA_IN_TILES;

    public static int MESO_CHUNK_AREA_IN_MICRO_CHUNKS;
    #endregion

    #region macrochunk variables

    public static float MACRO_CHUNK_WIDTH_IN_METERS;

    public static int MACRO_CHUNK_WIDTH_IN_TILES;

    public static int MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS;

    public static int MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;

    public static int MACRO_CHUNK_AREA_IN_TILES;
    
    public static int MACRO_CHUNK_AREA_IN_MICRO_CHUNKS;

    public static int MACRO_CHUNK_AREA_IN_MESO_CHUNKS;
    #endregion

    #region river related variables
    public static int MACRO_CHUNK_WIDTH_IN_RIVER_CHUNKS = 32;

    public static int RIVER_CHUNK_WIDTH_IN_TILES = 64;

    public static float RIVER_PROBABILITY = 0.38f;

    public static float RIVER_NOISE_MIN_MAX_CUTOFF = 0.15f;

    public static float RIVER_HEIGHT_MULTIPLIER = 10f;

    public static int RIVER_MIN_NODES = 70;

    public static int RIVER_BRANCH_MIN_NODES = 8;
    #endregion

    public static float MAX_TERRAIN_BLEND_DIST = 16;

    #region road related variables
    public static int ROAD_TRIANGLE_MINIMUM_ANGLE = 58;

    public static int ROAD_NEIGHBOUR_RADIUS = 2;

    public static int ROAD_FAR_NEIGHBOUR_RADIUS = 3;

    public static float HIGHWAY_WIDTH_IN_METERS = 12;

    /// <summary>
    /// This also includes the width of the highway.
    /// </summary>
    public static float HIGHWAY_SHOULDER_WIDTH_IN_METERS = 24;

    public static float SIDE_ROAD_WIDTH_IN_METERS = 8;

    /// <summary>
    /// This also includes the width of the side road.
    /// </summary>
    public static float SIDE_ROAD_SHOULDER_WIDTH_IN_METERS = 22;

    public static float MESO_CHUNK_RURAL_ROAD_PROBABILITY = 0.25f;

    public static float MACRO_CHUNK_ROAD_PROBABILITY = 0.22f;

    public static float MACRO_CHUNK_ROAD_RADIUS = 6f;
    
    public static int CITY_BLOCK_WIDTH_IN_METERS = 128;

    public static int STREET_WIDTH_IN_METERS = 6;

    public static int UNPAVED_ROAD_WIDTH_IN_METERS = 6;
    #endregion

    public static void Refresh(float tileWidthInMeters, int microChunkWidthInTiles,
        int mesoChunkWidthInMicroChunks, int macroChunkWidthInMesoChunks)
    {
        

        #region init
        TILE_WIDTH_IN_METERS = tileWidthInMeters;

        MICRO_CHUNK_WIDTH_IN_METERS = microChunkWidthInTiles * tileWidthInMeters;
        MICRO_CHUNK_WIDTH_IN_TILES = microChunkWidthInTiles;
        MICRO_CHUNK_AREA_IN_TILES = microChunkWidthInTiles * microChunkWidthInTiles;
        MICRO_CHUNK_AREA_IN_VERTICES = (microChunkWidthInTiles + 1) * (microChunkWidthInTiles + 1);

        MESO_CHUNK_WIDTH_IN_METERS = mesoChunkWidthInMicroChunks * MICRO_CHUNK_WIDTH_IN_METERS;
        MESO_CHUNK_WIDTH_IN_TILES = mesoChunkWidthInMicroChunks * MICRO_CHUNK_WIDTH_IN_TILES;
        MESO_CHUNK_WIDTH_IN_MICRO_CHUNKS = mesoChunkWidthInMicroChunks;
        MESO_CHUNK_AREA_IN_TILES = MESO_CHUNK_WIDTH_IN_TILES * MESO_CHUNK_WIDTH_IN_TILES;
        MESO_CHUNK_AREA_IN_MICRO_CHUNKS = mesoChunkWidthInMicroChunks * mesoChunkWidthInMicroChunks;

        MACRO_CHUNK_WIDTH_IN_METERS = macroChunkWidthInMesoChunks * MESO_CHUNK_WIDTH_IN_METERS;
        MACRO_CHUNK_WIDTH_IN_TILES = macroChunkWidthInMesoChunks * MESO_CHUNK_WIDTH_IN_TILES;
        MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS = macroChunkWidthInMesoChunks * MESO_CHUNK_WIDTH_IN_MICRO_CHUNKS;
        MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS = macroChunkWidthInMesoChunks;
        MACRO_CHUNK_AREA_IN_TILES = MACRO_CHUNK_WIDTH_IN_TILES * MACRO_CHUNK_WIDTH_IN_TILES;
        MACRO_CHUNK_AREA_IN_MICRO_CHUNKS = MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS * MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS;
        MACRO_CHUNK_AREA_IN_MESO_CHUNKS = macroChunkWidthInMesoChunks * macroChunkWidthInMesoChunks;
        #endregion

        RIVER_CHUNK_WIDTH_IN_TILES = MACRO_CHUNK_WIDTH_IN_TILES / MACRO_CHUNK_WIDTH_IN_RIVER_CHUNKS;

        //Backwards compatibility
        HALF_TILE = new Vector3(TILE_WIDTH_IN_METERS / 2, 0, TILE_WIDTH_IN_METERS / 2);
        CHUNK_WIDTH_IN_METERS = CHUNK_WIDTH_IN_TILES * TILE_WIDTH_IN_METERS;
        CHUNK_AREA_IN_METERS = CHUNK_WIDTH_IN_METERS * CHUNK_WIDTH_IN_METERS;
        HALF_CHUNK = new Vector3(CHUNK_WIDTH_IN_METERS / 2, 0, CHUNK_WIDTH_IN_METERS / 2);
        RIVER_CHUNK_WIDTH_IN_METERS = RIVER_CHUNK_WIDTH_IN_CHUNKS * CHUNK_WIDTH_IN_METERS;
    }


    public static int CHUNK_WIDTH_IN_TILES = 32;
    public static int RIVER_CHUNK_WIDTH_IN_CHUNKS = 16;

    public static Vector3 HALF_TILE = new Vector3(TILE_WIDTH_IN_METERS / 2, 0, TILE_WIDTH_IN_METERS / 2);

    public static Vector3 HALF_CHUNK = new Vector3(CHUNK_WIDTH_IN_METERS / 2, 0, CHUNK_WIDTH_IN_METERS / 2);

    public static float CHUNK_WIDTH_IN_METERS = CHUNK_WIDTH_IN_TILES * TILE_WIDTH_IN_METERS;

    public static float CHUNK_AREA_IN_METERS = CHUNK_WIDTH_IN_METERS * CHUNK_WIDTH_IN_METERS;

    public static float RIVER_CHUNK_WIDTH_IN_METERS = RIVER_CHUNK_WIDTH_IN_CHUNKS * CHUNK_WIDTH_IN_METERS;
    //-----

    public static int HEIGHTMAP_RANGE_IN_METERS = 512;

    
    public static int CHUNK_WIDTH_IN_VERTICES = CHUNK_WIDTH_IN_TILES + 1;

    
    public static int CHUNK_AREA_IN_TILES = CHUNK_WIDTH_IN_TILES * CHUNK_WIDTH_IN_TILES;

    public static int CHUNK_AREA_IN_VERTICES = CHUNK_WIDTH_IN_VERTICES * CHUNK_WIDTH_IN_VERTICES;
    
}
