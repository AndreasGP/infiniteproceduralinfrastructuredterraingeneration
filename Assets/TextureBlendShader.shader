﻿Shader "Custom/TextureBlendShader" {
 Properties {
             _Color ("Color", Color) = (1,1,1,1)
             _MainTexWhite ("White (RGB)", 2D) = "white" {}
             _MainTexR ("Albedo 1 (RGB)", 2D) = "white" {}
             _MainTexG ("Albedo 2 (RGB)", 2D) = "white" {}
             _MainTexB ("Albedo 3 (RGB)", 2D) = "white" {}
             _MainTexA ("Albedo 4 (RGB)", 2D) = "white" {}
             _Glossiness ("Smoothness", Range(0,1)) = 0.5
             _Metallic ("Metallic", Range(0,1)) = 0.0
         }
         SubShader {
             Tags { "RenderType"="Opaque" }
			Pass {
				Blend One One
				SetTexture [_MainTexR] { combine texture }
			}
             LOD 200
            
             CGPROGRAM
             // Physically based Standard lighting model, and enable shadows on all light types
             #pragma surface surf Standard fullforwardshadows
      
             // Use shader model 3.0 target, to get nicer looking lighting
             #pragma target 3.0
      
             sampler2D _MainTexWhite;
             sampler2D _MainTexR;
             sampler2D _MainTexG;
             sampler2D _MainTexB;
             sampler2D _MainTexA;
      
             struct Input {
                 float2 uv_MainTexR;
				 float4 color : Color;
             };
      
             half _Glossiness;
             half _Metallic;
             fixed4 _Color;
      
             void surf (Input IN, inout SurfaceOutputStandard o) {
                 // Albedo comes from a texture tinted by color

				 float w1 = IN.color.r + IN.color.g;
				 float w2 = IN.color.b + IN.color.a;

                 fixed4 lerp1 = lerp (tex2D (_MainTexR, IN.uv_MainTexR), tex2D (_MainTexG, IN.uv_MainTexR), IN.color.g / w1) * _Color;
                 fixed4 lerp2 = lerp (tex2D (_MainTexB, IN.uv_MainTexR), tex2D (_MainTexA, IN.uv_MainTexR), IN.color.a / w2) * _Color;

				 fixed4 c = lerp(lerp1, lerp2, w2 / (w1 + w2));

                 o.Albedo = c.rgb;
                 // Metallic and smoothness come from slider variables
                 o.Metallic = _Metallic;
                 o.Smoothness = _Glossiness;
                 o.Alpha = IN.color.a;
             }
             ENDCG
         }
         FallBack "Diffuse"
     }